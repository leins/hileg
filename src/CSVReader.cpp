/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 *
 * ------------------------------------------------------------------------------------
 *
 * Class to read data from a csv file.
 * Adapted from:
 * https://thispointer.com/how-to-read-data-from-a-csv-file-in-c/
 * https://www.techiedelight.com/split-string-cpp-using-delimiter/
 */

#include "CSVReader.h"

/*
* Parses through csv file line by line and returns the data
* in vector of vector of strings.
*/
vector<vector<string> > CSVReader::getData()
{
	ifstream file(fileName);

	vector<vector<string> > dataList;

	string line = "";
	size_t nCols = 0;
	// Iterate through each line and split the content using delimiter
	while (getline(file, line))
	{
		if (line.rfind("#", 0) == 0 || line.empty()) {
		  continue;
		}
		size_t nDelim = std::count(line.begin(), line.end(), delimiter);
		if(nCols == 0) {
			nCols = nDelim + 1;
		}
		if(nCols <= 0 || nCols != (nDelim + 1)) {
			dataList.clear();
			break;
		}
		if (!line.empty() && line[line.size() - 1] == '\r') {
			line.erase(line.size() - 1);
		}
		string delim(1, delimiter);
		regex regex(delim);
		vector<string> vec(
					sregex_token_iterator(line.begin(), line.end(), regex, -1),
					sregex_token_iterator()
					);

//		adding empty string in case in case last column was empty and therefore skipped
		if(vec.size() < nCols) {
			vec.push_back("");
		}

		dataList.push_back(vec);
	}
	// Close the File
	file.close();

	return dataList;
}

bool CSVReader::isValid()
{
	ifstream file(fileName);
	bool valid = file.is_open();
	file.close();
	return valid;
}

