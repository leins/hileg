/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef POPULATIONATTS_H_
#define POPULATIONATTS_H_

#include "Point.h"
#include "Constants.h"

class PopulationAtts {
private:
	Point coord;
	double carryingCapacity; // per sqm
	double densityAboveGround, densityBelowGround;
	DispersalType dispersalType;
	bool gaining;
	bool longDistDisp; // long distance dispersal activated
	bool discreteDispersal; // only allow dispersal of full (discrete) individuals
public:
	PopulationAtts(Point coord, double carryingCapacity, DispersalType dispersalType, bool longDistDisp = false, bool discreteDispersal = false);
	Point getCoord();
	double getCarryingCapacity() const;
	double getDensity() const;
	double getDensityAboveGround() const;
	void setDensityAboveGround(double densityAboveGround);
	double getDensityBelowGround() const;
	void setDensityBelowGround(double densityBelowGround);
	DispersalType getDispersalType() const;
	bool isGaining() const;
	void setGaining(bool gaining);
	bool isLongDistDisp() const;
	void setLongDistDisp(bool longDistDisp);
	bool isDiscreteDispersal() const;
	void setDiscreteDispersal(bool discreteDispersal);
};

#endif /* POPULATIONATTS_H_ */
