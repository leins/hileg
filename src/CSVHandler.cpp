/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "CSVHandler.h"

bool CSVHandler::isEmpty() const {
	return empty;
}

bool CSVHandler::isValid() const {
	return valid;
}

string CSVHandler::getMessage() const {
	return message;
}

vector<vector<string>> CSVHandler::getData(){
	return data;
}

vector<string> CSVHandler::getHeader(){
	return header;
}

CSVHandler::CSVHandler(string filename, bool hasHeader, char delm) {
	CSVReader reader(filename, delm);
	if(!reader.isValid()) {
		message = "UNREADABLE";
		valid = false;
		return;
	}
	vector<vector<string>> coreData(reader.getData());
	if(coreData.empty()) {
		message = "EMPTY";
	}else{
		data = coreData;
		if(hasHeader) {
			for(auto headerName : coreData[0]) {
				transform(headerName.begin(), headerName.end(), headerName.begin(), ::tolower);
				header.push_back(headerName);
			}
			data.erase(data.begin());
		}
		if(data.empty()){
			message = "EMPTY";
		}else{
			message = "VALID";
			empty = false;
			nRows = data.size();
		}
	}
}

CSVHandlerCoordinates::CSVHandlerCoordinates(string filename, vector<string> headerNames)
		: CSVHandlerDefault(filename, headerNames) {
	if(!(empty || missingHeaders)){

		vector<string> iLons = getValues(headerNames[0]);
		vector<string> iLats = getValues(headerNames[1]);

		for(unsigned int iCoords = 0; iCoords < iLons.size(); iCoords++) {
			int iLon = stoi(iLons[iCoords]);
			int iLat = stoi(iLats[iCoords]);
			coordinates.emplace(new Point(iLon - 1, iLat - 1));
		}

		if(coordinates.size() == 0) {
			empty = true;
		}
	}
}

unordered_set<Point*> CSVHandlerCoordinates::getCoordinates() {
	return coordinates;
}

//{"xCoord","yCoord","coverRatio","iLonClim","iLatClim"}
CSVHandlerGrassland::CSVHandlerGrassland(string filename, bool climInterpolate, vector<string> headerNames)
		: CSVHandlerDefault(filename, headerNames) {
	if(!(empty || missingHeaders)){

		vector<string> xCoords = getValues(headerNames[0]);
		vector<string> yCoords = getValues(headerNames[1]);
		vector<string> coverRatios = getValues(headerNames[2]);
		vector<string> iLonsClim = getValues(headerNames[3]);
		vector<string> iLatsClim = getValues(headerNames[4]);
		vector<string> GLZs = getValues("GLZ");
		vector<string> climCellIDs = getValues("climCellID");

		for(unsigned int iRow = 0; iRow < xCoords.size(); iRow++) {
			std::pair<tools::ConversionResult, int> xCoord = tools::conv_stoi(xCoords[iRow]);
			std::pair<tools::ConversionResult, int> yCoord = tools::conv_stoi(yCoords[iRow]);
			std::pair<tools::ConversionResult, double> coverRatio = tools::conv_stod(coverRatios[iRow]);
			std::pair<tools::ConversionResult, int> iLonClim = tools::conv_stoi(iLonsClim[iRow]);
			std::pair<tools::ConversionResult, int> iLatClim = tools::conv_stoi(iLatsClim[iRow]);

			string sGLZ = "";
			string sClimCellID = "";
			if(!GLZs.empty()) {
				sGLZ = GLZs[iRow];
			}
			if(!climCellIDs.empty()) {
				sClimCellID = climCellIDs[iRow];
			}
			std::pair<tools::ConversionResult, int> GLZ = tools::conv_stoi(sGLZ);
			std::pair<tools::ConversionResult, int> climCellID = tools::conv_stoi(sClimCellID);

			if(xCoord.first == tools::ConversionResult::EMPTY) {
				message = "SPECIFIED EMPTY VALUE FOR PARAMETER xCoord IN GRASSLAND CELL #";
				message.append(std::to_string(iRow + 1));
				valid = false;
				break;
			}
			if(yCoord.first == tools::ConversionResult::EMPTY) {
				message = "SPECIFIED EMPTY VALUE FOR PARAMETER yCoord IN GRASSLAND CELL #";
				message.append(std::to_string(iRow + 1));
				valid = false;
				break;
			}
			if(iLonClim.first == tools::ConversionResult::EMPTY) {
				message = "SPECIFIED EMPTY VALUE FOR PARAMETER iLonClim IN GRASSLAND CELL #";
				message.append(std::to_string(iRow + 1));
				valid = false;
				break;
			}
			if(iLatClim.first == tools::ConversionResult::EMPTY) {
				message = "SPECIFIED EMPTY VALUE FOR PARAMETER iLatClim IN GRASSLAND CELL #";
				message.append(std::to_string(iRow + 1));
				valid = false;
				break;
			}

			if(xCoord.first == tools::ConversionResult::INVALID || xCoord.second < 1) {
				message = "SPECIFIED INVALID VALUE FOR PARAMETER xCoord IN GRASSLAND CELL #";
				message.append(std::to_string(iRow + 1));
				message.append(". PLEASE PROVIDE A VALUE > 0");
				valid = false;
				break;
			}
			if(yCoord.first == tools::ConversionResult::INVALID || yCoord.second < 1) {
				message = "SPECIFIED INVALID VALUE FOR PARAMETER yCoord IN GRASSLAND CELL #";
				message.append(std::to_string(iRow + 1));
				message.append(". PLEASE PROVIDE A VALUE > 0");
				valid = false;
				break;
			}
			if(coverRatio.first == tools::ConversionResult::INVALID || !(coverRatio.second > 0 && coverRatio.second <= 1.0)) {
				message = "SPECIFIED INVALID VALUE FOR PARAMETER coverRatio IN GRASSLAND CELL #";
				message.append(std::to_string(iRow + 1));
				message.append(". PLEASE PROVIDE A VALUE > 0 AND <= 1");
				valid = false;
				break;
			}
			if(iLonClim.first == tools::ConversionResult::INVALID || iLonClim.second < 1) {
				message = "SPECIFIED INVALID VALUE FOR PARAMETER iLonClim IN GRASSLAND CELL #";
				message.append(std::to_string(iRow + 1));
				message.append(". PLEASE PROVIDE A VALUE > 0");
				valid = false;
				break;
			}
			if(iLatClim.first == tools::ConversionResult::INVALID || iLonClim.second < 1) {
				message = "SPECIFIED INVALID VALUE FOR PARAMETER iLatClim IN GRASSLAND CELL #";
				message.append(std::to_string(iRow + 1));
				message.append(". PLEASE PROVIDE A VALUE > 0");
				valid = false;
				break;
			}
			if(GLZ.first == tools::ConversionResult::INVALID || (GLZ.first == tools::ConversionResult::VALID && GLZ.second < 1)) {
				message = "SPECIFIED INVALID VALUE FOR PARAMETER GLZ IN GRASSLAND CELL #";
				message.append(std::to_string(iRow + 1));
				message.append(". PLEASE PROVIDE A VALUE > 0");
				valid = false;
				break;
			}
			if(climCellID.first == tools::ConversionResult::INVALID || (climCellID.first == tools::ConversionResult::VALID && climCellID.second < 1)) {
				message = "SPECIFIED INVALID VALUE FOR PARAMETER climCellID IN GRASSLAND CELL #";
				message.append(std::to_string(iRow + 1));
				message.append(". PLEASE PROVIDE A VALUE > 0");
				valid = false;
				break;
			}

			if(coverRatio.first == tools::ConversionResult::EMPTY) {
				coverRatio.second = 1.0;
			}
			Point climCoord(iLonClim.second - 1, iLatClim.second - 1);
			if(Constants::VALID_COORDINATES_NW_GERMANY.find(climCoord) == Constants::VALID_COORDINATES_NW_GERMANY.end()) {
				message = "INVALID CLIMATE COORDINATE [";
				message.append(std::to_string(climCoord.getX()));
				message.append(",");
				message.append(std::to_string(climCoord.getY()));
				valid = false;
				break;
			}

//			TODO check for duplicate use of grassland coordinate

			Point grasslandCoord(xCoord.second - 1, yCoord.second - 1);
			Cell* grasslandCell = new Cell(grasslandCoord, climCoord, coverRatio.second, GLZ.second, climCellID.second);

			climCoords.emplace(climCoord);
			grasslandCells.emplace(grasslandCell);

			float centerClimX = iLonClim.second
					* Constants::GRASSLAND_PER_CLIMATE_CELL_1D
					* Constants::STRAIN_FACTOR_GRASSLAND_X
					- Constants::OFFSET_GRASSLAND_X;
			float centerClimY = iLatClim.second
					* Constants::GRASSLAND_PER_CLIMATE_CELL_1D
					* Constants::STRAIN_FACTOR_GRASSLAND_Y
					- Constants::OFFSET_GRASSLAND_Y;
			if(climInterpolate && !(centerClimX == xCoord.second && centerClimY == yCoord.second)) {
				float totalArea = 0.0;
				Point interpolCoordHabitat(iLonClim.second - 1, iLatClim.second - 1);
				unordered_map<Point, float> interpolFactByClimCoord;
				interpolFactByClimCoord[interpolCoordHabitat] = 0.0;
				int shiftX = Constants::STRAIN_FACTOR_GRASSLAND_X < 0 ? -1 : 1;
				int shiftY = Constants::STRAIN_FACTOR_GRASSLAND_Y < 0 ? -1 : 1;
				if(xCoord.second < centerClimX) {
					Point interpolCoord(interpolCoordHabitat.getX() - shiftX, interpolCoordHabitat.getY());
					if(Constants::VALID_COORDINATES_NW_GERMANY.find(interpolCoord) != Constants::VALID_COORDINATES_NW_GERMANY.end()) {
						interpolFactByClimCoord[interpolCoord] = 0.0;
					}
				}
				if(xCoord.second > centerClimX) {
					Point interpolCoord(interpolCoordHabitat.getX() + shiftX, interpolCoordHabitat.getY());
					if(Constants::VALID_COORDINATES_NW_GERMANY.find(interpolCoord) != Constants::VALID_COORDINATES_NW_GERMANY.end()) {
						interpolFactByClimCoord[interpolCoord] = 0.0;
					}
				}
				if(yCoord.second < centerClimY) {
					Point interpolCoord(interpolCoordHabitat.getX(), interpolCoordHabitat.getY() - shiftY);
					if(Constants::VALID_COORDINATES_NW_GERMANY.find(interpolCoord) != Constants::VALID_COORDINATES_NW_GERMANY.end()) {
						interpolFactByClimCoord[interpolCoord] = 0.0;
					}
				}
				if(yCoord.second > centerClimY) {
					Point interpolCoord(interpolCoordHabitat.getX(), interpolCoordHabitat.getY() + shiftY);
					if(Constants::VALID_COORDINATES_NW_GERMANY.find(interpolCoord) != Constants::VALID_COORDINATES_NW_GERMANY.end()) {
						interpolFactByClimCoord[interpolCoord] = 0.0;
					}
				}
				if(xCoord.second < centerClimX && yCoord.second > centerClimY) {
					Point interpolCoord(interpolCoordHabitat.getX() - shiftX, interpolCoordHabitat.getY() + shiftY);
					if(Constants::VALID_COORDINATES_NW_GERMANY.find(interpolCoord) != Constants::VALID_COORDINATES_NW_GERMANY.end()) {
						interpolFactByClimCoord[interpolCoord] = 0.0;
					}
				}
				if(xCoord.second > centerClimX && yCoord.second > centerClimY) {
					Point interpolCoord(interpolCoordHabitat.getX() + shiftX, interpolCoordHabitat.getY() + shiftY);
					if(Constants::VALID_COORDINATES_NW_GERMANY.find(interpolCoord) != Constants::VALID_COORDINATES_NW_GERMANY.end()) {
						interpolFactByClimCoord[interpolCoord] = 0.0;
					}
				}
				if(xCoord.second > centerClimX && yCoord.second < centerClimY) {
					Point interpolCoord(interpolCoordHabitat.getX() + shiftX, interpolCoordHabitat.getY() - shiftY);
					if(Constants::VALID_COORDINATES_NW_GERMANY.find(interpolCoord) != Constants::VALID_COORDINATES_NW_GERMANY.end()) {
						interpolFactByClimCoord[interpolCoord] = 0.0;
					}
				}
				if(xCoord.second < centerClimX && yCoord.second < centerClimY) {
					Point interpolCoord(interpolCoordHabitat.getX() - shiftX, interpolCoordHabitat.getY() - shiftY);
					if(Constants::VALID_COORDINATES_NW_GERMANY.find(interpolCoord) != Constants::VALID_COORDINATES_NW_GERMANY.end()) {
						interpolFactByClimCoord[interpolCoord] = 0.0;
					}
				}

				for(auto pairClimCoordInterpolRatio : interpolFactByClimCoord) { // first calculate all distances and set 1/dist as interpol factor
					float sizeX = abs(Constants::GRASSLAND_PER_CLIMATE_CELL_1D * Constants::STRAIN_FACTOR_GRASSLAND_X);
					float sizeY = abs(Constants::GRASSLAND_PER_CLIMATE_CELL_1D * Constants::STRAIN_FACTOR_GRASSLAND_Y);
					float centerInterpolClimX = (pairClimCoordInterpolRatio.first.getX() + 1) //add 1 to X to be in same system as offset calc
							* Constants::GRASSLAND_PER_CLIMATE_CELL_1D
							* Constants::STRAIN_FACTOR_GRASSLAND_X
							- Constants::OFFSET_GRASSLAND_X;
					float centerInterpolClimY = (pairClimCoordInterpolRatio.first.getY() + 1) //add 1 to Y to be in same system as offset calc
							* Constants::GRASSLAND_PER_CLIMATE_CELL_1D
							* Constants::STRAIN_FACTOR_GRASSLAND_Y
							- Constants::OFFSET_GRASSLAND_Y;
					float distGrasslandToX = abs(xCoord.second - centerInterpolClimX);
					distGrasslandToX = distGrasslandToX >= sizeX ? sizeX - 1 : distGrasslandToX;
					float distGrasslandToY = abs(yCoord.second - centerInterpolClimY);
					distGrasslandToY = distGrasslandToY >= sizeY ? sizeY - 1 : distGrasslandToY;
					float area = (sizeX - distGrasslandToX) * (sizeY - distGrasslandToY);
					interpolFactByClimCoord[pairClimCoordInterpolRatio.first] = area;
					totalArea += area;
				}
				for(auto pairClimCoordInterpolRatio : interpolFactByClimCoord) { // second calculate the actual interpol factor according to the distances sum
					Point climCoordInterpol(pairClimCoordInterpolRatio.first.getX(), pairClimCoordInterpolRatio.first.getY());
					float interpolRatio = pairClimCoordInterpolRatio.second / totalArea;
					grasslandCell->setInterpolRatio(climCoordInterpol,  interpolRatio);
					climCoords.emplace(climCoordInterpol);
				}

			}
		}

		if(valid && climCoords.size() == 0) {
			empty = true;
		}
	}
}

unordered_set<Cell*> CSVHandlerGrassland::getGrasslandCells() {
	return grasslandCells;
}

unordered_set<Point> CSVHandlerGrassland::getClimCoords() {
	return climCoords;
}

CSVHandlerDefault::CSVHandlerDefault(string filename, vector<string> headerNames) : CSVHandler(filename) {
	if(!empty){
		for(auto headerName : headerNames) {
			validHeaderNames.push_back(headerName);
			transform(headerName.begin(), headerName.end(), headerName.begin(), ::tolower);
			missingHeaders = missingHeaders || (std::find(header.begin(), header.end(), headerName) == header.end());
			this->headerNames.push_back(headerName);
		}
		int iCol = 0;
		for(auto colName : header) {
			transform(colName.begin(), colName.end(), colName.begin(), ::tolower);
			vector<string> colData;
			for(vector<string> row : data) {
				string cell = row[iCol];
				colData.push_back(row[iCol]);
			}
			valuesByHeader[colName] = colData;
			if(std::find(headerNames.begin(), headerNames.end(), colName) == headerNames.end()) {
				headerNames.push_back(colName);
			}
			iCol++;
		}
		if(missingHeaders) {
			message = "MISSING HEADERS";
		}
		if(valuesByHeader.size() == 0) {
			message = "EMPTY";
			empty = true;
		}
	}
}

vector<string> CSVHandlerDefault::getValues(string headerName) {
	vector<string> values;
	transform(headerName.begin(), headerName.end(), headerName.begin(), ::tolower);
	if(valuesByHeader.find(headerName) != valuesByHeader.end()) {
		values = valuesByHeader[headerName];
	}
	return values;
}

CSVHandlerDensities::CSVHandlerDensities(string filename,
		unordered_set<string> validStageNames)
		: CSVHandlerDefault(filename, {"xCoord", "yCoord", "stageName", "density"})
{
	if(!valid) {
		message.append("\nPlease supply valid or empty file ");
		message.append(filename);
		return;
	}
	if(!(empty || missingHeaders)){

		vector<string> xCoords = getValues(headerNames[0]);
		vector<string> yCoords = getValues(headerNames[1]);
		vector<string> stageNames = getValues(headerNames[2]);
		vector<string> densities = getValues(headerNames[3]);

		for(unsigned int iRow = 0; iRow < xCoords.size(); iRow++) {
			std::pair<tools::ConversionResult, int> xCoord = tools::conv_stoi(xCoords[iRow]);
			std::pair<tools::ConversionResult, int> yCoord = tools::conv_stoi(yCoords[iRow]);
			string stageName = stageNames[iRow];
			std::pair<tools::ConversionResult, double> density = tools::conv_stod(densities[iRow]);

			if(validStageNames.find(stageName) == validStageNames.end()) {
				message = "ERROR IN FILE ";
				message.append(filename);
				message.append("-> INVALID STAGE NAME ");
				message.append(stageName);
				message.append(". VALID NAMES ARE:");
				for(auto validStageName : validStageNames) {
					message.append(" ");
					message.append(validStageName);
				}
				valid = false;
				break;
			}

			if(xCoord.first == tools::ConversionResult::EMPTY || xCoord.first == tools::ConversionResult::INVALID || xCoord.second < 1) {
				message = "ERROR IN FILE ";
				message.append(filename);
				message.append("-> EMPTY OR INVALID VALUE FOR PARAMETER xCoord IN GRASSLAND CELL #");
				message.append(std::to_string(iRow + 1));
				message.append(". PLEASE SPECIFY A VALUE > 0");
				valid = false;
				break;
			}
			if(yCoord.first == tools::ConversionResult::EMPTY || yCoord.first == tools::ConversionResult::INVALID || yCoord.second < 1) {
				message = "ERROR IN FILE ";
				message.append(filename);
				message.append("-> EMPTY OR INVALID VALUE FOR PARAMETER yCoord IN GRASSLAND CELL #");
				message.append(std::to_string(iRow + 1));
				message.append(". PLEASE SPECIFY A VALUE > 0");
				valid = false;
				break;
			}
			if(density.first == tools::ConversionResult::EMPTY || density.first == tools::ConversionResult::INVALID || density.second < 0) {
				message = "ERROR IN FILE ";
				message.append(filename);
				message.append("-> EMPTY OR INVALID VALUE FOR PARAMETER density IN GRASSLAND CELL #");
				message.append(std::to_string(iRow + 1));
				message.append(". PLEASE SPECIFY A VALUE >= 0");
				valid = false;
				break;
			}
			Point coord(xCoord.second - 1, yCoord.second - 1);
			unordered_map<string, double> stageDensities;
			if(densitiesByCoord.find(coord) != densitiesByCoord.end()) {
				stageDensities = densitiesByCoord[coord];
			}

			if(stageDensities.find(stageName) != stageDensities.end()) {
				message = "ERROR IN FILE ";
				message.append(filename);
				message.append("-> DUPLICATE DEFINITION OF DENSITY FOR STAGE ");
				message.append(stageName);
				message.append(" AT COORDINATE [");
				message.append(std::to_string(xCoord.second));
				message.append(",");
				message.append(std::to_string(yCoord.second));
				message.append("]");
				valid = false;
				break;
			}
			stageDensities[stageName] = density.second;

			densitiesByCoord[coord] = stageDensities;
			coords.insert(coord);
		}
		if(valid && densitiesByCoord.size() == 0) {
			empty = true;
		}
	}
}

unordered_map<string, double> CSVHandlerDensities::getDensitiesAt(Point coord) {
	if(densitiesByCoord.find(coord) != densitiesByCoord.end()) {
		return densitiesByCoord[coord];
	}
	return unordered_map<string, double>();
}

unordered_set<Point> CSVHandlerDensities::getCoords() {
	return coords;
}

CSVHandlerDisturbanceScheme::CSVHandlerDisturbanceScheme(string filename,
		unordered_set<string> validDisturbanceNames)
		: CSVHandlerDefault(filename, {"xCoord", "yCoord", "disturbanceName", "fixed", "prob"})
{
	if(!valid) {
		message.append("\nPlease supply valid or empty file ");
		message.append(filename);
		return;
	}
	if(!(empty || missingHeaders)){

		vector<string> xCoords = getValues(headerNames[0]);
		vector<string> yCoords = getValues(headerNames[1]);
		vector<string> disturbanceNames = getValues(headerNames[2]);
		vector<string> fixed = getValues(headerNames[3]);
		vector<string> probs = getValues(headerNames[4]);

		for(unsigned int iRow = 0; iRow < xCoords.size(); iRow++) {
			std::pair<tools::ConversionResult, int> xCoord = tools::conv_stoi(xCoords[iRow]);
			std::pair<tools::ConversionResult, int> yCoord = tools::conv_stoi(yCoords[iRow]);
			string disturbanceName = disturbanceNames[iRow];
			std::pair<tools::ConversionResult, int> numFixed = tools::conv_stoi(fixed[iRow]);
			std::pair<tools::ConversionResult, double> prob = tools::conv_stod(probs[iRow]);

			bool emptyCoord = xCoord.first == tools::ConversionResult::EMPTY && yCoord.first == tools::ConversionResult::EMPTY;

			if(!emptyCoord) {
				if(xCoord.first == tools::ConversionResult::EMPTY || yCoord.first == tools::ConversionResult::EMPTY) {
					message = "SPECIFIED EMPTY VALUE FOR PARAMETER xCoord OR yCoord IN GRASSLAND CELL #";
					message.append(std::to_string(iRow + 1));
					valid = false;
					break;
				}
				if(numFixed.first == tools::ConversionResult::EMPTY) {
					message = "SPECIFIED EMPTY VALUE FOR PARAMETER fixed IN GRASSLAND CELL #";
					message.append(std::to_string(iRow + 1));
					valid = false;
					break;
				}

				if(xCoord.first == tools::ConversionResult::INVALID || xCoord.second < 1) {
					message = "SPECIFIED INVALID VALUE FOR PARAMETER xCoord IN GRASSLAND CELL #";
					message.append(std::to_string(iRow + 1));
					message.append(". PLEASE PROVIDE A VALUE > 0");
					valid = false;
					break;
				}
				if(yCoord.first == tools::ConversionResult::INVALID || yCoord.second < 1) {
					message = "SPECIFIED INVALID VALUE FOR PARAMETER yCoord IN GRASSLAND CELL #";
					message.append(std::to_string(iRow + 1));
					message.append(". PLEASE PROVIDE A VALUE > 0");
					valid = false;
					break;
				}
			}
			if(disturbanceName != Constants::NAME_DISTURBANCE_NONE && validDisturbanceNames.find(disturbanceName) == validDisturbanceNames.end()) {
				message = "ERROR: DISTURBANCE NAME ";
				message.append(disturbanceName);
				message.append(" IS NOT IN LIST OF VALID DISTURBANCE NAMES:");
				for(auto validDistName : validDisturbanceNames) {
					message.append(" ");
					message.append(validDistName);
				}
				valid = false;
				break;
			}
			if(numFixed.first == tools::ConversionResult::INVALID || (numFixed.second != 0 && numFixed.second != 1)) {
				message = "SPECIFIED INVALID VALUE FOR PARAMETER fixed IN GRASSLAND CELL #";
				message.append(std::to_string(iRow + 1));
				message.append(". PLEASE USE EITHER 0 (false) OR 1 (true) AS VALUE");
				valid = false;
				break;
			}
			if(prob.first != tools::ConversionResult::EMPTY && (prob.first == tools::ConversionResult::INVALID || (prob.second < 0.0 || prob.second > 1.0))) {
				message = "SPECIFIED INVALID VALUE FOR PARAMETER prob IN GRASSLAND CELL #";
				message.append(std::to_string(iRow + 1));
				message.append(". PLEASE DEFINE VALUE FROM 0 TO 1");
				valid = false;
				break;
			}

			Point* coord;
			if(emptyCoord) {
				coord = new Point();
				if(prob.first != tools::ConversionResult::EMPTY) {
					probsByDistName[disturbanceName] = prob.second;
					continue; // do not add disturbances that will be selected randomly to schemeByCoord
				}
			}else {
				coord = new Point(xCoord.second - 1, yCoord.second - 1);
			}

//			TODO check if coord already exists
			schemeByCoord[*coord] = pair<string, bool>(disturbanceName, numFixed.second == 1);
		}
		if(valid && schemeByCoord.size() == 0 && probsByDistName.size() == 0) {
			empty = true;
		}
	}
}

string CSVHandlerDisturbanceScheme::getDisturbanceNameAt(Point coord, mt19937* randGen) {
	if(schemeByCoord.find(coord) != schemeByCoord.end()) {
		return schemeByCoord[coord].first;
	}
	if(Point() != coord && randGen) {
		uniform_real_distribution<double> uniDist(0, 1);
		for(auto pairDistProb : probsByDistName) {
			if(uniDist(*randGen) < pairDistProb.second) {
				return pairDistProb.first;
			}
		}
	}
	if(schemeByCoord.find(Point()) != schemeByCoord.end()) {
		return schemeByCoord[Point()].first;
	}
	return Constants::NAME_DISTURBANCE_NONE;
}

bool CSVHandlerDisturbanceScheme::isFixedAt(Point coord) {
	if(schemeByCoord.find(coord) != schemeByCoord.end()) {
		return schemeByCoord[coord].second;
	}
	if(schemeByCoord.find(Point()) != schemeByCoord.end()) {
		return schemeByCoord[Point()].second;
	}
	return true;
}

CSVHandlerDisturbance::CSVHandlerDisturbance(string filename)
		: CSVHandlerDefault(filename, {"year", "day", "xCoord", "yCoord", "disturbanceName","iLonClim","iLatClim"})
{
	if(!valid) {
		message.append("\nPlease supply valid or empty file ");
		message.append(filename);
		return;
	}
	if(empty || missingHeaders) {
		if(missingHeaders) {
			valid = false;
		}
	}else{
		vector<string> loadedDisturbanceNames = getValues(headerNames[4]);
		for(string loadedDisturbanceName : loadedDisturbanceNames) {
			if(loadedDisturbanceName == Constants::NAME_DISTURBANCE_NONE) {
				valid = false;
				message = "INVALID disturbance name '";
				message.append(Constants::NAME_DISTURBANCE_NONE);
				message.append("'");
				break;
			}
			disturbanceNames.insert(loadedDisturbanceName);
		}
	}
	if(valid) {
		handleTiming();
	}
}

void CSVHandlerDisturbance::handleTiming() {
	unordered_map<int, vector<int>> timingByYear;
	vector<string> years = getValues(headerNames[0]);
	vector<string> days = getValues(headerNames[1]);
	vector<string> xCoords = getValues(headerNames[2]);
	vector<string> yCoords = getValues(headerNames[3]);
	vector<string> disturbanceNames = getValues(headerNames[4]);
	vector<string> iLonsClim = getValues(headerNames[5]);
	vector<string> iLatsClim = getValues(headerNames[6]);
	unsigned int nRows = disturbanceNames.size();
	unsigned int qRows = nRows / 20;
	qRows = qRows > 0 ? qRows : 1;
	for(unsigned int iRow = 0; iRow < disturbanceNames.size(); iRow++) {
		string disturbanceName = disturbanceNames[iRow];
		std::pair<tools::ConversionResult, int> year = tools::conv_stoi(years[iRow]);
		std::pair<tools::ConversionResult, int> day = tools::conv_stoi(days[iRow]);
		std::pair<tools::ConversionResult, int> xCoord = tools::conv_stoi(xCoords[iRow]);
		std::pair<tools::ConversionResult, int> yCoord = tools::conv_stoi(yCoords[iRow]);
		std::pair<tools::ConversionResult, int> iLonClim = tools::conv_stoi(iLonsClim[iRow]);
		std::pair<tools::ConversionResult, int> iLatClim = tools::conv_stoi(iLatsClim[iRow]);

		bool hasCoord = xCoord.first != tools::ConversionResult::EMPTY || yCoord.first != tools::ConversionResult::EMPTY;
		bool hasClim = iLonClim.first != tools::ConversionResult::EMPTY || iLatClim.first != tools::ConversionResult::EMPTY;

		if(year.first == tools::ConversionResult::EMPTY) {
			year.second = -1;
		}
		if(xCoord.first == tools::ConversionResult::EMPTY) {
			xCoord.second = -1;
		} else {
			xCoord.second -= 1;
		}
		if(yCoord.first == tools::ConversionResult::EMPTY) {
			yCoord.second = -1;
		} else {
			yCoord.second -= 1;
		}
		if(iLonClim.first == tools::ConversionResult::EMPTY) {
			iLonClim.second = -1;
		} else {
			iLonClim.second -= 1;
		}
		if(iLatClim.first == tools::ConversionResult::EMPTY) {
			iLatClim.second = -1;
		} else {
			iLatClim.second -= 1;
		}

		if(hasClim && !hasCoord) {
			Point climCoord(iLonClim.second, iLatClim.second);
			unordered_map<int, vector<int>> timingByYear;
			unordered_map<Point, unordered_map<int, vector<int>>> climCoordTiming;
			if(climCoordTimingByDisturbance.find(disturbanceName) != climCoordTimingByDisturbance.end()) {
				climCoordTiming = climCoordTimingByDisturbance[disturbanceName];
				if(climCoordTiming.find(climCoord) != climCoordTiming.end()) {
					timingByYear =  climCoordTiming[climCoord];
				}
			}
			timingByYear[year.second].push_back(day.second);
			climCoordTiming[climCoord] = timingByYear;
			climCoordTimingByDisturbance[disturbanceName] = climCoordTiming;
		}else{
			Point coord(xCoord.second, yCoord.second);
			unordered_map<int, vector<int>> timingByYear;
			unordered_map<Point, unordered_map<int, vector<int>>> coordTiming;
			if(coordTimingByDisturbance.find(disturbanceName) != coordTimingByDisturbance.end()) {
				coordTiming = coordTimingByDisturbance[disturbanceName];
				if(coordTiming.find(coord) != coordTiming.end()) {
					timingByYear =  coordTiming[coord];
				}
			}
			timingByYear[year.second].push_back(day.second);
			coordTiming[coord] = timingByYear;
			coordTimingByDisturbance[disturbanceName] = coordTiming;
		}
		if(iRow % qRows == 0) {
			cout << "... " << iRow << "/" << nRows << " rows  loaded (" << round(100.0 * iRow / ((double) nRows)) << "% )" << endl;
		}
	}
}

unordered_set<string> CSVHandlerDisturbance::getDisturbanceNames() {
	return disturbanceNames;
}

unordered_map<int, vector<int> > CSVHandlerDisturbance::getTimingByYear(
		Point* coord, Point* climCoord, string disturbanceName) {
	unordered_map<int, vector<int>> timingByYear;

	if(coordTimingByDisturbance.find(disturbanceName) != coordTimingByDisturbance.end()) {
		unordered_map<Point, unordered_map<int, vector<int>>> coordTiming = coordTimingByDisturbance[disturbanceName];
		vector<Point> coords = {*coord, Point(-1, coord->getY()), Point(coord->getX(), -1), Point(-1,-1)};
		for(auto gCoord : coords) {
			if(coordTiming.find(gCoord) != coordTiming.end()) {
				for(auto yearDayPair : coordTiming[gCoord]) {
					timingByYear[yearDayPair.first].insert(timingByYear[yearDayPair.first].end(), yearDayPair.second.begin(), yearDayPair.second.end());
				}
			}
		}
	}

	if(climCoordTimingByDisturbance.find(disturbanceName) != climCoordTimingByDisturbance.end()) {
		unordered_map<Point, unordered_map<int, vector<int>>> climCoordTiming = climCoordTimingByDisturbance[disturbanceName];
		vector<Point> climCoords = {*climCoord, Point(-1, coord->getY()), Point(coord->getX(), -1)};
		for(auto cCoord : climCoords) {
			if(climCoordTiming.find(cCoord) != climCoordTiming.end()) {
				for(auto yearDayPair : climCoordTiming[cCoord]) {
					timingByYear[yearDayPair.first].insert(timingByYear[yearDayPair.first].end(), yearDayPair.second.begin(), yearDayPair.second.end());
				}
			}
		}
	}

	return timingByYear;
}

CSVHandlerInfluences::CSVHandlerInfluences(string filename)
		: CSVHandlerDefault(filename, {"name", "type", "target", "targetName", "targetProcess", "variable", "minFactor", "maxFactor", "mode", "coeff1", "coeff2", "coeff3", "coeff4", "initDevState", "keepDev"})
{
	if(!valid) {
		message.append("\nPlease supply valid or empty file ");
		message.append(filename);
		return;
	}
	if(!empty) {
		valid = handleInfluences();
	}
}

bool CSVHandlerInfluences::handleInfluences() {
	if(!valid || missingHeaders) {
		message = "INVALID INFLUENCE INPUT FILE\n";
		message.append("\n");
		message.append("*** Instructions for valid Influence input file ***\n");
		message.append("\n");
		message.append("Filename: influences.csv\n");
		message.append("Required headers:\n");
		for (string headerName : validHeaderNames) {
			message.append("-");
			message.append(headerName);
			message.append("\n");
		}
		message.append("Definition of at least 1 Influence");
		return false;
	}
	vector<string> inflNames = getValues(headerNames[0]);
	vector<string> types = getValues(headerNames[1]);
	vector<string> targets = getValues(headerNames[2]);
	vector<string> targetNames = getValues(headerNames[3]);
	vector<string> targetProcesses = getValues(headerNames[4]);
	vector<string> variables = getValues(headerNames[5]);
	vector<string> minFactors = getValues(headerNames[6]);
	vector<string> maxFactors = getValues(headerNames[7]);
	vector<string> modes = getValues(headerNames[8]);
	vector<string> coeffs1 = getValues(headerNames[9]);
	vector<string> coeffs2 = getValues(headerNames[10]);
	vector<string> coeffs3 = getValues(headerNames[11]);
	vector<string> coeffs4 = getValues(headerNames[12]);
	vector<string> initDevStates = getValues(headerNames[13]);
	vector<string> keepsDevs = getValues(headerNames[14]);

	bool flowInfluence = true;
	bool stageInfluence = true;
	unordered_map<string, Influence*> inflsByTargetProcess;
	MeasureHandler* mHandler = NULL;
	for (unsigned int iInfl = 0; iInfl < inflNames.size(); ++iInfl) {
		string inflName = inflNames[iInfl];
		std::pair<bool, int> result = tools::findInVector<string>(Constants::VALID_INFLUENCE_PROCESS_NAMES, inflName);
		if(!result.first) {
			message = "INVALID INFLUENCE NAME '";
			message.append(inflName);
			message.append("'. VALID NAMES ARE: ");
			for(unsigned int iName = 0; iName < Constants::VALID_INFLUENCE_PROCESS_NAMES.size(); iName++) {
				message.append("'");
				message.append(Constants::VALID_INFLUENCE_PROCESS_NAMES[iName]);
				message.append("'");
				if(iName < (Constants::VALID_INFLUENCE_PROCESS_NAMES.size() - 1)){
					message.append(", ");
				}
			}
			return false;
		}
		int iInflName = result.second;

		string type = types[iInfl];
		result = tools::findInVector<string>(Constants::VALID_INFLUENCE_TYPES, type);
		if(!result.first) {
			message = "INVALID INFLUENCE TYPE ";
			message.append(type);
			message.append("'. VALID TYPES ARE: ");
			for(unsigned int iName = 0; iName < Constants::VALID_INFLUENCE_TYPES.size(); iName++) {
				message.append("'");
				message.append(Constants::VALID_INFLUENCE_TYPES[iName]);
				message.append("'");
				if(iName < (Constants::VALID_INFLUENCE_TYPES.size() - 1)){
					message.append(", ");
				}
			}
			return false;
		}
		bool multiplicative = Constants::VALID_INFLUENCE_TYPES[result.second] == Constants::VALID_INFLUENCE_TYPES[1];

		string target = targets[iInfl];
		result = tools::findInVector<string>(Constants::VALID_INFLUENCE_TARGETS, target);
		if(!result.first) {
			message = "INVALID TARGET ";
			message.append(target);
			message.append("'. VALID TARGETS ARE: ");
			for(unsigned int iName = 0; iName < Constants::VALID_INFLUENCE_TARGETS.size(); iName++) {
				message.append("'");
				message.append(Constants::VALID_INFLUENCE_TARGETS[iName]);
				message.append("'");
				if(iName < (Constants::VALID_INFLUENCE_TARGETS.size() - 1)){
					message.append(", ");
				}
			}
			return false;
		}

		string targetName = targetNames[iInfl];

		string targetProcess = targetProcesses[iInfl];
		switch (result.second) {
			case 0: // Stage
			{
				stageInfluence = true;
				auto itFind = Constants::MAPPED_VALID_FLOW_TYPE_NAMES.find(targetProcess);
				flowInfluence = itFind != Constants::MAPPED_VALID_FLOW_TYPE_NAMES.end();
				if (!flowInfluence && targetProcess != "dev") {
					message = "INVALID TARGET PROCESS ";
					message.append(targetProcess);
					return false;
				}
				break;
			}
			case 1: // Influence
				stageInfluence = false;
				break;
//				TODO: handle valid Influence target processes
			default:
				break;
		}

		bool isClimParam = false;
		bool isDensLevel = false;
		ClimateVariable climParam = ClimateVariable::TS;
		DensityLevel densLevel = DensityLevel::POPULATION;
		result = tools::findInVector<string>(Constants::VALID_INFLUENCE_PROCESS_NAMES_VARIABLE, inflName);
		if(result.first) {
			string sVariable = variables[iInfl];
			auto itFindClim = Constants::CLIMATE_VARIABLE_NAMES.find(sVariable);
			if (itFindClim != Constants::CLIMATE_VARIABLE_NAMES.end()) {
				climParam = (*itFindClim).second;
				appliedClimParams.insert(climParam);
				isClimParam = true;
			} else {
				auto itFindDens = Constants::DENSITY_LEVEL_NAMES.find(sVariable);
				if (itFindDens != Constants::DENSITY_LEVEL_NAMES.end()) {
					densLevel = (*itFindDens).second;
					isDensLevel = true;
				}
			}
			if(!isClimParam && !isDensLevel) {
				message = "INVALID VARIABLE ";
				message.append(sVariable);
				return false;
			}
		}

		std::pair<tools::ConversionResult, double> minFactor;
		std::pair<tools::ConversionResult, double> maxFactor;
		std::pair<tools::ConversionResult, double> coeff1;
		std::pair<tools::ConversionResult, double> coeff2;
		std::pair<tools::ConversionResult, double> coeff3;
		std::pair<tools::ConversionResult, double> coeff4;
		std::pair<tools::ConversionResult, double> initDevState;
		std::pair<tools::ConversionResult, bool> keepDev;
		string sMinValue = minFactors[iInfl];
		string sMaxValue = maxFactors[iInfl];
		string sMode = modes[iInfl];
		string sCoeff1 = coeffs1[iInfl];
		string sCoeff2 = coeffs2[iInfl];
		string sCoeff3 = coeffs3[iInfl];
		string sCoeff4 = coeffs4[iInfl];
		string sInitDevState = initDevStates[iInfl];
		string sKeepDev = keepsDevs[iInfl];

		minFactor = tools::conv_stod(sMinValue);
		if(minFactor.first == tools::ConversionResult::INVALID) {
			message = "INVALID DOUBLE FOR MIN VALUE ";
			message.append(sMinValue);
			return false;
		}
		if(minFactor.first == tools::ConversionResult::EMPTY) {
			minFactor.second = 0.0;
		}

//		if (targetProcess != "dev" && minValue < 0 ) {
//			message = "INVALID NEGATIVE MIN VALUE";
//			return false;
//		}
//
		maxFactor = tools::conv_stod(sMaxValue);
		if(maxFactor.first == tools::ConversionResult::INVALID) {
			message = "INVALID DOUBLE FOR MAX VALUE ";
			message.append(sMaxValue);
			return false;
		}
		if(maxFactor.first == tools::ConversionResult::EMPTY) {
			maxFactor.second = 1.0;
		}

//		if (targetProcess != "repr" && maxValue > 1.0 ) {
//			message = "INVALID MAX VALUE > 1";
//			return false;
//		}
//
		coeff1 = tools::conv_stod(sCoeff1);
		if(coeff1.first == tools::ConversionResult::INVALID) {
			message = "INVALID DOUBLE FOR COEFF1 ";
			message.append(sMaxValue);
			return false;
		}

		coeff2 = tools::conv_stod(sCoeff2);
		if(coeff2.first == tools::ConversionResult::INVALID) {
			message = "INVALID DOUBLE FOR COEFF3 ";
			message.append(sMaxValue);
			return false;
		}

		coeff3 = tools::conv_stod(sCoeff3);
		if(coeff3.first == tools::ConversionResult::INVALID) {
			message = "INVALID DOUBLE FOR COEFF3 ";
			message.append(sMaxValue);
			return false;
		}

		coeff4 = tools::conv_stod(sCoeff4);
		if(coeff4.first == tools::ConversionResult::INVALID) {
			message = "INVALID DOUBLE FOR COEFF4 ";
			message.append(sMaxValue);
			return false;
		}

		initDevState = tools::conv_stod(sInitDevState);
		keepDev = tools::conv_stob(sKeepDev);
		if(!flowInfluence) {
			if(initDevState.first == tools::ConversionResult::INVALID) {
				message = "INVALID DOUBLE FOR initDevSate ";
				message.append(sInitDevState);
				return false;
			}
			if(keepDev.first == tools::ConversionResult::INVALID) {
				message = "INVALID BOOLEAN FOR keepDev ";
				message.append(sKeepDev);
				return false;
			}

		}

		std::pair<string,Influence*> inflByTargetName;
		inflByTargetName.first = targetName;
		switch (iInflName) {
			case 0: // Disturbance
			case 1: // Static
				if(coeff1.first == tools::ConversionResult::EMPTY) {
					message = "UNDEFINED coeff1";
					return false;
				}
				if(coeff1.second < minFactor.second || coeff1.second > maxFactor.second) {
					message = "coeff1 OUT OF BOUNDS WITH MIN OR MAX VALUE";
					return false;
				}
				break;
			case 2: // Capacity
			case 3: // Linear
				if(coeff1.first == tools::ConversionResult::EMPTY) {
					message = "UNDEFINED coeff1";
					return false;
				}
				if(coeff2.first == tools::ConversionResult::EMPTY) {
					message = "UNDEFINED coeff2";
					return false;
				}
				break;
			case 4: // Exponential
				if(coeff1.first == tools::ConversionResult::EMPTY) {
					message = "UNDEFINED coeff1";
					return false;
				}
				if(coeff2.first == tools::ConversionResult::EMPTY) {
					message = "UNDEFINED coeff2";
					return false;
				}
				if(coeff3.first == tools::ConversionResult::EMPTY) {
					message = "UNDEFINED coeff3";
					return false;
				}
				if(coeff4.first == tools::ConversionResult::EMPTY) {
					message = "UNDEFINED coeff3";
					return false;
				}
				break;
			case 5: // Sigmoid
			case 6: // Threshold
				if(coeff1.first == tools::ConversionResult::EMPTY) {
					message = "UNDEFINED coeff1";
					return false;
				}
				if(coeff2.first == tools::ConversionResult::EMPTY) {
					message = "UNDEFINED coeff2";
					return false;
				}
				if(coeff3.first == tools::ConversionResult::EMPTY) {
					message = "UNDEFINED coeff3";
					return false;
				}
				break;
			default:
				break;
		}
		switch (iInflName) {
//		static inline const vector<string> validProcessNames = {"dist", "stat", "cap", "lin", "exp", "sig", "thd", "bin"};
			case 0: // Disturbance
				if(mHandler == NULL) {
					mHandler = new MeasureHandler();
				}
				inflByTargetName.second = new InfluenceDisturbance(coeff1.second, mHandler, maxFactor.second, minFactor.second, multiplicative, keepDev.second, initDevState.second);
				break;
			case 1: // Static
				inflByTargetName.second = new InfluenceStatic(coeff1.second, maxFactor.second, minFactor.second, multiplicative, keepDev.second, initDevState.second);
				break;
			case 2: // Capacity
				inflByTargetName.second = new InfluenceCarryingCapacity(coeff1.second, coeff2.second, maxFactor.second, minFactor.second, multiplicative);
				break;
			case 3: // Linear
			{
				vector<double> coeffs;
				coeffs.push_back(coeff1.second);
				coeffs.push_back(coeff2.second);
				if(isClimParam) {
					inflByTargetName.second = new InfluenceLinearRegression<ClimateVariable>(climParam, coeffs, maxFactor.second, minFactor.second, multiplicative, keepDev.second, initDevState.second);
				}
				else if(isDensLevel) {
					inflByTargetName.second = new InfluenceLinearRegression<DensityLevel>(densLevel, coeffs, maxFactor.second, minFactor.second, multiplicative, keepDev.second, initDevState.second);
				}
				break;
			}
			case 4: // Exponential
			{
				vector<double> coeffs;
				coeffs.push_back(coeff1.second);
				coeffs.push_back(coeff2.second);
				coeffs.push_back(coeff3.second);
				coeffs.push_back(coeff4.second);
				if(isClimParam) {
					inflByTargetName.second = new InfluenceExponentialRegression<ClimateVariable>(climParam, coeffs, maxFactor.second, minFactor.second, multiplicative, keepDev.second, initDevState.second);
				}
				else if(isDensLevel) {
					inflByTargetName.second = new InfluenceExponentialRegression<DensityLevel>(densLevel, coeffs, maxFactor.second, minFactor.second, multiplicative, keepDev.second, initDevState.second);
				}
				break;
			}
			case 5: // Sigmoid
			{
				vector<double> coeffs;
				coeffs.push_back(coeff1.second);
				coeffs.push_back(coeff2.second);
				coeffs.push_back(coeff3.second);
				if(isClimParam) {
					inflByTargetName.second = new InfluenceSigmoidRegression<ClimateVariable>(climParam, coeffs, maxFactor.second, minFactor.second, multiplicative, keepDev.second, initDevState.second);
				}
				else if(isDensLevel) {
					inflByTargetName.second = new InfluenceSigmoidRegression<DensityLevel>(densLevel, coeffs, maxFactor.second, minFactor.second, multiplicative, keepDev.second, initDevState.second);
				}
				break;
			}
			case 6: // Treshold
			{
				bool eqAbove = (sMode == "above");
				if(isClimParam) {
					inflByTargetName.second = new InfluenceThreshold<ClimateVariable>(climParam, coeff1.second, coeff2.second, coeff3.second, eqAbove, multiplicative, keepDev.second, initDevState.second);
				}
				else if(isDensLevel) {
					inflByTargetName.second = new InfluenceThreshold<DensityLevel>(densLevel, coeff1.second, coeff2.second, coeff3.second, eqAbove, multiplicative, keepDev.second, initDevState.second);
				}
				break;
			}
			case 7: // Binominal
			{
				Influence* influenceMean = inflsByTargetProcess["mean"];
				Influence* influenceSD = inflsByTargetProcess["SD"];
				if(influenceMean == NULL || influenceSD == NULL) {
					message = "UNDEFINED mean OR standard deviation FOR BINOMINAL INFLUENCE";
					return false;
				}
				inflByTargetName.second = new InfluenceBinomial(influenceMean, influenceSD, true, 0.01, maxFactor.second, minFactor.second, multiplicative, keepDev.second, initDevState.second);
				break;
			}
			default:
				break;
		}
		if(stageInfluence) {
			Influence* modInfl = inflsByTargetProcess["fact"];
			if(modInfl != NULL) {
				inflByTargetName.second->setModInfl(modInfl);
			}
			if(flowInfluence) {
				auto itFind = Constants::MAPPED_VALID_FLOW_TYPE_NAMES.find(targetProcess);
				FlowType type = (*itFind).second;
				flowInfluencesByType[type].push_back(inflByTargetName);
	//			flowInfluences.push_back(inflByTargetName);
			}else{
				devInfluences.push_back(inflByTargetName);
			}
			inflsByTargetProcess.clear();
		} else {
			inflsByTargetProcess[targetProcess] = inflByTargetName.second;
		}

	}
	return true;
}

unordered_map<FlowType,vector<std::pair<string,Influence*>>> CSVHandlerInfluences::getFlowInfluencesByType() {
	return flowInfluencesByType;
}

vector<std::pair<string,Influence*>> CSVHandlerInfluences::getDevInfluences() {
	return devInfluences;
}

unordered_set<ClimateVariable> CSVHandlerInfluences::getAppliedClimParams() {
	return appliedClimParams;
}

CSVHandlerLifeCycle::CSVHandlerLifeCycle(string filename)
		: CSVHandlerDefault(filename, {	"name",
										"initDens",
										"minDens",
										"maxAge",
										"multiCohort",
										"capConstr",
										"thdQuasiExtinct",
										"rateTrans",
										"rateRepr",
										"rateMort",
										"rateMigr",
										"migrRadius",
										"migrDecay",
										"migrSight",
										"migrPref",
										"baseImm",
										"baseImmDay",
										"baseImmDur"})
{
	iHeader = 0;
	valid = handleStages();
	if(valid) {
		valid = handleFlows();
	}
}

bool CSVHandlerLifeCycle::handleStages() {
	if(empty || !valid || missingHeaders) {
		message = "MISSING OR INVALID LIFE STAGE INPUT FILE\n";
		message.append("\n");
		message.append("*** Instructions for valid Life Stage input file ***\n");
		message.append("\n");
		message.append("Filename: either stages.csv or use -fnameStages <filename>\n");
		if(!empty) {
			message.append("Required headers:\n");
			for (string headerName : validHeaderNames) {
				message.append("-");
				message.append(headerName);
				message.append("\n");
			}
		}
		message.append("Definition of at least 2 Life Stages");
		return false;
	}
	vector<string> stageNames = getValues(headerNames[iHeader++]);
	uniqueStageNames.insert(stageNames.begin(), stageNames.end());
	vector<string> initDensities = getValues(headerNames[iHeader++]);
	vector<string> minDensities = getValues(headerNames[iHeader++]);
	vector<string> maxAges = getValues(headerNames[iHeader++]);
	vector<string> multiCohorts = getValues(headerNames[iHeader++]);
	vector<string> capConstraints = getValues(headerNames[iHeader++]);
	vector<string> thdsQuasiExtinct = getValues(headerNames[iHeader++]);
	if(stageNames.size() > uniqueStageNames.size()) {
		message = "DUPLICATE STAGE NAMES";
		return false;
	}
	for (unsigned int iStage = 0; iStage < stageNames.size(); ++iStage) {
		string stageName = stageNames[iStage];
		std::pair<tools::ConversionResult, double> convDouble;
		std::pair<tools::ConversionResult, int> convInt;

		convDouble = tools::conv_stod(initDensities[iStage]);
		if(convDouble.first == tools::ConversionResult::INVALID) {
			message = "INVALID VALUE FOR INIT DENS";
			return false;
		}
		double initDens = convDouble.second;

		convDouble = tools::conv_stod(minDensities[iStage]);
		if(convDouble.first == tools::ConversionResult::INVALID) {
			message = "INVALID VALUE FOR MIN DENS";
			return false;
		}
		double minDens = convDouble.second;

		convDouble = tools::conv_stod(thdsQuasiExtinct[iStage]);
		if(convDouble.first == tools::ConversionResult::INVALID) {
			message = "INVALID VALUE FOR THRESHOLD QUASI EXTINCT";
			return false;
		}
		// if empty, set to zero
		double thdQuasiExtinct =  convDouble.first == tools::ConversionResult::EMPTY ? 0.0 : convDouble.second;

		convInt = tools::conv_stoi(maxAges[iStage]);
		if(convInt.first == tools::ConversionResult::INVALID) {
			message = "INVALID VALUE FOR MAX AGE";
			return false;
		}
		int maxAge = convInt.second;

		string multiCohortType = multiCohorts[iStage];
		bool multiCohort = multiCohortType != "false";
		bool capConstr = capConstraints[iStage] == "true";

		if(initDens < 0) {
			message = "INVALID NEGATIVE INIT DENSITY FOR STAGE ";
			message.append(stageName);
			return false;
		}
		if(minDens < 0) {
			message = "INVALID NEGATIVE MIN DENSITY FOR STAGE ";
			message.append(stageName);
			return false;
		}
		if(initDens > 0 && initDens < minDens) {
			message = "INIT DENSITY > 0 BUT BELOW MIN DENSITY FOR STAGE ";
			message.append(stageName);
			return false;
		}
		if(thdQuasiExtinct < 0.0) {
			message = "INVALID NEGATIVE THRESHOLD QUASI EXTINCT FOR STAGE ";
			message.append(stageName);
			return false;
		}
		if(maxAge < 1) {
			message = "INVALID MAX AGE BELOW 1 FOR STAGE ";
			message.append(stageName);
			return false;
		}
		if(multiCohort && !(multiCohortType == "daily" || multiCohortType == "yearly" )) {
			message = "INVALID MULTI COHORT TYPE '";
			message.append(multiCohortType);
			message.append("'. CHOOSE 'false', 'daily' OR 'yearly'");
			return false;
		}

		LifeStage* lifeStage = new LifeStage(stageName, initDens, capConstr, !multiCohort, maxAge, minDens, thdQuasiExtinct);
		if(multiCohortType == "yearly") {
			Constraint* constrCohortCreation = new ConstraintCycle(lifeStage);
			lifeStage->setCohortCreationConstraint(constrCohortCreation);
		}
		lifeStages.push_back(lifeStage);
	}
	return true;
}

bool CSVHandlerLifeCycle::handleFlows() {
	vector<string> transRates = getValues(headerNames[iHeader++]);
	vector<string> reprRates = getValues(headerNames[iHeader++]);
	vector<string> mortRates = getValues(headerNames[iHeader++]);
	vector<string> migrRates = getValues(headerNames[iHeader++]);
	vector<string> migrRads = getValues(headerNames[iHeader++]);
	vector<string> sMigrDecays = getValues(headerNames[iHeader++]);
	vector<string> sMigrSights = getValues(headerNames[iHeader++]);
	vector<string> sMigrPrefs = getValues(headerNames[iHeader++]);
	vector<string> baseImms = getValues(headerNames[iHeader++]);
	vector<string> baseImmDays = getValues(headerNames[iHeader++]);
	vector<string> baseImmDurs = getValues(headerNames[iHeader++]);

	for (unsigned int iStage = 0; iStage < lifeStages.size(); ++iStage) {
		LifeStage* stageFrom = lifeStages[iStage];
		double rateTrans = stod(transRates[iStage]);
		double rateRepr = stod(reprRates[iStage]);
		double rateMort = stod(mortRates[iStage]);
		std::pair<tools::ConversionResult, double> migrRate = tools::conv_stod(migrRates[iStage]);
		std::pair<tools::ConversionResult, int> migrRad = tools::conv_stoi(migrRads[iStage]);
		std::pair<tools::ConversionResult, double> migrDecay = tools::conv_stod(sMigrDecays[iStage]);
		std::pair<tools::ConversionResult, double> migrSight = tools::conv_stod(sMigrSights[iStage]);
		std::pair<tools::ConversionResult, double> migrPref = tools::conv_stod(sMigrPrefs[iStage]);
		std::pair<tools::ConversionResult, double> baseImm = tools::conv_stod(baseImms[iStage]);
		std::pair<tools::ConversionResult, int> baseImmDay = tools::conv_stoi(baseImmDays[iStage]);
		std::pair<tools::ConversionResult, int> baseImmDur = tools::conv_stoi(baseImmDurs[iStage]);

		if(rateTrans < 0.0 || rateTrans > 1.0) {
			message = "INVALID TRANSFER RATE FOR STAGE ";
			message.append(stageFrom->getName());
			message.append(". CHOOSE VALUE BETWEEN 0 AND 1");
			return false;
		}
		if(rateRepr < 0.0) {
			message = "INVALID REPRODUCTION RATE BELOW ZERO FOR STAGE ";
			message.append(stageFrom->getName());
			return false;
		}
		if(rateMort < 0.0 || rateMort > 1.0) {
			message = "INVALID MORTALITY RATE FOR STAGE ";
			message.append(stageFrom->getName());
			message.append(". CHOOSE VALUE BETWEEN 0 AND 1");
			return false;
		}
		if(migrRate.first == tools::ConversionResult::INVALID ||
				(migrRate.first == tools::ConversionResult::VALID
						&& (migrRate.second < 0.0 || migrRate.second > 1.0))) {

			message = "INVALID MIGRATION RATE FOR STAGE ";
			message.append(stageFrom->getName());
			message.append(". CHOOSE VALUE BETWEEN 0 AND 1");
			return false;
		}
		if(migrRad.first == tools::ConversionResult::INVALID ||
				(migrRad.first == tools::ConversionResult::VALID && migrRad.second < 1)) {

			message = "INVALID MIGRATION RADIUS FOR STAGE ";
			message.append(stageFrom->getName());
			message.append(". CHOOSE VALUE > 0");
			return false;
		}
		if((migrRate.first == tools::ConversionResult::VALID || migrRad.first == tools::ConversionResult::VALID)
			&& (migrRate.first == tools::ConversionResult::EMPTY || migrRad.first == tools::ConversionResult::EMPTY)){
			message = "MISSING PARAMETER DEFINITION FOR MIGRATION OF STAGE ";
			message.append(stageFrom->getName());
			message.append(". PLEASE SPECIFY VALID VALUES FOR ALL PARAMETERS: migrRate, migrRadius");
			return false;
		}
		if(migrDecay.first == tools::ConversionResult::INVALID ||
				(migrDecay.first == tools::ConversionResult::VALID && (migrDecay.second < 0.0 || migrDecay.second > 1.0))) {

			message = "INVALID MIGRATION DECAY RATE FOR STAGE ";
			message.append(stageFrom->getName());
			message.append(". CHOOSE VALUE FROM 0 TO 1");
			return false;
		}
		if(migrSight.first == tools::ConversionResult::INVALID ||
				(migrSight.first == tools::ConversionResult::VALID && (migrSight.second < 0.0 || migrSight.second > 1.0))) {

			message = "INVALID MIGRATION SIGHT RATE FOR STAGE ";
			message.append(stageFrom->getName());
			message.append(". CHOOSE VALUE FROM 0 TO 1");
			return false;
		}
		if(migrPref.first == tools::ConversionResult::INVALID ||
				(migrPref.first == tools::ConversionResult::VALID && migrPref.second < 0.0)) {

			message = "INVALID MIGRATION SIGHT PREF FOR STAGE ";
			message.append(stageFrom->getName());
			message.append(". CHOOSE VALUE >= 0");
			return false;
		}
		if(baseImm.first == tools::ConversionResult::INVALID ||
				(baseImm.first == tools::ConversionResult::VALID && !(baseImm.second > 0))) {

			message = "INVALID BASE IMMIGRATION FOR STAGE ";
			message.append(stageFrom->getName());
			message.append(". CHOOSE VALUE > 0");
			return false;
		}
		if(baseImmDay.first == tools::ConversionResult::INVALID ||
				(baseImmDay.first == tools::ConversionResult::VALID && baseImmDay.second < 1)) {

			message = "INVALID FIRST DAY FOR BASE IMMIGRATION FOR STAGE ";
			message.append(stageFrom->getName());
			message.append(". CHOOSE VALUE > 0");
			return false;
		}
		if(baseImmDur.first == tools::ConversionResult::INVALID ||
				(baseImmDur.first == tools::ConversionResult::VALID && baseImmDur.second < 1)) {

			message = "INVALID DURATION FOR BASE IMMIGRATION FOR STAGE ";
			message.append(stageFrom->getName());
			message.append(". CHOOSE VALUE > 0");
			return false;
		}
		if((baseImm.first == tools::ConversionResult::VALID || baseImmDay.first == tools::ConversionResult::VALID || baseImmDur.first == tools::ConversionResult::VALID)
			&& (baseImm.first == tools::ConversionResult::EMPTY || baseImmDay.first == tools::ConversionResult::EMPTY || baseImmDur.first == tools::ConversionResult::EMPTY)){
			message = "MISSING PARAMETER DEFINITION FOR BASE IMMIGRATION OF STAGE ";
			message.append(stageFrom->getName());
			message.append(". PLEASE SPECIFY VALID VALUES FOR ALL PARAMETERS: baseImm, baseImmDay, baseImmDur");
			return false;
		}

		Flow* flowMort = new Flow(stageFrom, NULL, rateMort);
		mortFlows.push_back(flowMort);

		if(iStage < (lifeStages.size() - 1)) {
			LifeStage* stageTo = lifeStages[iStage + 1];
			Flow* flowTrans = new Flow(stageFrom, stageTo, rateTrans);
			transFlows[iStage] = flowTrans;
		}

		if(rateRepr > 0 && iStage > 0) {
			LifeStage* stageTo = lifeStages[0];
			Flow* flowRepr = new Flow(stageFrom, FlowType::REPRODUCTION, stageTo, rateRepr);
			reprFlows[iStage] = flowRepr;
		}
		bool migratory = false;
		if(migrRate.first == tools::ConversionResult::VALID && migrRad.first == tools::ConversionResult::VALID) {
			ratesMigr[iStage] = migrRate.second;
			radsMigr[iStage] = migrRad.second;
			migratory = true;
		}
		if(migrDecay.first == tools::ConversionResult::VALID) {
			migrDecays[iStage] = migrDecay.second;
		}
		if(migrSight.first == tools::ConversionResult::VALID) {
			migrSights[iStage] = migrSight.second;
		}
		if(migrPref.first == tools::ConversionResult::VALID) {
			migrPrefs[iStage] = migrPref.second;
		}
		if(baseImm.first == tools::ConversionResult::VALID) {
			string stageDummyBaseImmName = stageFrom->getName();
			stageDummyBaseImmName.append("BaseImm");
			LifeStage* stageDummyBaseImm = new LifeStage(stageDummyBaseImmName, baseImm.second, false, true, 1000000, baseImm.second, 0.0, true);
			Flow* flowBaseImm = new Flow(stageDummyBaseImm, FlowType::MIGRATION, stageFrom, 1);
			flowBaseImm->addInfluence(new InfluenceTimeWindow(baseImmDay.second, baseImmDur.second));
			baseImmFlows.push_back(flowBaseImm);
			migratory = true;
		}
		stageFrom->setMigratory(migratory);
	}

	return true;
}

vector<LifeStage*> CSVHandlerLifeCycle::getLifeStages() {
	return lifeStages;
}

unordered_set<string> CSVHandlerLifeCycle::getStageNames() {
	return uniqueStageNames;
}


Flow* CSVHandlerLifeCycle::getTransFlow(int iStage) {
	return transFlows[iStage];
}

Flow* CSVHandlerLifeCycle::getReprFlow(int iStage) {
	return reprFlows[iStage];
}

Flow* CSVHandlerLifeCycle::getMortFlow(int iStage) {
	return mortFlows[iStage];
}

double CSVHandlerLifeCycle::getMigrRate(unsigned int iStage) {
	if(ratesMigr.count(iStage) > 0) {
		return ratesMigr[iStage];
	}
	return 0;
}

int CSVHandlerLifeCycle::getMigrRad(unsigned int iStage) {
	if(radsMigr.count(iStage) > 0) {
		return radsMigr[iStage];
	}
	return 0;
}

double CSVHandlerLifeCycle::getMigrDecay(unsigned int iStage) {
	if(migrDecays.count(iStage) > 0) {
		return migrDecays[iStage];
	}
	return 0.5; // default migration decay
}

double CSVHandlerLifeCycle::getMigrSight(unsigned int iStage) {
	if(migrSights.count(iStage) > 0) {
		return migrSights[iStage];
	}
	return 0.5; // default migration sight
}

double CSVHandlerLifeCycle::getMigrPref(unsigned int iStage) {
	if(migrPrefs.count(iStage) > 0) {
		return migrPrefs[iStage];
	}
	return 1.0; // default migration pref
}

vector<Flow*> CSVHandlerLifeCycle::getBaseImmFlows() {
	return baseImmFlows;
}
