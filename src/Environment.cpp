/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Environment.h"

Environment::Environment(vector<StrategyClimate*> climateStrategies, unordered_set<Cell*> grasslandCells, int timestep, int deltaTimesteps, int habitatWidth, int climateCellWidth, int cycleLength, int seed) {
	this->habitatWidth = habitatWidth > 0 ? habitatWidth : Constants::GRASSLAND_CELL_WIDTH;
	this->climateCellWidth = climateCellWidth > 0 ? climateCellWidth : Constants::CLIMATE_CELL_WIDTH;
	this->timestep = timestep;
	this->deltaTimesteps = deltaTimesteps;
	this->initialTimestep = timestep;
	this->cycleLength = cycleLength > 0 ? cycleLength : Constants::DAYS_PER_YEAR;
	initGrasslandCells(grasslandCells);
	initClimate(climateStrategies);
	setSeed(seed);
	updateIndexTime(true);
//	get current timestamp from system clock
	runID = runID < 0 ? static_cast<long int>(time(NULL)) : runID;
}

bool Environment::update() {
	timestep++;
	return updateIndexTime();
}

bool Environment::updateIndexTime(bool force) {
	if(force || timestep % cycleLength == 0) {
		indexTime = timestep;
//		unsigned int iCycle = getCycle(timestep - initialTimestep) - 1;
		unsigned int indexYear = Constants::BASE_YEAR + getCycle(timestep) - 1;
		if(orderOfYears.find(indexYear) != orderOfYears.end()) {
			int initialYear = Constants::BASE_YEAR + getInitialCycle() - 1;
			int newYear = orderOfYears[indexYear];
//			TODO remove cout again?
			cout << "Setting year #" << indexYear << "=" << newYear << endl;
			int diffYears = newYear - initialYear;
			int day = indexTime % cycleLength;
			size_t tTime = initialTimestep + day + diffYears * cycleLength;
			indexTime = tTime < climate->getNTimesteps() && tTime >= 0 ? tTime : indexTime;
		}
		return true;
	}
	else {
		indexTime++;
		return false;
	}
}

void Environment::initClimate(vector<StrategyClimate*> climateStrategies) {
	climate = new Climate(climateStrategies);
}

void Environment::initGrasslandCells(unordered_set<Cell*> grasslandCells) {
	for(auto cell : grasslandCells) {
		Point grasslandCoord = cell->getCoord();
		Point climateCoord = cell->getClimCoord();
		minX = grasslandCoord.getX() < minX ? grasslandCoord.getX() : minX;
		minY = grasslandCoord.getY() < minY ? grasslandCoord.getY() : minY;
		minLon = climateCoord.getX() < minLon ? climateCoord.getX() : minLon;
		minLat = climateCoord.getY() < minLat ? climateCoord.getY() : minLat;
		maxX = grasslandCoord.getX() > maxX ? grasslandCoord.getX() : maxX;
		maxY = grasslandCoord.getY() > maxY ? grasslandCoord.getY() : maxY;
		maxLon = climateCoord.getX() > maxLon ? climateCoord.getX() : maxLon;
		maxLat = climateCoord.getY() > maxLat ? climateCoord.getY() : maxLat;
		cellsByCoord[grasslandCoord] = cell;
		grasslandCoords.emplace(grasslandCoord);
	}
}

unordered_set<Point> Environment::getGrasslandCoords() {
	return grasslandCoords;
}

int Environment::getTimestep() {
	return timestep;
}

void Environment::setTimestep(int timestep) {
	this->timestep = timestep;
	updateIndexTime(true);
}

int Environment::getInitialTimestep() {
	return initialTimestep;
}

int Environment::getDeltaTimesteps() {
	return deltaTimesteps;
}

void Environment::setInitialTimestep(int initialTimestep) {
	this->initialTimestep = initialTimestep;
}

int Environment::getCycle(int reference) {
	if(reference < 0) {
		reference = timestep;
	}
	//INFO: not sure why I did this in the first place
	//Hopefully not problems occur from commenting this out
	//Probably to prevent division by zero if cycle length is still 0
//	if(reference == initialTimestep) {
//		return 0;
//	}
	return 1 + reference / cycleLength;
}

int Environment::getInitialCycle() {
	return getCycle(initialTimestep);
}

int Environment::getFinalCycle() {
	return getCycle(climate->getNTimesteps() - 1);
}

int Environment::getCycleLength() {
	return cycleLength;
}

int Environment::getDayOfCycle() {
	return timestep % cycleLength;
}

int Environment::getDayOfVegetation(Point coord) {
	Cell* cell = activeCellsByCoord[coord];
	if(cell == NULL) {
		return -1;
	}
	int cycleDay = getDayOfCycle();
	unsigned int lastUpdate = cell->getLastUpdateTimestep();
	int vegetationDay = cell->getDayOfVegetation();
	float temperatureSum = cell->getTemperatureSum();
	int diffTime = timestep - lastUpdate;
	if(diffTime == 0) {
		if(timestep > 0) { // the last update was actually during the current timestep
			return vegetationDay;
		}
		diffTime = 1; // in case we are starting the simulation at timestep 0
	}
	if(getCycle(lastUpdate) < getCycle()) {
		vegetationDay = -1;
		temperatureSum = 0;
		diffTime = getDayOfCycle();
	}
	for (int iDiff = 0; iDiff < diffTime; ++iDiff) {
		int iTimestep = timestep - iDiff;
		int iIndex = indexTime - iDiff;
		int iDay = cycleDay - iDiff;
		float factMonth = 1;
		if(iDay < 31) { // JANUARY
			factMonth = 0.5;
		}
		else if(iDay < 59) { // FEBRUARY
			factMonth = 0.75;
		}
		float temperature = climate->getValue(TS, iTimestep, iIndex, cell);
		temperature = temperature < 0.0 ? 0.0 : temperature;
		temperatureSum += factMonth * temperature;
		if(temperatureSum >= Constants::TEMPERATURE_SUM_VEGETATION_START) {
			vegetationDay++;
		}
	}
	cell->reset(timestep, vegetationDay, temperatureSum);
	return vegetationDay;
}

int Environment::getWeek(int reference) {
	if(reference < 0) {
		reference = timestep;
	}
	return (reference - 1) / 7 + 1;
}

int Environment::getWeekOfCylce() {
	return getWeek(timestep % cycleLength);
}

Climate* Environment::getClimate() {
	return climate;
}

Measure* Environment::getMeasure(Point coord) {
	vector<int> timing;

	bool uniformUnfixed = uniformDisturbances && fixedDisturbanceCoords.find(coord) == fixedDisturbanceCoords.end();
	if(activeCellsByCoord.find(coord) != activeCellsByCoord.end()) {
		if(uniformUnfixed) {
			return activeCellsByCoord[coord]->getMeasure(uniformDisturbanceID);
		}else{
			return activeCellsByCoord[coord]->getMeasure();
		}
	}
	else if(cellsByCoord.find(coord) != cellsByCoord.end()) {
		if(uniformUnfixed) {
			return cellsByCoord[coord]->getMeasure(uniformDisturbanceID);
		}else{
			return cellsByCoord[coord]->getMeasure();
		}
	}
	return new Measure(Constants::ID_DISTURBANCE_NONE, timing);
}

bool Environment::hasDisturbanceScheduled(Point coord) {
//	TODO handle exception if coord not in map
	return getMeasure(coord)->hasSchedule();
}


float Environment::getValue(ClimateVariable climVar, Point coord) {
	Cell* cell = activeCellsByCoord[coord];
	if(cell == NULL) {
		cell = cellsByCoord[coord];
	}
	float value = climate->getValue(climVar, timestep, indexTime, cell);
	return value;
}

void Environment::setDisturbanceAt(Point coord, unsigned int disturbanceID) {
	if(coord == Point()) {
		uniformDisturbances = true;
		uniformDisturbanceID = disturbanceID;
		fixedDisturbanceCoords.clear();
		return;
	}
	if(uniformDisturbances) {
		fixedDisturbanceCoords.insert(coord);
	}
	if(activeCellsByCoord.find(coord) != activeCellsByCoord.end()) {
		activeCellsByCoord[coord]->setActiveMeasure(disturbanceID);
	} else {
		cellsByCoord[coord]->setActiveMeasure(disturbanceID);
	}
////	auto itMeasuresAtCoord = measures.find(coord);
//	if(measures.find(coord) != measures.end()) {
////		auto itMeasures = itMeasuresAtCoord->second.find(disturbanceID);
//		if(measures[coord].find(disturbanceID) != measures[coord].end()) {
//			Benchmark::getInstance().mark(1011001);
//			Measure* measure = measures[coord][disturbanceID];
//			if(activeMeasures.find(coord) != activeMeasures.end()) {
//				activeCellsByCoord[coord]->setMeasure(measure);
//			} else {
//				cellsByCoord[coord]->setMeasure(measure);
//			}
//			Benchmark::getInstance().stop(1011001);
//		}
//	}
}

unsigned int Environment::getDisturbanceAt(Point coord) {
	if(uniformDisturbances && fixedDisturbanceCoords.find(coord) == fixedDisturbanceCoords.end()) {
		return uniformDisturbanceID;
	}
	if(activeCellsByCoord.find(coord) != activeCellsByCoord.end()) {
		return activeCellsByCoord[coord]->getMeasure()->getID();
	}
	return cellsByCoord[coord]->getMeasure()->getID();
}

void Environment::clearDisturbances() {
	uniformDisturbances = false;
	uniformDisturbanceID = Constants::ID_DISTURBANCE_NONE;
	fixedDisturbanceCoords.clear();
	activeNeighborhoods.clear();
	activeCellsByCoord.clear();
	for(auto cellAt : cellsByCoord) {
		cellAt.second->clearMeasures();
	}
}

bool Environment::addDisturbance(Point coord, unsigned int disturbanceID, unordered_map<int, vector<int>> disturbancesTimingByYear, bool dynamicDisturbanceTiming) {
	vector<int> timing;
//	empty timing vector => no disturbance
	Measure* measure = NULL;
	if(Constants::ID_DISTURBANCE_NONE != disturbanceID && disturbancesTimingByYear.size() > 0) {
		if(!disturbancesTimingByYear.empty()) {
			bool relativeTiming = false;
			if(disturbancesTimingByYear.size() == 1 && disturbancesTimingByYear.find(-1) != disturbancesTimingByYear.end()) {
				vector<int> days = disturbancesTimingByYear[-1];
				timing.insert(timing.end(), days.begin(), days.end());
				relativeTiming = true;
			} else {
				unsigned int iOrder = 0;
				for(auto yearPair : orderOfYears){
					int currYear =  yearPair.second;
					vector<int> days = disturbancesTimingByYear[currYear];
					vector<int> daysRel = disturbancesTimingByYear[iOrder + 1];
					days.insert(days.end(), daysRel.begin(), daysRel.end());
					for(int day : days) {
						if(day > 0) {
							int dayOfUse = initialTimestep + iOrder * cycleLength + day - 1;
							timing.push_back(dayOfUse);
						}
					}
					iOrder++;
				}
			}
			measure = new Measure(disturbanceID, timing, relativeTiming, dynamicDisturbanceTiming);
		}
	}
	if(!measure) {
		 measure =  new Measure(Constants::ID_DISTURBANCE_NONE, timing);
	}
	cellsByCoord[coord]->addMeasure(measure);
	return measure->hasSchedule();
//	TODO next line apparently only required in old version without setDisturbanceAt --> remove at some point?
//	cellsByCoord[coord]->setMeasure(measure);
}

mt19937* Environment::getRandGenLocal(Point coord){
	auto itRandGen = localRandGens.find(coord);
	if (itRandGen == localRandGens.end()) {
		return &randGenGlobal;
	}
	return &(itRandGen->second);
}

mt19937* Environment::getRandGenGlobal(){
	return &randGenGlobal;
}

void Environment::setSeed(int seed, unordered_set<Point> coords) {
	seedGlobal = seed;
	randGenGlobal.seed(seedGlobal);
	localRandGens.clear();
	localSeeds.clear();
	for(auto coord : coords) {
		int localSeed = 1000000000 + coord.getX() * 1000000 + coord.getY() * 1000 + seedGlobal;
		mt19937 localRandGen(localSeed);
		localRandGens[coord] = localRandGen;
		localSeeds[coord] = localSeed;
	}
}

CSVHandlerLifeCycle* Environment::getLifeCycleDefinition() const {
	return lifeCycleDefinition;
}

CSVHandlerInfluences* Environment::getInfluenceDefinitions() const {
	return influenceDefinitions;
}

void Environment::setLifeCycleDefinition(
		CSVHandlerLifeCycle *&lifeCycleDefinition) {
	this->lifeCycleDefinition = lifeCycleDefinition;
}

unsigned int Environment::getMaxLat() const {
	return maxLat;
}

unsigned int Environment::getMaxLon() const {
	return maxLon;
}

unsigned int Environment::getMaxX() const {
	return maxX;
}

unsigned int Environment::getMaxY() const {
	return maxY;
}

unsigned int Environment::getMinLat() const {
	return minLat;
}

unsigned int Environment::getMinLon() const {
	return minLon;
}

unsigned int Environment::getMinX() const {
	return minX;
}

Point Environment::getSoloPop() const {
	return soloPop;
}

void Environment::setSoloPop(Point soloPop) {
	this->soloPop = soloPop;
}

bool Environment::isSoloPop() {
	return soloPop != Point();
}

unsigned int Environment::getMinY() const {
	return minY;
}

void Environment::setInfluenceDefinitions(
		CSVHandlerInfluences *&influenceDefinitions) {
	this->influenceDefinitions = influenceDefinitions;
}

int Environment::getSeedLocal(Point coord) const {
	auto itSeed = localSeeds.find(coord);
	if (itSeed == localSeeds.end()) {
		return seedGlobal;
	}
	return itSeed->second;
}

void Environment::setOrderOfYears(const map<int, int>& orderOfYears) {
	this->orderOfYears = orderOfYears;
}

unsigned int Environment::getHabitatWidth() const {
	return habitatWidth;
}

unsigned int Environment::getHabitatSize() const {
	return habitatWidth * habitatWidth;
}

unsigned int Environment::getClimateCellWidth() const {
	return climateCellWidth;
}

unsigned int Environment::getClimateCellSize() const {
	return climateCellWidth * climateCellWidth;
}

void Environment::addActiveNeighbor(int key, Point activeNeighbor) {
//	unordered_map<Point, unordered_map<string, Measure*>> activeMeasures;
	unordered_map<Point, Population*> activeNeighborhood;
	if(activeNeighborhoods.find(key) != activeNeighborhoods.end()) {
		activeNeighborhood = activeNeighborhoods[key];
	}
	if(activeNeighborhood.find(activeNeighbor) == activeNeighborhood.end()) {
		activeNeighborhood[activeNeighbor] = neighborhoods[key].at(activeNeighbor);
		activeCellsByCoord[activeNeighbor] = cellsByCoord.at(activeNeighbor);
	}
	activeNeighborhoods[key] = activeNeighborhood;
}

void Environment::removeActiveNeighbor(int key, Point activeNeighbor) {
	activeNeighborhoods[key].erase(activeNeighbor);
	if(activeNeighborhoods[key].size() == 0) {
		activeCellsByCoord.erase(activeNeighbor);
	}
}

void Environment::clearActiveNeighbors(int key) {
	activeNeighborhoods[key].clear();
}

void Environment::addActiveNeighbors(int key, unordered_set<Point> activeNeighbors) {
//	unordered_map<Point, Population*> activeNeighborhood;
//	if(activeNeighborhoods.find(key) != activeNeighborhoods.end()) {
//		Benchmark::getInstance().mark(1013211);
//		activeNeighborhood = activeNeighborhoods[key];
//		Benchmark::getInstance().stop(1013211);
//	}
	for(auto activeNeighbor : activeNeighbors) {
		if(activeNeighborhoods[key].find(activeNeighbor) == activeNeighborhoods[key].end()) {
			activeNeighborhoods[key][activeNeighbor] = neighborhoods[key].at(activeNeighbor);
			activeCellsByCoord[activeNeighbor] = cellsByCoord.at(activeNeighbor);
		}
	}
//	activeNeighborhoods[key] = activeNeighborhood;
}

unordered_map<Point, Population*> Environment::getActiveNeighborhood(int key) {
	return activeNeighborhoods.at(key);
}

void Environment::addNeighborhood(int key,
		unordered_map<Point, Population*> neighbordhood) {
	neighborhoods[key] = neighbordhood;
}

unordered_map<Point, Population*> Environment::getNeighborhood(int key) {
	return neighborhoods.at(key);
}
