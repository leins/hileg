/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LandUseH
#define LandUseH

#include <vector>
#include <set>
#include "Constants.h"

using namespace std;

// forward declaration
class Measure;

enum UsageType {
	MOWING,
	CATTLE,
	BUFFALO,
	GOAT,
	SHEEP
};

enum MowingPatternType {
	ONE, TWO, THREE, FOUR, FIVE, NONE
};

class MowingPattern {
private:
	MowingPatternType type;
	int nStrips=0; // number of remaining strips
	int nBorderStrips=0; // number of remaining strips at the grid cell border
	int stripWidth=0; // width of each remaining strip in meters
public:
	MowingPattern(MowingPatternType type = NONE);
	int getBorderStrips();
	int getNStrips();
	MowingPatternType getType();
	int getStripWidth();
};

class Measure {
private:
	unsigned int ID;

	int duration;			// duration of each use in days

	UsageType typeOfUse;			// 0 = Mowing, 1 = Cattle, 2 = Buffalo ...

	int typeOfStrips;		// 0 = None/Pasture, 1 = Temporary grass, 2 = Flower, 3 = established grass
	MowingPattern* mowingPattern;		//

	bool insideOutMown;		// is the field mown concentrically starting from the inside
	int cuttingHeight;		// the grass cutting height in cm

	double stockingRate;	// livestock in GVE/ha

	bool fertilizerUsed;	// is fertilizer used during this measure?

	bool mandatory;			// is this measure mandatory?

	double payment;			// payment in EURO/ha the farmer receives for measure

	int weekOfLastUse;		// keeps track of the week the measure was last used
	int timestepOfLastUse;	// keeps track of the timestep the measure was last used

	bool relativeTiming; 	// measure timing is given as weeks relative to each year
	bool followVegetation; // measure timing is following the start of the vegetation period

	set<int> startingWeeks; 	// the starting weeks of all uses
	set<int> timing; 		// timing of the measure either relative to year or with absolute days
	unsigned int iNextUse = 0;			// index of the timing vector for the next use

	double mownRatio;

	Measure(unsigned int ID, bool relativeTiming, bool followVegetation = false, int durationDays = 0, UsageType typeOfUse = MOWING,
			int typeOfStrips = 0, MowingPatternType mowingPatternType = NONE, int widthOfStrips = 0,
			double stockingRate = 0, bool fertilizerUsed = true,
			bool mandatory = false, double payment = 0, bool insideOutMown = false, int cuttingHeight = 5);

public:
	Measure(unsigned int ID, vector<int> daysOfUse, bool relativeTiming = false, bool followVegetation = false, int durationDays = 0, UsageType typeOfUse = MOWING,
			int typeOfStrips = 0, MowingPatternType mowingPatternType = NONE, int widthOfStrips = 0,
			double stockingRate = 0, bool fertilizerUsed = true,
			bool mandatory = false, double payment = 0, bool insideOutMown = false, int cuttingHeight = 5);
	Measure(unsigned int ID, bool followVegetation = true, int weekOfFirstUse = 21, int deltaSecondUse = 6, int deltaThirdUse =
			6, int deltaFourthUse = 0, int durationWeeks = 0, UsageType typeOfUse = MOWING,
			int typeOfStrips = 0, MowingPatternType mowingPatternType = NONE, int widthOfStrips = 0,
			double stockingRate = 0, bool fertilizerUsed = true,
			bool mandatory = false, double payment = 0, bool insideOutMown = false, int cuttingHeight = 5);


	~Measure() {
		delete mowingPattern;
	}

	bool hasSchedule();
	int getDuration();
	bool isMandatory();
	MowingPattern* getMowingPattern();
	double getPayment();
	double getStockingRate();
	int getTypeOfStrips();
	UsageType getTypeOfUse();
	bool isFertilizerUsed();
	int getWeekOfLastUse();
	int getLastUse();
	int getNextUse();
	void resetLastUse(unsigned int timestep, int week);
	bool hasRelativeTiming();
	bool doesFollowVegetation();
	double getMownRatio();
	int getCuttingHeight();
	bool isInsideOutMown();
	unsigned int getID();
};

#endif

