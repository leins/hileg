/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TOOLS_H_
#define TOOLS_H_

#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <fstream>

namespace tools{
	/*
	--- Adapted from https://thispointer.com/c-how-to-find-an-element-in-vector-and-get-its-index/ ---

	Generic function to find an element in vector and also its position.
	It returns a pair of bool & int i.e.
	bool : Represents if element is present in vector or not.
	int : Represents the index of element in vector if its found else -1
	*/
	template < typename T>
	inline std::pair<bool, int > findInVector(const std::vector<T>  & vecOfElements, const T  & element)
	{
		std::pair<bool, int > result;
		// Find given element in vector
		auto it = std::find(vecOfElements.begin(), vecOfElements.end(), element);
		if (it != vecOfElements.end())
		{
			result.second = distance(vecOfElements.begin(), it);
			result.first = true;
		}
		else
		{
			result.first = false;
			result.second = -1;
		}
		return result;
	}

	enum ConversionResult {
		EMPTY,
		VALID,
		INVALID
	};

	inline std::pair<ConversionResult, bool> conv_stob(string convString) {
		std::pair<ConversionResult, int> retVals;
		retVals.first = VALID;
		retVals.second = false;
		if(convString.empty()) {
			retVals.first = EMPTY;
			return retVals;
		}
		retVals.second = convString == "true";
//		string is neither "true" nor "false"
		if(!retVals.second && convString != "false") {
			retVals.first = INVALID;
		}
		return retVals;
	}

	inline std::pair<ConversionResult, int> conv_stoi(string convString) {
		std::pair<ConversionResult, int> retVals;
		retVals.first = VALID;
		retVals.second = 0;
		if(convString.empty()) {
			retVals.first = EMPTY;
			return retVals;
		}
		char* endString;
		retVals.second = strtol(convString.c_str(),&endString, 10);
		if(*endString) {
			retVals.first = INVALID;
		}
		return retVals;
	}

	inline std::pair<ConversionResult, double> conv_stod(string convString) {
		std::pair<ConversionResult, double> retVals;
		retVals.first = VALID;
		retVals.second = 0.0;
		if(convString.empty()) {
			retVals.first = EMPTY;
			return retVals;
		}
		char* endString;
		retVals.second = strtod(convString.c_str(),&endString);
		if(*endString) {
			retVals.first = INVALID;
		}
		return retVals;
	}

	inline double normCDF(double x, double mean = 0.0, double SD = 1.0) {
//		return std::erfc(-x/std::sqrt(2))/2;
//		return 0.5 * (1 + std::erfc((x - mean) / (SD * sqrt(2.0)) ));
		return 0.5 * (std::erfc((mean - x) / (SD * sqrt(2.0)) ));
	}

	inline unsigned int strToHash(string str) {
	    std::hash<std::string> hasher;
	    return hasher(str);
	}
	inline void copyFile(string filenameSource, string foldernameTarget, string filenameTarget = "") {
		std::ifstream  source(filenameSource, std::ios::binary);
		if(filenameTarget.empty()) {
			filenameTarget = filenameSource;
		}
		std::ofstream  target(foldernameTarget.append("/").append(filenameTarget), std::ios::binary);
		string line = "";
		while (getline(source, line))
		{
			if (line.rfind("#", 0) == 0 || line.empty()) {
			  continue;
			}
			target << line << endl;
		}
//		target << source.rdbuf();
		target.close();
	}
}

#endif /* TOOLS_H_ */
