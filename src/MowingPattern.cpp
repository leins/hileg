/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Disturbance.h"

MowingPattern::MowingPattern(MowingPatternType type) {
	this->type = type;
	switch (type) {
		case MowingPatternType::ONE:
			nStrips = 1;
			nBorderStrips = 0;
			stripWidth = 4;
			break;
		case MowingPatternType::TWO:
			nStrips = 3;
			nBorderStrips = 2;
			stripWidth = 4;
			break;
		case MowingPatternType::THREE:
			nStrips = 11;
			nBorderStrips = 1;
			stripWidth = 3;
			break;
		case MowingPatternType::FOUR:
			nStrips = 10;
			nBorderStrips = 2;
			stripWidth = 4;
			break;
		case MowingPatternType::FIVE:
			nStrips = 10;
			nBorderStrips = 2;
			stripWidth = 5;
			break;
		default:
			break;
	}
}

int MowingPattern::getBorderStrips() {
	return nBorderStrips;
}

int MowingPattern::getNStrips() {
	return nStrips;
}

int MowingPattern::getStripWidth() {
	return stripWidth;
}

MowingPatternType MowingPattern::getType() {
	return type;
}


