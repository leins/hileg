/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 *
 * ------------------------------------------------------------------------------------
 *
 * Class to read data from a csv file.
 * Adapted from:
 * https://thispointer.com/how-to-read-data-from-a-csv-file-in-c/
 * https://www.techiedelight.com/split-string-cpp-using-delimiter/
 */

#ifndef CSVREADER_H_
#define CSVREADER_H_

#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <string>
#include <algorithm>
#include <regex>

using namespace std;

class CSVReader {
private:
	string fileName;
	char delimiter;
public:
	CSVReader(string filename, char delm = ',') :
			fileName(filename), delimiter(delm)
	{ }

	// Function to fetch data from a CSV File
	vector<vector<string> > getData();

	bool isValid();
};


#endif /* CSVREADER_H_ */
