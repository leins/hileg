/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "PopulationAtts.h"

PopulationAtts::PopulationAtts(Point coord, double carryingCapacity, DispersalType dispersalType, bool longDistDisp, bool discreteDispersal) {
	this->carryingCapacity = carryingCapacity;
	this->densityAboveGround = 0.0;
	this->densityBelowGround = 0.0;
	this->coord = coord;
	this->dispersalType = dispersalType;
	this->gaining = false;
	this->longDistDisp = longDistDisp;
	this->discreteDispersal = discreteDispersal;
}


Point PopulationAtts::getCoord() {
	return coord;
}
double PopulationAtts::getDensity() const {
	return densityAboveGround + densityBelowGround;
}

double PopulationAtts::getDensityAboveGround() const {
	return densityAboveGround;
}

void PopulationAtts::setDensityAboveGround(double densityAboveGround) {
	this->densityAboveGround = densityAboveGround;
}

double PopulationAtts::getDensityBelowGround() const {
	return densityBelowGround;
}

void PopulationAtts::setDensityBelowGround(double densityBelowGround) {
	this->densityBelowGround = densityBelowGround;
}

double PopulationAtts::getCarryingCapacity() const {
	return carryingCapacity;
}

DispersalType PopulationAtts::getDispersalType() const {
	return dispersalType;
}

bool PopulationAtts::isGaining() const {
	return gaining;
}

void PopulationAtts::setGaining(bool gaining) {
	this->gaining = gaining;
}

bool PopulationAtts::isLongDistDisp() const {
	return longDistDisp;
}

void PopulationAtts::setLongDistDisp(bool longDistDisp) {
	this->longDistDisp = longDistDisp;
}

bool PopulationAtts::isDiscreteDispersal() const {
	return discreteDispersal;
}

void PopulationAtts::setDiscreteDispersal(bool discreteDispersal) {
	this->discreteDispersal = discreteDispersal;
}
