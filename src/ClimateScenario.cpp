/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "ClimateScenario.h"


ClimateScenario::ClimateScenario(GlobalModel gm, ReprConcPath rcp) {
	this->gm = gm;
	this->rcp = rcp;
}

GlobalModel ClimateScenario::getGlobalModel() {
	return gm;
}

ReprConcPath ClimateScenario::getReprConPath() {
	return rcp;
}

string ClimateScenario::getGlobalModelName() {
	auto gmName = Constants::GLOBAL_CLIMATE_MODEL_NAMES_BY_ENUM.find(gm);
	return (*gmName).second;
}

string ClimateScenario::getReprConPathName() {
	auto rcpName = Constants::RCP_NAMES_BY_ENUM.find(rcp);
	return (*rcpName).second;
}

string ClimateScenario::getRealisation() {
	auto realisation = Constants::GLOBAL_CLIMATE_REALISATION_BY_MODEL.find(gm);
	return (*realisation).second;
}

string ClimateScenario::getFilename(ClimateVariable climVar) {
	string filenameClimate = "climate/";
	filenameClimate.append(getGlobalModelName());
	filenameClimate.append("_");
	filenameClimate.append(getReprConPathName());
	filenameClimate.append("_");
	filenameClimate.append(getRealisation());
	filenameClimate.append("_");
	filenameClimate.append(Constants::REGIONAL_CLIMATE_MODEL);
	filenameClimate.append("/");

	auto climVarName = Constants::CLIMATE_VARIABLE_NAMES_BY_ENUM.find(climVar);
	filenameClimate.append((*climVarName).second);
	filenameClimate.append("_");
	filenameClimate.append(Constants::CLIMATE_MODEL_REGION);
	filenameClimate.append("_");
	filenameClimate.append(getGlobalModelName());
	filenameClimate.append("_");
	filenameClimate.append(getReprConPathName());
	filenameClimate.append("_");
	filenameClimate.append(getRealisation());
	filenameClimate.append("_");
	filenameClimate.append(Constants::REGIONAL_CLIMATE_MODEL);
	filenameClimate.append("_");
	filenameClimate.append(Constants::CLIMATE_TIME_STEP);
	filenameClimate.append("_");
	filenameClimate.append(Constants::CLIMATE_TIME_PERIOD);
	filenameClimate.append(".nc");

	return filenameClimate;
}
