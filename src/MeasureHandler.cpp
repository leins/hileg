/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "MeasureHandler.h"

long MeasureHandler::NEXT_ID = 1;

MeasureHandler::MeasureHandler() {
	this->id = MeasureHandler::NEXT_ID++;
}

bool MeasureHandler::isDue(Measure* measure, Environment* environment, Point coord) {
	int timestep = environment->getTimestep();
	/** actually the last day of the last usage period. days in the middle of a period/duration are not stored **/
	int dayOfLastUse = measure->getLastUse();
	if(timestep == dayOfLastUse) {
		return true;
	}
	int currentWeek = environment->getWeek();
	int currentDay = timestep;
	if (measure->hasRelativeTiming()) {
		if (measure->doesFollowVegetation()) {
			currentDay = environment->getDayOfVegetation(coord);
		} else {
			currentDay = environment->getDayOfCycle();
		}
	}
	int duration = measure->getDuration();
	int durationNormalized = duration > 0 ? duration - 1 : 0;

	int dayStart = measure->getNextUse();
	int dayEnd = dayStart + durationNormalized;
	if(dayStart >= 0 && currentDay >= dayStart && currentDay <= dayEnd){
		//TODO: make sure next use is only increased after duration has passed
		if(currentDay == dayEnd){
			measure->resetLastUse(timestep, currentWeek);
		}
		if (measure->doesFollowVegetation()) {
			if(fname.str().empty()) {
//				stringstream fname;
				fname << environment->getFullpathRun() << "/disturbanceTiming";
				if(id > 1) {
					fname << "_" << id;
				}
				fname << ".csv";
				ofstream fileMeasures;
				fileMeasures.open(fname.str(), std::ofstream::app);
				fileMeasures << "seed,xCoord,yCoord,year,day\n";
				fileMeasures.close();
			}
			ofstream fileMeasures;
			fileMeasures.open(fname.str(), std::ofstream::app);
			fileMeasures << environment->getSeedLocal() << "," << coord.getX() + 1 << "," << coord.getY() + 1 << "," << Constants::BASE_YEAR + environment->getCycle() - 1 << "," << environment->getDayOfCycle() << "\n";
			fileMeasures.close();
		}
		return true;
	}
	return false;
}


