/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "LifeCycle.h"

Influence::Influence(double maxFactor, double minFactor, bool multiplicative, bool devRemain, double initDevState) {
	this->multiplicative = multiplicative;
	this->useMinMax = minFactor != maxFactor;
	this->minFactor = minFactor;
	this->maxFactor = maxFactor;
	this->deltaFactor = maxFactor > minFactor ? fabs(maxFactor - minFactor) : 0;
	this->devRemain = devRemain;
	this->initDevState = initDevState;

}

double Influence::factor(Environment* environment, Cohort* cohort) {
	if(modInfl != NULL) {
		factModInfl = modInfl->multiplier(environment, cohort);
	}
	double fact = multiplier(environment, cohort);
	if(modInfl != NULL) {
		if(multModInfl) {
			fact *= factModInfl;
		}else {
			fact += factModInfl;
		}
	}
	if(useMinMax) {
		fact = fact < minFactor ? minFactor : (fact > (minFactor + deltaFactor) ? minFactor + deltaFactor : fact);
	}
	return fact;
}

bool Influence::isDue(Environment* environment) {
	return true;
}

bool Influence::isMultiplicative() {
	return multiplicative;
}

bool Influence::isDevRemain()  const {
	return devRemain;
}

double Influence::getInitDevState() const {
	return initDevState;
}

void Influence::setModInfl(Influence* modInfl) {
	this->modInfl = modInfl;
	multModInfl = modInfl->isMultiplicative();
}

