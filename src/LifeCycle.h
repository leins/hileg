/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LifeCycleH
#define LifeCycleH

#include <string>
#include <vector>
#include <functional>
#include <unordered_map>
#include "Environment.h"
#include "Constraints.h"
#include "PopulationAtts.h"
//#include <cmath>

// forward declaration to prevent circular inclusion
class Flow;
class Cohort;
class Influence;
class Environment;

using namespace std;

//BEGIN DEFINITION LifeStage
class LifeStage
{
private:
	string name;
	bool migratory;
	double initDensitySqm, minDensitySqm, density, residents, residentRatio, grossDiff, gain, minDensity, meanDensity, meanResidents, thdQuasiExtinct, thdQuasiExtinctSqm;
	unordered_map<string, double> meanStats;
	unordered_map<string, double> yearlyStats;
	int nReset, nStatYears;
	int currStatYear = -1;
	unordered_map<Point, double> immigrationFrom;
	unordered_map<Point, double> emigrationTo;
	bool aboveGround, multiCohorts, quasiExtinct, dummy, gaining;
	int maxAge;
	unsigned int lifetime, nQuasiExtinction;
	int dayFirstGain; // timestep of year's first gain
	int dayLastAlive; // last alive timestep of the year
	Constraint* cohortCreationConstraint = NULL;
	long gainingCohortID;
	LifeStage* prevStage = NULL;
	vector<Flow*> input;
	vector<Flow*> output;
	vector<Cohort*> cohorts;
	vector<Influence*> devInfluences;

	PopulationAtts* atts = NULL;

	Cohort* createCohort(Environment* environment, double density);

public:
	LifeStage(const LifeStage& templStage);
	LifeStage(Environment* environment, string name, PopulationAtts* stats, double initDensitySqm=0, bool aboveGround = false, bool uniqueCohort=true, int maxAge=364, double minDensitySqm=0.0001, double thdQuasiExtinctSqm=0.0, bool dummy = false);
	LifeStage(string name, double density=0, bool aboveGround = false, bool uniqueCohort=true, int maxAge=364, double minDensitySqm=0.0001, double thdQuasiExtinctSqm=0.0, bool dummy = false);

	void reset(Environment* environment, PopulationAtts* stats);

	string getName();

	PopulationAtts* getAtts() const;

	Point getCoord();

	double getDensity();

	double getDensitySqm(Environment* environment);

	double getInitDensity();

	void setInitDensity(double initDensitySqm);

	double getMinDensity();

	int getMaxAge();

	double getGain() ;

	void setCohortCreationConstraint(Constraint* constraint);

	long getGainingCohortID();

	void addInput(Flow* input);

	void addOutput(Flow* output);

	void removeInput(Flow* remFlow);

	void removeOutput(Flow* remFlow);

	void addDevInfluence(Influence* devInflunece, double devState = 0.0);

	vector<Cohort*> getCohorts();

	vector<Flow*> getOutput();

	void setPrevStage(LifeStage* prevStage);

	void update(Environment* environment);

	int getDayFirstGain();

	int getDayLastAlive();

	bool isAboveGround() const;

	bool hasMultiCohorts() const;
	bool isAlive() const;
	pair<double, double> getEmigration(double comp = 0.0) const;
	pair<double, double> getImmigration(double comp = 0.0) const;
	double getYearlyStat(string key, bool useMean = false);
	double getNQuasiExtinct(bool useMean = false);
	bool isDummy() const;
	bool isGaining() const;
	void setGaining(bool gaining);
	void setQuasiExtinct(bool quasiExtinct);
	bool isEstablished();
	void setMigratory(bool migratory);
	bool isMigratory() const;
	unordered_map<string, double> getYearlyStats(bool useMean = false);
};
//END DEFINITION LifeStage


//BEGIN DEFINITION Cohort
class Cohort
{
private:
	long id;
	int creationTime;
	vector<double> devStates;
	double density, devState, flowAmount, developedRatio, maturation;
	LifeStage* parentStage;
	void updateDevState(Environment* environment, vector<Influence*> devInfluences);

protected:
	static long NEXT_ID;

public:
	Cohort(LifeStage* parentStage, int creationTime, double density=0);

	long getId();

	int getAge(Environment* environment);

//	overaged is defined as being above maximum age
	bool isOveraged(Environment* environment);

	int getCycle(Environment* environment);

	double getDensity();

//	empty is defined as being below minimum density
	bool isEmpty();

	bool isDeveloped(double delta_max=0.001);

	void addDevState(double devState);

	void update(Environment* environment, vector<Influence*> devInfluences);

	double getDevelopedRatio();

	void setDevelopedRatio(double developedRatio);

	double getMaturation();

	void setMaturation(double maturation);

	LifeStage* getParentStage();
};
//END DEFINITION Cohort

//BEGIN DEFINITION Influence
class Influence
{

private:
	bool multiplicative, devRemain, useMinMax;
	double initDevState;

protected:
	Influence* modInfl = NULL;
	bool multModInfl = false;
	double factModInfl = 0.0;
	double minFactor, maxFactor, deltaFactor;
	virtual double multiplier(Environment* environment, Cohort* cohort = NULL) = 0;

public:
	virtual ~Influence(){};
	virtual bool isDue(Environment* environment);
	Influence(double maxFactor = 1.0, double minFactor = 0.0, bool multiplicative = true, bool devRemain = true, double initDevState = 0.0);
	double factor(Environment* environment, Cohort* cohort = NULL);
	bool isMultiplicative();
	bool isDevRemain() const;
	double getInitDevState() const;
	void setModInfl(Influence* modInfl);
};
//END DEFINITION Influence

//BEGIN DEFINITION Flow
class Flow
{
private:
	long id;
	FlowType type;
	double amount, rate, baseRate;
	bool discrete; // flow amount in discrete steps using 1 / [habitat size] individual(s) per sqm
	LifeStage* from = NULL;
	LifeStage* to = NULL;

	unordered_map<long, double> amountByCohort;
	unordered_map<long, double> rateByCohort;
	vector<Influence*> allInfluences, multiplicativeInfluences, additiveInfluences;

	void setBaseRate(double baseRate);

protected:
	static long NEXT_ID;
	double calculateRate(Environment* environment, Cohort* cohort);


public:
	Flow(LifeStage* from, LifeStage* to = NULL, double baseRate = 1.0, bool discrete = false);
	Flow(LifeStage* from, FlowType type, LifeStage* to = NULL, double baseRate = 1.0, bool discrete = false);

	void reset();

	long getId() const;

	LifeStage* getFrom();

	LifeStage* getTo();

	vector<Influence*> getInfluences();

	void addInfluence(Influence* influence);

	void addAll(vector<Influence*> influences);

	bool update(Environment* environment);

	double getAmount(long cohortID = -1);

	double getRate(long cohortID = -1);

	double getBaseRate();

	FlowType getType();

	bool operator< (const Flow &ob) const;
	bool operator== (const Flow &ob) const;
	bool operator!= (const Flow &ob) const;
};
//END DEFINITION Flow

#endif

