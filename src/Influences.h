/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef InfluencesH
#define InfluencesH

#include "LifeCycle.h"
#include "MeasureHandler.h"
#include "Climate.h"
#include "tools.h"
#include <math.h>
#include <vector>
#include <random>
#include <algorithm>

using namespace std;

class InfluenceStatic : public Influence
{
private:
	double staticFactor;

public:
	InfluenceStatic(double staticFactor, double maxFactor = 1.0, double minFactor = 0.0, bool multiplicative = true, bool devRemain = true, double initDevState = 0.0);

	double multiplier(Environment* environment, Cohort* cohort = NULL);

};

class InfluenceTimeWindow : public InfluenceStatic
{
private:
	int dayOfYear, nDays;

public:
	InfluenceTimeWindow(int dayOfYear, int nDays = 1, double staticFactor = 1.0, double maxFactor = 1.0, double minFactor = 0.0, bool multiplicative = true, bool devRemain = true, double initDevState = 0.0);

	double multiplier(Environment* environment, Cohort* cohort = NULL);
	bool isDue(Environment* environment) override;

};

class InfluenceCarryingCapacity : public Influence
{
private:
	double rTurningPoint, steepness;
public:
	// the influence is additive
	InfluenceCarryingCapacity(double steepness, double rTurningPoint, double maxFactor = 1.0, double minFactor = 0.0, bool multiplicative = false);
	double multiplier(Environment* environment, Cohort* cohort = NULL);
};

template <class T>
class InfluenceVariable : public Influence
{
private:
//	TODO implement Template that allows use of arbitrary enum
	T variable;
	bool validType;
protected:
	double getValue(Environment* environment, Cohort* cohort = NULL);
	double getValue(ClimateVariable variable, Environment* environment, Cohort* cohort = NULL);
	double getValue(DensityLevel variable, Environment* environment, Cohort* cohort = NULL);
public:
	InfluenceVariable(T variable, double maxFactor = 1.0, double minFactor = 0.0, bool multiplicative = true, bool devRemain = true, double initDevState = 0.0);
};

class InfluenceDensity : public Influence
{
private:
	DensityLevel densityLevel;
protected:
	double getDensitySqm(Environment* environment, Cohort* cohort = NULL);
public:
	InfluenceDensity(DensityLevel densityLevel, double maxFactor = 1.0, double minFactor = 0.0, bool multiplicative = true, bool devRemain = true, double initDevState = 0.0);
	DensityLevel getDensityLevel();
};

template <class T>
class InfluenceRegression : public InfluenceVariable<T>
{
	using InfluenceVariable<T>::InfluenceVariable;

protected:
	vector<double> coeffs;

public:
	InfluenceRegression(T variable, vector<double> coeffs, double maxFactor = 1.0, double minFactor = 0.0, bool multiplicative = true, bool devRemain = true, double initDevState = 0.0);
};

template <class T>
class InfluenceThreshold : public InfluenceVariable<T>
{
private:
	float threshold;
	double factBelow, factAbove;
	bool eqAbove;
public:
	InfluenceThreshold(T variable, float tempThreshold, double factBelow, double factAbove, bool eqAbove = true, bool multiplicative = true, bool devRemain = true, double initDevState = 0.0);
	double multiplier(Environment* environment, Cohort* cohort = NULL);
};

template <class T>
class InfluenceLinearRegression : public InfluenceRegression<T>
{
	using InfluenceRegression<T>::InfluenceRegression;
public:
	double multiplier(Environment* environment, Cohort* cohort = NULL);
};

template <class T>
class InfluenceExponentialRegression : public InfluenceRegression<T>
{
	using InfluenceRegression<T>::InfluenceRegression;

public:
	double multiplier(Environment* environment, Cohort* cohort = NULL);
};

template <class T>
class InfluenceSigmoidRegression: public InfluenceRegression<T>
{
	using InfluenceRegression<T>::InfluenceRegression;

public:
	double multiplier(Environment* environment, Cohort* cohort = NULL);
};

class InfluenceBinomial : public Influence
{
private:
	Influence* influenceMean;
	Influence* influenceSD;
	double pNegative; // maximum probability for values below zero when switching from binomial dist to normal dist
	bool includeDecimals; // include and handle decimal places for occurance calculation
public:
	virtual ~InfluenceBinomial(){};
	InfluenceBinomial(Influence* influenceMean, Influence* influenceSD, bool includeDecimals = false, double pNegative = 0.01, double maxFactor = 1.0, double minFactor = 0.0, bool multiplicative = true, bool devRemain = true, double initDevState = 0.0);
	double multiplier(Environment* environment, Cohort* cohort = NULL);
};


// TODO check if this can be done with generic templates as well
class InfluenceBinomialFactor : public Influence
{
private:
	double binomialFactor; // binomial probability to apply for integer part of cohort density
	unsigned int thdDensity; // density threshold to switch from binomial dist to approximated normal dist
	bool includeDecimals; // include and handle decimal places for occurance calculation
public:
	virtual ~InfluenceBinomialFactor(){};
	InfluenceBinomialFactor(double binomialFactor, bool includeDecimals = false, double pNegative = 0.01, double maxFactor = 1.0, double minFactor = 0.0, bool multiplicative = true, bool devRemain = true, double initDevState = 0.0);
	double multiplier(Environment* environment, Cohort* cohort = NULL);
};

class InfluenceDisturbance : public Influence
{
private:
	double baseFactor;
	UsageType usageType;
	double refValue; // e.g. stocking rate value that yields exact base factor as multiplier
	MeasureHandler* mHandler;
public:
	InfluenceDisturbance(double baseFactor, MeasureHandler* mHandler = NULL, double maxFactor = 1.0, double minFactor = 0.0, bool multiplicative = false, bool devRemain = true, double initDevState = 0.0);
	InfluenceDisturbance(double baseFactor, MeasureHandler* mHandler = NULL, UsageType usageType = MOWING, double refValue = 1.0, double maxFactor = 1.0, double minFactor = 0.0, bool multiplicative = false, bool devRemain = true, double initDevState = 0.0);
	double multiplier(Environment* environment, Cohort* cohort = NULL);
};

#endif

