/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Influences.h"

InfluenceStatic::InfluenceStatic(double staticFactor, double maxFactor, double minFactor, bool multiplicative, bool devRemain, double initDevState)
		: Influence(maxFactor, minFactor, multiplicative, devRemain, initDevState) {
	this->staticFactor = staticFactor;
}

double InfluenceStatic::multiplier(Environment* environment, Cohort* cohort) {
	return staticFactor;
}

InfluenceTimeWindow::InfluenceTimeWindow(int dayOfYear, int nDays, double staticFactor, double maxFactor, double minFactor, bool multiplicative, bool devRemain, double initDevState)
		: InfluenceStatic(staticFactor, maxFactor, minFactor, multiplicative, devRemain, initDevState) {
	this->dayOfYear = dayOfYear;
	this->nDays = nDays;
}

double InfluenceTimeWindow::multiplier(Environment* environment, Cohort* cohort) {
	if(isDue(environment)) {
		return InfluenceStatic::multiplier(environment, cohort);
	}
	return 0;
}

bool InfluenceTimeWindow::isDue(Environment* environment) {
	int dayOfCycle = environment->getDayOfCycle();
	return dayOfCycle >= dayOfYear && dayOfCycle <= (dayOfYear + nDays - 1);
}

InfluenceCarryingCapacity::InfluenceCarryingCapacity(double steepness, double rTurningPoint, double maxFactor,
		double minFactor, bool multiplicative) : Influence(maxFactor, minFactor, multiplicative) {
	this->rTurningPoint = rTurningPoint;
	this->steepness = steepness;
}

double InfluenceCarryingCapacity::multiplier(Environment* environment,
		Cohort* cohort) {
	PopulationAtts* atts = cohort->getParentStage()->getAtts();
	double densityAboveGround = atts->getDensityAboveGround();
	double capacity = atts->getCarryingCapacity();
	int habitatSize = environment->getHabitatSize();
	double mortality = maxFactor / (1.0 + exp( steepness * (densityAboveGround / habitatSize  - capacity * rTurningPoint ) ) );
	return mortality;
}

InfluenceBinomialFactor::InfluenceBinomialFactor(double binomialFactor, bool includeDecimals, double pNegative,
		double maxFactor, double minFactor, bool multiplicative, bool devRemain,
		double initDevState)
		: Influence(maxFactor, minFactor, multiplicative, devRemain, initDevState) {
	this->binomialFactor = binomialFactor;
	this->includeDecimals = includeDecimals;
	for(int pow10 = 0; pow10 <= pow(10,7); pow10++) {
		thdDensity = pow(10,pow10);
		double mean = binomialFactor * thdDensity;
		double SD = sqrt(mean * (1 - binomialFactor));
		double pNorm = tools::normCDF(0, mean, SD);
		if(pNorm <= pNegative) {
			break;
		}
	}
}

double InfluenceBinomialFactor::multiplier(Environment *environment, Cohort *cohort) {
	mt19937* randGen = environment->getRandGenLocal(cohort->getParentStage()->getCoord());

	double cohortDensity = cohort->getDensity();
	int nDensity = cohortDensity; // the integer part of the cohort density
	double nOccur = 0.0;
	double currBinomialFactor = binomialFactor;
	if(modInfl != NULL) {
		if(multModInfl) {
			currBinomialFactor *= factModInfl;
		}else{
			currBinomialFactor += factModInfl;
		}
	}

	if(nDensity > this->thdDensity) {
		double meanBinom = cohortDensity * currBinomialFactor;
		double SDBinom = sqrt(meanBinom * (1 - currBinomialFactor));
		if(meanBinom <= 0 || SDBinom <= 0) {
			return 0.0;
		}
		normal_distribution<double> normDistOccur(meanBinom, SDBinom);
		nOccur = normDistOccur(*randGen);
	} else {

		//	draw a random number of event occurrences from a binominal distribution depending on the current density
		binomial_distribution<> binDistOccur(nDensity, currBinomialFactor);
		nOccur = binDistOccur(*randGen);

		if(includeDecimals) {
			double decDensity = cohortDensity - nDensity; // the decimal part of the cohort density
			double meanBinom = decDensity * currBinomialFactor;
			double SDBinom = sqrt(meanBinom * (1 - currBinomialFactor));
			if(meanBinom <= 0 || SDBinom <= 0) {
				return 0.0;
			}
			normal_distribution<double> normDistOccur(meanBinom, SDBinom);
			double decOccur = normDistOccur(*randGen);
			decOccur = decOccur < 0 ? 0 : (decOccur > (2 * meanBinom) ? (2 * meanBinom) : decOccur);
			decOccur = decOccur > decDensity ? decDensity : decOccur;
			nOccur += decOccur;
		}
	}
	double mult = nOccur / cohortDensity;
	if(modInfl != NULL) {
		if(multModInfl) {
			mult /= factModInfl;
		}else{
			currBinomialFactor -= factModInfl;
		}
	}
	return mult;
}

template<class T>
InfluenceVariable<T>::InfluenceVariable(T variable, double maxFactor, double minFactor, bool multiplicative, bool devRemain, double initDevState)
		: Influence(maxFactor, minFactor, multiplicative, devRemain, initDevState) {
	this->variable = variable;
	validType = is_same<T, ClimateVariable>::value || is_same<T, DensityLevel>::value;
}

template<class T>
double InfluenceVariable<T>::getValue(Environment *environment,	Cohort *cohort) {
	if(validType) {
		return getValue(variable, environment, cohort);
	}
	return 0.0;
}

template<class T>
double InfluenceVariable<T>::getValue(ClimateVariable variable,	Environment *environment, Cohort *cohort) {
		LifeStage* parentStage = cohort->getParentStage();
		return environment->getValue(variable, parentStage->getCoord());
}

template<class T>
double InfluenceVariable<T>::getValue(DensityLevel variable,
		Environment *environment, Cohort *cohort) {
	double density = 0.0;
	switch (variable) {
		case COHORT:
			density = cohort->getDensity();
			break;
		case LIFE_STAGE:
			density = cohort->getParentStage()->getDensity();
			break;
		case BELOW_GROUND:
			density =  cohort->getParentStage()->getAtts()->getDensityBelowGround();
			break;
		case ABOVE_GROUND:
			density =  cohort->getParentStage()->getAtts()->getDensityAboveGround();
			break;
		case POPULATION:
			density =  cohort->getParentStage()->getAtts()->getDensity();
			break;
		default:
			break;
	}
	return density / environment->getHabitatSize();
}

InfluenceDensity::InfluenceDensity(DensityLevel densityLevel, double maxFactor, double minFactor, bool multiplicative, bool devRemain, double initDevState)
		: Influence(maxFactor, minFactor, multiplicative, devRemain, initDevState) {
	this->densityLevel = densityLevel;
}
DensityLevel InfluenceDensity::getDensityLevel() {
	return densityLevel;
}

double InfluenceDensity::getDensitySqm(Environment* environment, Cohort* cohort) {
	double density = 0.0;
	switch (densityLevel) {
		case COHORT:
			density = cohort->getDensity();
			break;
		case LIFE_STAGE:
			density = cohort->getParentStage()->getDensity();
			break;
		case BELOW_GROUND:
			density =  cohort->getParentStage()->getAtts()->getDensityBelowGround();
			break;
		case ABOVE_GROUND:
			density =  cohort->getParentStage()->getAtts()->getDensityAboveGround();
			break;
		case POPULATION:
			density =  cohort->getParentStage()->getAtts()->getDensity();
			break;
		default:
			break;
	}
	return density / environment->getHabitatSize();
}

template <class T>
InfluenceRegression<T>::InfluenceRegression(T variable, vector<double> coeffs, double maxFactor, double minFactor, bool multiplicative, bool devRemain,	double initDevState)
		: InfluenceRegression<T>(variable, maxFactor, minFactor, multiplicative, devRemain, initDevState) {
	this->coeffs = coeffs;
}

template <class T>
InfluenceThreshold<T>::InfluenceThreshold(T variable, float threshold,
		double factBelow, double factAbove, bool eqAbove,
		bool multiplicative, bool devRemain, double initDevState)
		: InfluenceVariable<T>(variable, std::max(factBelow, factAbove), std::min(factBelow, factAbove), multiplicative, devRemain, initDevState) {
	this->threshold = threshold;
	this->factBelow = factBelow;
	this->factAbove = factAbove;
	this->eqAbove = eqAbove;
}

template <class T>
double InfluenceThreshold<T>::multiplier(Environment* environment, Cohort* cohort) {
	double value = InfluenceVariable<T>::getValue(environment, cohort);
	if(eqAbove) {
		if(value < threshold) {
			return factBelow;
		}
		return factAbove;
	}
	if(value > threshold) {
		return factAbove;
	}
	return factBelow;
}

template <class T>
double InfluenceLinearRegression<T>::multiplier(Environment* environment, Cohort* cohort) {
	double value = InfluenceVariable<T>::getValue(environment, cohort);
	return this->coeffs[0] + this->coeffs[1] * value;
}


template <class T>
double InfluenceExponentialRegression<T>::multiplier(Environment* environment,
		Cohort* cohort) {
	double value = InfluenceVariable<T>::getValue(environment, cohort);
	return this->coeffs[0] + this->coeffs[1] * exp(this->coeffs[2] * (value - this->coeffs[3]));
}

template <class T>
double InfluenceSigmoidRegression<T>::multiplier(Environment* environment,
		Cohort* cohort) {
	double value = InfluenceVariable<T>::getValue(environment, cohort);
	return this->coeffs[0] * abs(1 - 1 / (1 + exp(-1 * this->coeffs[1] * (value - this->coeffs[2]))));
}

InfluenceBinomial::InfluenceBinomial(Influence* influenceMean, Influence* influenceSD, bool includeDecimals, double pNegative, double maxFactor, double minFactor, bool multiplicative, bool devRemain, double initDevState) : Influence(maxFactor, minFactor, multiplicative, devRemain, initDevState) {
	this->influenceMean = influenceMean;
	this->influenceSD = influenceSD;
	this->includeDecimals = includeDecimals;
	this->pNegative = pNegative;
}

double InfluenceBinomial::multiplier(Environment* environment, Cohort* cohort) {
	//	if there's no cohort defined, leave method by return 0.0 multiplier
	if(!cohort) {
		return 0.0;
	}
	double cohortDensity = cohort->getDensity();
	double developedRatio = cohort->getDevelopedRatio();

	//	get mean and SD for the current temperature
	double meanDuration = influenceMean->factor(environment, cohort);
	double SDDuration = influenceSD->factor(environment, cohort);
	SDDuration = SDDuration < Constants::EPSILON ? Constants::EPSILON : SDDuration; // make sure there's no negative or ZERO SD
	//	calculate the maximum duration defined as mean duration plus 3 SDs
	double maxDuration = meanDuration + 3 * SDDuration;
	double maturation = cohort->getMaturation();

	double relAgeBefore = maxDuration * maturation;
	//	increase the relative age by one
	double relAgeAfter = maxDuration * maturation + 1.0;
	maturation = relAgeAfter / maxDuration;
	maturation = maturation > 1.0 ? 1.0 : maturation;
	cohort->setMaturation(maturation);

//	the number of SD the current relative age is away from the mean duration
//	see Papula Band 3 Page 385 for z-Score calc
	double zScoreBefore = (relAgeBefore - meanDuration) / SDDuration;
	double zScoreAfter = (relAgeAfter - meanDuration) / SDDuration;

//	the cumulative distribution function of a standard normal distribution converged by the c++ complementary error function
//	see https://en.cppreference.com/w/cpp/numeric/math/erfc
//	see https://stackoverflow.com/questions/2328258/cumulative-normal-distribution-function-in-c-c
	double normCDFbefore = std::erfc(-zScoreBefore/std::sqrt(2))/2;
	double normCDFafter = std::erfc(-zScoreAfter/std::sqrt(2))/2;

//	calc the propability for the event to occur during a delta of 1 (diff of relative ages)
	double pOccur = normCDFafter - normCDFbefore;

	if(pOccur <= 0) {
		return 0.0;
	}

	//	calculate the parameters to construct a normal distribution using binomial mean and SD
//	TODO make use of generics
	double meanBinom = cohortDensity * pOccur;
	double SDBinom = sqrt(meanBinom * (1 - pOccur));

	if(meanBinom <= 0 || SDBinom <= 0) {
		return 0.0;
	}
	mt19937* randGen = environment->getRandGenLocal(cohort->getParentStage()->getCoord());
	double pNorm = tools::normCDF(0, meanBinom, SDBinom);
	double nOccur = 0.0;
	if(pNorm <= pNegative) {
		normal_distribution<double> normDistOccur(meanBinom, SDBinom);
		nOccur = normDistOccur(*randGen);
	} else {
		int nDensity = cohortDensity;
		//	draw a random number of event occurrences from a binominal distribution depending on the current density
		binomial_distribution<> binDistOccur(nDensity, pOccur);
		nOccur = binDistOccur(*randGen);

		if(includeDecimals) {
			double decDensity = cohortDensity - nDensity; // the decimal part of the cohort density
			double meanBinom = decDensity * pOccur;
			double SDBinom = sqrt(meanBinom * (1 - pOccur));
			if(meanBinom <= 0 || SDBinom <= 0) {
				return 0.0;
			}
			normal_distribution<double> normDistOccur(meanBinom, SDBinom);
			double decOccur = normDistOccur(*randGen);
			decOccur = decOccur < 0 ? 0 : (decOccur > (2 * meanBinom) ? (2 * meanBinom) : decOccur);
			decOccur = decOccur > decDensity ? decDensity : decOccur;
			nOccur += decOccur;
		}
	}

	// the absolute occurrence rate at the current age and temperature depending
	double rHatchAbs = nOccur / cohortDensity;

	// the relative occurrence rate depending on the density it already occurred to
	double rOccurRel = 0.0;
	if((1 - developedRatio) > 0.0) {
		rOccurRel = rHatchAbs / (1 - developedRatio);
	}
	rOccurRel = rOccurRel > 1.0 ? 1.0 : rOccurRel;

	cohort->setDevelopedRatio(developedRatio + rHatchAbs);

	return rOccurRel;
}

InfluenceDisturbance::InfluenceDisturbance(double baseFactor, MeasureHandler* mHandler, double maxFactor, double minFactor, bool multiplicative, bool devRemain, double initDevState)
	: InfluenceDisturbance(baseFactor, mHandler, MOWING, 1.0, maxFactor, minFactor, multiplicative, devRemain, initDevState)
{ }

InfluenceDisturbance::InfluenceDisturbance(double baseFactor, MeasureHandler* mHandler, UsageType usageType, double refValue, double maxFactor, double minFactor, bool multiplicative, bool devRemain, double initDevState) : Influence(maxFactor, minFactor, multiplicative, devRemain, initDevState) {
	if(mHandler == NULL) {
		this->mHandler = new MeasureHandler();
	} else {
		this->mHandler = mHandler;
	}
	this->baseFactor = baseFactor;
	this->usageType = usageType;
	this->refValue = refValue;
}

double InfluenceDisturbance::multiplier(Environment* environment, Cohort* cohort) {
	double multi = 0.0;
	Point coord = cohort->getParentStage()->getCoord();
	Measure* measure = environment->getMeasure(coord);
	UsageType mUsageType = measure->getTypeOfUse();
	if(usageType == mUsageType && mHandler->isDue(measure, environment, coord)){
		switch (mUsageType) {
			case MOWING:
				multi = baseFactor * measure->getMownRatio();
				break;
			case CATTLE:
			case BUFFALO:
			case GOAT:
			case SHEEP:
				multi = baseFactor * (measure->getStockingRate() / refValue);
				break;
			default:
				break;
		}
	}
	return multi;
}

template class InfluenceVariable<DensityLevel>;
template class InfluenceRegression<DensityLevel>;
template class InfluenceExponentialRegression<DensityLevel>;
template class InfluenceLinearRegression<DensityLevel>;
template class InfluenceSigmoidRegression<DensityLevel>;
template class InfluenceThreshold<DensityLevel>;

template class InfluenceVariable<ClimateVariable>;
template class InfluenceRegression<ClimateVariable>;
template class InfluenceExponentialRegression<ClimateVariable>;
template class InfluenceLinearRegression<ClimateVariable>;
template class InfluenceSigmoidRegression<ClimateVariable>;
template class InfluenceThreshold<ClimateVariable>;
