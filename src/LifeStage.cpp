/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "LifeCycle.h"

LifeStage::LifeStage(Environment* environment, string name, PopulationAtts* atts, double initDensitySqm, bool aboveGround, bool uniqueCohort, int maxAge, double minDensitySqm, double thdQuasiExtinctSqm, bool dummy) { // default constructor
	this->name = name;
	this->initDensitySqm = initDensitySqm;
	this->aboveGround = aboveGround;
	this->multiCohorts = !uniqueCohort;
	this->minDensitySqm = minDensitySqm;
	this->minDensity = minDensitySqm;
	this->thdQuasiExtinct = thdQuasiExtinctSqm;
	this->thdQuasiExtinctSqm = thdQuasiExtinctSqm;
	this->maxAge = maxAge;
	this->dummy = dummy;
	this->gaining = false;
	this->cohortCreationConstraint = new ConstraintDefault(!uniqueCohort);
	nReset = -1; // signaling that reset will be called from constructor
	migratory = false;
	reset(environment, atts);
}

LifeStage::LifeStage(string name, double density,
		bool aboveGround, bool uniqueCohort, int maxAge, double minDensitySqm, double thdQuasiExtinctSqm, bool dummy)
	: LifeStage(NULL, name, NULL, density, aboveGround, uniqueCohort, maxAge, minDensitySqm, thdQuasiExtinctSqm, dummy)
{
}

LifeStage::LifeStage(const LifeStage& templStage)
	: LifeStage(NULL, templStage.name, templStage.atts, templStage.initDensitySqm, templStage.aboveGround, !templStage.multiCohorts, templStage.maxAge, templStage.minDensitySqm, templStage.thdQuasiExtinct, templStage.dummy)
{
}

void LifeStage::reset(Environment* environment, PopulationAtts* atts) {
	density = 0;
	residents = 0;
	residentRatio = 0.0;
	grossDiff = 0;
	meanDensity = 0.0;
	meanResidents = 0.0;
	lifetime = 0;
	yearlyStats["lifetime"] = 0.0;
	yearlyStats["meanDensity"] = 0.0;
	yearlyStats["meanResidents"] = 0.0;
	yearlyStats["meanResidentRatio"] = 0.0;
	yearlyStats["peakDensity"] = 0.0;
	yearlyStats["peakResidents"] = 0.0;
	yearlyStats["peakDensityRatio"] = 0.0;
	yearlyStats["peakResidentsRatio"] = 0.0;
	yearlyStats["grossPeak"] = 0.0;
	yearlyStats["gain"] = 0.0;
	yearlyStats["emigration"] = 0.0;
	yearlyStats["immigration"] = 0.0;
	yearlyStats["mortality"] = 0.0;
	currStatYear = -1;
	nStatYears = 0;
	nQuasiExtinction = 0;
	quasiExtinct = false;
	if(nReset < 0) { // was called by constructor
		nReset = 0;
		meanStats["lifetime"] = 0.0;
		meanStats["meanDensity"] = 0.0;
		meanStats["meanResidents"] = 0.0;
		meanStats["peakDensity"] = 0.0;
		meanStats["peakResidents"] = 0.0;
		meanStats["peakDensityRatio"] = 0.0;
		meanStats["peakResidentsRatio"] = 0.0;
		meanStats["grossPeak"] = 0.0;
		meanStats["gain"] = 0.0;
		meanStats["emigration"] = 0.0;
		meanStats["immigration"] = 0.0;
		meanStats["mortality"] = 0.0;
		meanStats["nQuasiExtinction"] = 0.0;
	} else {
		nReset++;
	}
	gain = 0;
	dayFirstGain = 0;
	dayLastAlive = 0;
	cohorts.clear();
	gainingCohortID = -1;
	this->atts = atts;
	if(environment != NULL) {
		minDensity = minDensitySqm * environment->getHabitatSize();
		thdQuasiExtinct = thdQuasiExtinctSqm * environment->getHabitatSize();
		if(initDensitySqm >= minDensitySqm && (!environment->isSoloPop() || environment->getSoloPop() == atts->getCoord())) {
			density = initDensitySqm * environment->getHabitatSize();
			Cohort* initCohort = createCohort(environment, density);
	//		if an initial Cohort was created
			if(initCohort) {
				cohorts.push_back(initCohort);
				for(Influence* influence : devInfluences) {
					initCohort->addDevState(influence->getInitDevState());
				}
				residents = density;
				residentRatio = 1.0;
			}
		}
	}
}

Cohort* LifeStage::createCohort(Environment* environment, double density) {
	if(density >= minDensity) {
		Cohort* cohort = new Cohort(this, environment->getTimestep(), density);
		gainingCohortID = cohort->getId();
		return cohort;
	}
	return NULL;
}

string LifeStage::getName() {
	return name;
}

double LifeStage::getDensity() {
	return density;
}

double LifeStage::getDensitySqm(Environment* environment) {
	return density / environment->getHabitatSize();
}


double LifeStage::getInitDensity() {
	return initDensitySqm;
}

void LifeStage::setInitDensity(double initDensitySqm) {
	this->initDensitySqm = initDensitySqm;
}

double LifeStage::getMinDensity() {
	return minDensity;
}

int LifeStage::getMaxAge() {
	return maxAge;
}

void LifeStage::setCohortCreationConstraint(Constraint* constraint) {
	this->cohortCreationConstraint = constraint;
}

long LifeStage::getGainingCohortID() {
	return gainingCohortID;
}

vector<Cohort*> LifeStage::getCohorts() {
	return cohorts;
}

vector<Flow*> LifeStage::getOutput() {
	return output;
}

void LifeStage::setPrevStage(LifeStage *prevStage) {
	this->prevStage = prevStage;
}

void LifeStage::addInput(Flow* flow) {
	migratory = migratory || flow->getType() == FlowType::MIGRATION;
	input.push_back(flow);
}

void LifeStage::addOutput(Flow* flow) {
	migratory = migratory || flow->getType() == FlowType::MIGRATION;
	output.push_back(flow);
}

void LifeStage::removeInput(Flow* remFlow) {
	vector<Flow*> reducedFlows;
	for(auto flow : input) {
		if(*flow == *remFlow) {
			continue;
		}
		reducedFlows.push_back(flow);
	}
	input = reducedFlows;
}

void LifeStage::removeOutput(Flow* remFlow) {
	vector<Flow*> reducedFlows;
	for(auto flow : output) {
		if(*flow == *remFlow) {
			continue;
		}
		reducedFlows.push_back(flow);
	}
	output = reducedFlows;
}

void LifeStage::addDevInfluence(Influence* devInfluence, double devState) {
	// if the influence to add is not NULL
	if(devInfluence) {
		devInfluences.push_back(devInfluence);
	}
}

void LifeStage::update(Environment* environment) {
	if(dummy) {
		return;
	}
	if(currStatYear < environment->getCycle()) {
		currStatYear = environment->getCycle();
		grossDiff = 0.0;
		for(auto yearlyStat : yearlyStats) {
			yearlyStats[yearlyStat.first] = 0.0;
		}
		nStatYears++;
	}
	bool aliveBefore = isAlive();
	double prevDens = density;
	density = 0;
	gain = 0;
	if(!aliveBefore && !gaining){
		return;
	}

	Cohort* newCohort = NULL; // object to hold a new Cohort, if there's any created
	// reset density, gain

	// calculate the total gain and immigration of this life stage by summing the (migration) input flows
	double growth = 0.0; // gain other than immigration
	for(auto flow : input) {
		double amount = flow->getAmount();
		gain += amount;
		if(flow->getType() == FlowType::MIGRATION) {
			immigrationFrom[flow->getFrom()->getCoord()] = amount;
			yearlyStats["immigration"] += amount;
			meanStats["immigration"] += amount;
			grossDiff -= amount;
		}else{
			growth += amount;
		}
	}

	double loss = 0.0;
	for(auto flow : output) {
		double amount = flow->getAmount();
		if(flow->getType() == FlowType::MORTALITY) {
			yearlyStats["mortality"] += amount;
			meanStats["mortality"] += amount;
		}
		// calculate the total emigration of this life stage by summing the migration output flows
		if(flow->getType() == FlowType::MIGRATION) {
			emigrationTo[flow->getTo()->getCoord()] = amount;
			yearlyStats["emigration"] += amount;
			meanStats["emigration"] += amount;
			grossDiff += amount;
		}
		if(flow->getType() != FlowType::REPRODUCTION) {
			loss += amount;
		}
	}

	// if gain > 0 AND if multi cohort LifeStage OR list of cohorts is empty
	if(gain > 0 && (cohortCreationConstraint->isSatisfied(environment) || cohorts.size() == 0)) {
		// create a new empty cohort
		newCohort = createCohort(environment, gain);
		if(newCohort && cohorts.size() == 0){
			dayFirstGain = environment->getDayOfCycle();
		}
		if(!newCohort && cohorts.size() == 0){
			gain = 0;
		}
	}
	yearlyStats["gain"] += gain;
	meanStats["gain"] += gain;
//	loop to update all existing cohorts
	for(std::vector<Cohort*>::iterator it_cohorts = cohorts.begin(); it_cohorts != cohorts.end();) {
		Cohort* cohort = (*it_cohorts);
//		update current cohort
		cohort->update(environment, devInfluences);
		if(cohort->isEmpty()) {
//			drop empty cohort
			delete (*it_cohorts);
			cohorts.erase(it_cohorts);
//			delete cohort;
			if(cohorts.size() == 0 && !newCohort){
				// define that no cohort is gaining atm
				gainingCohortID = -1;
				dayLastAlive = environment->getDayOfCycle();
			}
		}else{
			density += cohort->getDensity();
			++it_cohorts;
		}
	}
	// add new cohort if there is any and add its density to stage density
	if(newCohort) {
		cohorts.push_back(newCohort);
		density += newCohort->getDensity();
	}
	if(density > 0) {
		residents = (prevDens - loss) * residentRatio + growth;
		residentRatio = residents / density;
	} else {
		residents = 0.0;
		residentRatio = 0.0;
	}

	bool alive = isAlive();
	double yearlyPeakDensity = yearlyStats["peakDensity"];
	double yearlyPeakResidents = yearlyStats["peakResidents"];
	if(alive) {
		yearlyStats["meanDensity"] = (density + yearlyStats["lifetime"] * yearlyStats["meanDensity"]) / (yearlyStats["lifetime"] + 1);
		yearlyStats["meanResidentRatio"] = (residentRatio + yearlyStats["lifetime"] * yearlyStats["meanResidentRatio"]) / (yearlyStats["lifetime"] + 1);
		yearlyStats["meanResidents"] = (residents + yearlyStats["lifetime"] * yearlyStats["meanResidents"]) / (yearlyStats["lifetime"] + 1);

		// three step calc to keep value of prev random seed (meanStats are calculated over replicates)
		meanStats["meanDensity"] -= meanDensity;
		meanDensity = (density + lifetime * meanDensity) / (lifetime + 1);
		meanStats["meanDensity"] += meanDensity;

		meanStats["meanResidents"] -= meanResidents;
		meanResidents = (density + lifetime * meanResidents) / (lifetime + 1);
		meanStats["meanResidents"] += meanResidents;

		yearlyStats["lifetime"]++;
		meanStats["lifetime"]++;
		lifetime++;
		if(density > yearlyPeakDensity) {
			yearlyStats["peakDensity"] = density;
			yearlyStats["peakDensityRatio"] = residentRatio;
			meanStats["peakDensity"] = (density + meanStats["peakDensity"] * (nStatYears - 1)) / nStatYears;
			meanStats["peakDensityRatio"] = (residentRatio + meanStats["peakDensityRatio"] * (nStatYears - 1)) / nStatYears;
		}
		if(residents > yearlyPeakResidents) {
			yearlyStats["peakResidents"] = residents;
			yearlyStats["peakResidentsRatio"] = residentRatio;
			meanStats["peakResidents"] = (residents + meanStats["peakResidents"] * (nStatYears - 1)) / nStatYears;
			meanStats["peakResidentsRatio"] = (residentRatio + meanStats["peakResidentsRatio"] * (nStatYears - 1)) / nStatYears;
		}
		double yearlyGrossPeak = yearlyStats["grossPeak"];
		double grossDensity = density + grossDiff;
		if(grossDensity > yearlyGrossPeak) {
			yearlyStats["grossPeak"] = grossDensity;
			meanStats["grossPeak"] = (grossDensity + meanStats["grossPeak"] * (nStatYears - 1)) / nStatYears;
		}
		if(quasiExtinct && !(yearlyPeakDensity < thdQuasiExtinct)) { // if marked as quasi extinct, check if it is not below quasi extinct threshold anymore
			quasiExtinct = false;
		}
	}

	gaining = false; // reset flag for next iteration
	bool died = aliveBefore && !alive;
	//after life stage ended that isn't already quasi extinct, check if its predecessor stage isn't alive and its peak density was below quasi extinct threshold
	if(died && !quasiExtinct && !prevStage->isAlive() && yearlyPeakDensity < thdQuasiExtinct) {
		quasiExtinct = true;
		nQuasiExtinction++;
		meanStats["nQuasiExtinction"]++;
	}
}

int LifeStage::getDayFirstGain() {
	return dayFirstGain;
}

double LifeStage::getGain() {
	return gain;
}

int LifeStage::getDayLastAlive() {
	return dayLastAlive;
}

PopulationAtts* LifeStage::getAtts() const {
	return atts;
}

Point LifeStage::getCoord() {
	if(atts == NULL) {
		return Point(-1,-1);
	}
	return atts->getCoord();
}

bool LifeStage::isAboveGround() const {
	return aboveGround;
}

bool LifeStage::hasMultiCohorts() const {
	return multiCohorts;
}

bool LifeStage::isAlive() const {
	return density >= minDensity;
}

pair<double, double> LifeStage::getEmigration(double comp) const {
//	using Kahan summation algorithm for compensation of numerical error
	double emSum = 0;
	for(auto emigration : emigrationTo) {
		double emY = emigration.second - comp; // summand minus compensation
		double emTemp = emSum + emY; // temporary sum that might loose low-order digits of emY
		comp = (emTemp - emSum) - emY; // recover low part of emY and store as compensation for next iteration
		emSum = emTemp;
	}
	return pair<double,double>(emSum, comp);
}

double LifeStage::getYearlyStat(string key, bool useMean) {
	if(useMean) {
		if(meanStats.find(key) == meanStats.end()) {
			return 0.0;
		}
		return meanStats[key] / nReset;
	}
	if(yearlyStats.find(key) == yearlyStats.end()) {
		return 0.0;
	}
	return yearlyStats[key];
}

double LifeStage::getNQuasiExtinct(bool useMean) {
	if(useMean) {
		return meanStats["nQuasiExtinction"] / nReset;
	}
	return nQuasiExtinction;
}

pair<double, double> LifeStage::getImmigration(double comp) const {
//	using Kahan summation algorithm for compensation of numerical error
	double imSum = 0;
	for(auto immigration : immigrationFrom) {
		double imY = immigration.second - comp; // summand minus compensation
		double imTemp = imSum + imY; // temporary sum that might loose low-order digits of imY
		comp = (imTemp - imSum) - imY; // recover low part of imY and store as compensation for next iteration
		imSum = imTemp;
	}
	return pair<double,double>(imSum, comp);
}

bool LifeStage::isDummy() const {
	return dummy;
}

bool LifeStage::isGaining() const {
	return gaining;
}

void LifeStage::setGaining(bool gaining) {
	this->gaining = gaining;
	if(gaining) {
		atts->setGaining(gaining);
	}
}

unordered_map<string, double> LifeStage::getYearlyStats(bool useMean) {
	if(!useMean) {
		return yearlyStats;
	}
	unordered_map<string, double> yearlyStatsMean;
	for(auto yearlyStat : yearlyStats) {
		string key = yearlyStat.first;
		yearlyStatsMean[key] = meanStats[key] / nReset;
	}
	return yearlyStatsMean;
}

bool LifeStage::isMigratory() const {
	return migratory;
}

void LifeStage::setMigratory(bool migratory) {
	this->migratory = migratory;
}

bool LifeStage::isEstablished() {
	return yearlyStats["peakDensity"] > thdQuasiExtinct;
}

void LifeStage::setQuasiExtinct(bool quasiExtinct) {
	this->quasiExtinct = quasiExtinct;
}
