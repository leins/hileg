/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include <vector>
#include <map>
#include <unordered_set>
#include <iostream>
#include <stdio.h>
#include "Constants.h"
#include "Population.h"
#include "Environment.h"
#include "ClimateScenario.h"
#include "LifeCycle.h"
#include "Point.h"
#include "Cell.h"
#include "tools.h"
#include <fstream>
#include <date.h>
#include <time.h>
#include <chrono>
#include <sys/stat.h>

using namespace std;
using namespace date;
using namespace std::chrono;

ClimateScenario* climateScenario;
// list of available climate models and their RCP scenarios
const vector<ClimateScenario*> climateScenarios = {
													new ClimateScenario(CCCma, RCP85), 	// 00: 40515 timesteps
													new ClimateScenario(CNRM, RCP45), 	// 01: 40543 timesteps
													new ClimateScenario(CNRM, RCP85), 	// 02: 40543 timesteps
													new ClimateScenario(ICHEC, RCP26), 	// 03: 40543 timesteps
													new ClimateScenario(ICHEC, RCP45), 	// 04: 40543 timesteps
													new ClimateScenario(ICHEC, RCP85), 	// 05: 40543 timesteps
													new ClimateScenario(MIROC, RCP85), 	// 06: 40515 timesteps
													new ClimateScenario(MOHC, RCP45), 	// 07: 39960 timesteps
													new ClimateScenario(MOHC, RCP85), 	// 08: 39960 timesteps
													new ClimateScenario(MPI, RCP26), 	// 09: 40543 timesteps
													new ClimateScenario(MPI, RCP45), 	// 10: 40543 timesteps
													new ClimateScenario(MPI, RCP85) 	// 11: 40543 timesteps
};

// technical application run parameters
bool memorySavingMode = false; // year climate data preload only

// input file handler
CSVHandlerDisturbance* handlerDisturbances = NULL;
CSVHandlerDisturbanceScheme* handlerScheme = NULL;
CSVHandlerLifeCycle* handlerLifeCylce;
CSVHandlerDensities* handlerDensities;
CSVHandlerInfluences* handlerInfluences;
std::string filenameStages = "";
std::string filenameInfluences = "";
std::string filenameDisturbance = "";
std::string filenameDisturbanceScheme = "";
std::string filenameGrasslandCoords = "";
std::string filenameInitialDensities = "";

// output files, data and flags for output handling
bool noDens = true; // do not write density file of daily population density
bool refOut = false; // create file referencing the output folder names / files
long int runID = -1;
unordered_map<Point, ofstream*> densityFiles;
ofstream *fileStats = NULL;
ofstream *fileStatsYearly = NULL;
ofstream *fileMeanStats = NULL;
ofstream *fileOrderOfYears = NULL;
ofstream *fileAppliedDistrubanceScheme = NULL;
ofstream *fileRef = NULL;
stringstream folderNameRun, filenameAppliedDisturbanceScheme;

// model setup parameters with default values
bool useLoadedCoords = true;
bool interpolateClimate = false; // interpolate climate of subplots (sub cells / habitats) in climate cells
bool uniformDisturbances = false; // apply each potentially disturbances uniformly over grid and create a scheme for each
bool soloPops = false; // if true, run a simulation for each initial population separately
bool dynamicDisturbanceTiming = false; // if true, relative disturbance schedule (if applies) follows begin of vegetation period (temp. sum >= 200 °C)
Point currSoloCoord;
int limitShuffleYears = 0; // upper/lower limit to pick years from to shuffle
bool movingShuffleWindow = false; // true: choose year from current year +/- limitShuffleYears; false: choose year from (initialYearTimeFrame - limitShuffleYears) to (initialYearTimeFrame + deltaYearsTimeFrame + limitShuffleYears)
int disturbanceAppType = 2; // 0 = only without disturbance, 1 = only with disturbance, 2 = both
DispersalType dispersalType = DISPERSAL;
bool longDistDisp = false;
bool discreteDispersal = false;
int nTimestep = -1;
int deltaInit = -1; // delta to next initial timestep of a simulation run
int nWeightsCMT = 6; // number of time steps to use for CMT moving average
int timestepLimLower, timestepLimDelta;
unsigned int climScenIdxStart = 0;
unsigned int climScenIdxEnd = climateScenarios.size();
double carryingCapacity = 25; // per sqm
vector<bool> disturbed = {false, true};
vector<int> seeds = {3, 5, 7, 11, 13, 17, 19, 23, 29, 31};
set<unsigned int> disturbanceIDs;
unordered_map<unsigned int, string> disturbanceNameByID;
unordered_map<unsigned int, string> disturbanceSchemeNameByID;
unsigned int baseSchemeID;
map<Point, unsigned int> baseDisturbanceScheme;
unordered_map<unsigned int, map<Point, unsigned int>> disturbanceSchemes;
vector<StrategyClimate*> climateStrategies;
unordered_map<ClimateVariable, StrategyClimate*> loadedClimateData;
unordered_set<StrategyClimateNetCDF*> baseNetCDFClimateData;
unordered_set<Cell*> grasslandCells;
unordered_set<Point> grasslandCoords;
unordered_set<Point> climateCoords;
unordered_set<Point> surrCoords;

// current simulation run parameters
bool prognosis = false;
bool createFinalDensFiles = false;
bool yearly = false;
bool dailyPrint = false;
bool meanStatsOutput = false;
int initialTimestep = 0;
int initialSimulationYear = Constants::BASE_YEAR;
int initialYearTimeFrame = Constants::BASE_YEAR;
int deltaYearsTimeFrame = 1;
int finalSimulationYear = Constants::BASE_YEAR + 1;
int deltaYears = 0;
int currentStep = 0;
Point currentCoord;
unsigned int seedIdx = 0;
int currDisturbanceID;
int currDisturbanceSchemeID;
vector<int> usedSeeds;
string argString; // string containing command line argumentes passed to start simulation

// top level model entities
Environment* environment;
Population* population;
//unordered_map<string, unordered_map<Point, Population*>> populations;

// ***** BEGIN ------- LEVEL 4 ------- *****
// BEGIN: functions called by function init()

map<int, int> shuffleYears(int initialYearTimeFrame){
	map<int, int> orderOfYears;
	mt19937* randGen = environment->getRandGenGlobal();
	int maxYear = Constants::BASE_YEAR + environment->getFinalCycle() - 1;
	int finalYearShuffle = initialYearTimeFrame + deltaYearsTimeFrame - 1 + limitShuffleYears;
	int initialYearShuffle = (initialYearTimeFrame - limitShuffleYears) < Constants::BASE_YEAR ? Constants::BASE_YEAR : initialYearTimeFrame - limitShuffleYears;
	finalYearShuffle = finalYearShuffle > maxYear ? maxYear : finalYearShuffle;
	uniform_int_distribution<int> uniDistYears(initialYearShuffle, finalYearShuffle);
	cout << "Order of years for current seed: " << endl;
//	Start iterating at BASE_YEAR to assure same order of years independent of initialYearTimeFrame
	for(int indexYear = Constants::BASE_YEAR; indexYear < (initialYearTimeFrame + deltaYearsTimeFrame); indexYear++) { //stop the itertion at the final simulation year
		if(movingShuffleWindow && limitShuffleYears > 0) {
			int finalYearShuffleMov = indexYear + limitShuffleYears;
			int initialYearShuffleMov = (indexYear - limitShuffleYears) < Constants::BASE_YEAR ? Constants::BASE_YEAR : indexYear - limitShuffleYears;
			finalYearShuffleMov = finalYearShuffleMov > maxYear ? maxYear : finalYearShuffleMov;
			uniform_int_distribution<int> uniDistMov(initialYearShuffleMov, finalYearShuffleMov);
			uniDistYears = uniDistMov;

		}
		int vYear = movingShuffleWindow && limitShuffleYears == 0 ? indexYear : uniDistYears(*randGen);
		if(indexYear >= initialYearTimeFrame) { // only add to orderOfYears if we at the initialYearTimeFrame or later
			orderOfYears[indexYear] = vYear;
			if(usedSeeds.empty() || !(find(usedSeeds.begin(), usedSeeds.end(), seeds[seedIdx]) != usedSeeds.end())) {
				*fileOrderOfYears << seeds[seedIdx] << "," << indexYear << "," << vYear << "\n";
			}
			cout << indexYear << "->" << vYear << " ";
		}
	}
	cout << endl;
	if(usedSeeds.empty() || find(usedSeeds.begin(), usedSeeds.end(), seeds[seedIdx]) == usedSeeds.end()) {
		usedSeeds.push_back(seeds[seedIdx]);
	}
	if(usedSeeds.size() == seeds.size()) {
		fileOrderOfYears->close();
	}
	return orderOfYears;
}

// END: functions called by function init()
// ***** END   ------- LEVEL 4 ------- *****

// ***** BEGIN ------- LEVEL 3 ------- *****

// BEGIN: functions called by function initClimate()

StrategyClimate* loadClimateData(ClimateVariable climParam) {
	StrategyClimate* climateStrategy;
	auto findStrategy = loadedClimateData.find(climParam);
	if(findStrategy != loadedClimateData.end()) {
		climateStrategy = (*findStrategy).second;
		return climateStrategy;
	}
	switch (climParam) {
		case VTS:
		{
			StrategyClimate* stratClimTASMAX = loadClimateData(TASMAX);
			StrategyClimate* stratClimTS = loadClimateData(TS);
			StrategyClimate* stratClimTASMIN = loadClimateData(TASMIN);
			if(stratClimTASMAX == NULL || stratClimTS == NULL || stratClimTASMIN == NULL) {
				return NULL;
			}
			climateStrategy = new StrategyClimateVTS(stratClimTASMAX, stratClimTS, stratClimTASMIN);
			break;
		}
		case CMT:
		{
			StrategyClimate* stratClimPR = loadClimateData(PR);
			StrategyClimate* stratClimSMT = loadClimateData(SMT);
			if(stratClimPR == NULL || stratClimSMT == NULL) {
				return NULL;
			}
			climateStrategy = new StrategyClimateCMT(stratClimPR, stratClimSMT, nWeightsCMT);
			break;
		}
		case RSMC:
		{
			StrategyClimate* stratClimMRSO = loadClimateData(MRSO);
			if(stratClimMRSO == NULL) {
				return NULL;
			}
			climateStrategy = new StrategyClimateRSMC(stratClimMRSO);
			break;
		}
		case CW:
		{
			StrategyClimate* stratClimCMT = loadClimateData(CMT);
			StrategyClimate* stratClimRSMC = loadClimateData(RSMC);
			if(stratClimCMT == NULL || stratClimRSMC == NULL) {
				return NULL;
			}
			climateStrategy = new StrategyClimateCW(stratClimCMT, Constants::THRESHOLD_CW, stratClimRSMC, Constants::THRESHOLD_RMUG * 1.1);
			break;
		}
		case RHUG:
		{
			StrategyClimate* stratClimRSMC = loadClimateData(RSMC);
			if(stratClimRSMC == NULL) {
				return NULL;
			}
			climateStrategy = new StrategyClimateRHUG(stratClimRSMC, Constants::THRESHOLD_RMUG, false);
			break;
		}
		default:
			string filename = climateScenario->getFilename(climParam);
			if (filename.empty()) {
				return NULL;
			}
			mt19937* randGen = environment->getRandGenGlobal();
			unordered_set<Point> allCoords;
			allCoords.insert(climateCoords.begin(), climateCoords.end());
			allCoords.insert(surrCoords.begin(), surrCoords.end());
			climateStrategy = new StrategyClimateNetCDF(filename, *randGen, allCoords, climParam, timestepLimLower, timestepLimDelta, nWeightsCMT);
			baseNetCDFClimateData.emplace((StrategyClimateNetCDF*) climateStrategy);
			break;
	}
	loadedClimateData[climParam] = climateStrategy;
	return climateStrategy;
}

// END: functions called by function initClimate()

// BEGIN: functions called by function initEnvironment()

void updateClimate() {
	int newTimestepLow = timestepLimLower;
	bool reload = false;
	if(memorySavingMode) {
		newTimestepLow = environment->getIndexTime();
		reload = true;
	}
	for(auto strategyClimate : baseNetCDFClimateData) {
		strategyClimate->resetTimeframe(newTimestepLow, reload);
	}
}

// END: functions called by function initEnvironment()

// BEGIN: functions called by function outputRun()

void initDisturbances() {
	environment->clearDisturbances();
	fileAppliedDistrubanceScheme->open(filenameAppliedDisturbanceScheme.str(), std::ios::app);
	for(auto cell : grasslandCells) {
		Point coord = cell->getCoord();
		Point climCoord = cell->getClimCoord();
		for (auto distID : disturbanceIDs) {
			unordered_map<int, vector<int>> disturbanceTimingByYear;
			if(distID != Constants::ID_DISTURBANCE_NONE && !handlerDisturbances->isEmpty()) {
				disturbanceTimingByYear = handlerDisturbances->getTimingByYear(&coord, &climCoord, disturbanceNameByID.at(distID));
			}
			bool hasSchedule = environment->addDisturbance(coord, distID, disturbanceTimingByYear, dynamicDisturbanceTiming);
			if(distID != Constants::ID_DISTURBANCE_NONE && !hasSchedule) {
				cout << "WARNING: Disturbance '" << disturbanceNameByID.at(distID) << "' has zero occurrence days at Coord [" << (coord.getX() + 1) << ";" << (coord.getY() + 1) << "] for seed " << seeds[seedIdx] << endl;
			}
		}
		unsigned int distID = baseDisturbanceScheme[coord];
//		define basic disturbance at current cell
		environment->setDisturbanceAt(coord, distID);

		*fileAppliedDistrubanceScheme << seeds[seedIdx] << "," << (coord.getX() + 1) << "," << (coord.getY() + 1) << ","  << disturbanceNameByID.at(distID) << "\n";
	}
	fileAppliedDistrubanceScheme->close();
}

void initEnvironment(int initialTimestep, int initialYearTimeFrame) {
	currentStep = initialTimestep;
//BEGIN WARNING: Order of following lines is important. Fix in env class if there's time
	environment->setInitialTimestep(initialTimestep);
	environment->setOrderOfYears(shuffleYears(initialYearTimeFrame));
	environment->setTimestep(currentStep);
	updateClimate();
//END WARNING

	initDisturbances();

	for (auto disturbanceScheme : disturbanceSchemes) { // loop over each disturbance scheme
		int disturbanceSchemeID = disturbanceScheme.first;
		unordered_map<Point, Population*> neighborhood = environment->getNeighborhood(disturbanceSchemeID);
		environment->clearActiveNeighbors(disturbanceSchemeID);
		unordered_set<Point>  activeNeighbors;
		for(auto neighbor : neighborhood) {
			Point coord = neighbor.first;
			Population* population = neighbor.second;
			population->init(environment);
			if(DispersalType::BASE_IMMIGRATION == dispersalType || population->getVitality() != DEAD) {
				activeNeighbors.insert(coord);
			}

		}
		environment->addActiveNeighbors(disturbanceSchemeID, activeNeighbors);
	}
}

void step() {
	population->updateFlows(environment);
	population->updateStages(environment);
	population->updateStats(environment);
	currentStep++;
	environment->update();
}

// END: functions called by function outputRun()
// ***** END   ------- LEVEL 3 ------- *****


// ***** BEGIN ------- LEVEL 2 ------- *****
// BEGIN: functions called by function handleArgs()

void printHelp() {
	cout << "--- Help for model usage and parameter definition ---" << endl << endl;
	cout << "MANDATORY input files are: " << Constants::FILENAME_STAGES << ", " << Constants::FILENAME_GRASSLAND_COORDS << endl;
	cout << "OPTIONAL but at least empty input files: " << Constants::FILENAME_INITIAL_DENSITIES << ", " << Constants::FILENAME_INFLUENCES << ", " << Constants::FILENAME_DISTURBANCES << ", " << Constants::FILENAME_DISTURBANCE_SCHEME << endl;
	cout << "For the usage of the input files refer to the headers of the sample files provided in the project repository\n" << endl;
	cout << "The following command line arguments are valid (defaults in square brackets, if any)" << endl;
	cout << "-cCap:" << endl << "[25] carrying capacity per square meter. Floating point values > 0 are valid\n" << endl;
	cout << "-iClim:" << endl << "[0] index of initial climate change scenario to use. Values between 0 and 11 are valid (--listClim for options)\n" << endl;
	cout << "-nClim:" << endl << "[1] number of consecutive climate change scenario to use. Valid if > 0 AND (iClim + nClim - 1) < 12\n" << endl;
	cout << "-iYear:" << endl << "[1970] initial year of simulation run. Values from 1970 to 2080 are valid\n" << endl;
	cout << "-nYear:" << endl << "[111] duration of a simulation run in years. NOTE: by definition, a year has 364 days\n" << endl;
	cout << "-dYear:" << endl << "[0] delta years to additional simulation runs. Repeated n times as long as (iYear + n * dYear + nYear) <= 2081\n" << endl;
	cout << "-iFrame:" << endl << "[iYear] initial year of first time frame used to randomly draw years from. Defaults to initial year of simulation run\n" << endl;
	cout << "-nFrame:" << endl << "[nYear] length in years of first time frame used to randomly draw years from. Defaults to duration of of simulation run\n" << endl;
	cout << "-nSeeds:" << endl << "[10] number of replicates (random seeds) to create per climate change scenario and (timely) additional simulation runs\n" << endl;
	cout << "-seed:" << endl << "run simulation without replicates using the given explicit integer number for the random seed\n" << endl;
	cout << "-limShfl:" << endl << "[0] number of years before (iTime) and after (iTime + nTime) simulation period to include in reshuffling the order of years\n" << endl;
	cout << "-runID:" << endl << "[timestamp] use the given (unique) integer number to name output files associated with this simulation run. Uses current timestamp as default\n" << endl;
	cout << "-dispersal:" << endl << "[2] applied dispersal type: 0 = none, 1 = base immigration, 2 = migration between neighboring cells, 3 = both (1 and 2)\n" << endl;
	cout << "-fnameStages:" << endl << "[" << Constants::FILENAME_STAGES << "] filename of specific file defining the life cycle of the target species using the definition of life stages\n" << endl;
	cout << "-fnameCells:" << endl << "[" << Constants::FILENAME_GRASSLAND_COORDS << "] filename of specific file containing locations (grassland cells) of simulated populations and their associated climate cells\n" << endl;
	cout << "-fnameDens:" << endl << "[" << Constants::FILENAME_INITIAL_DENSITIES << "] filename of specific file containing initial density per stage and location deviating from values in " << Constants::FILENAME_STAGES << "\n" << endl;
	cout << "-fnameInfl:" << endl << "[" << Constants::FILENAME_INFLUENCES << "] filename of specific file containing the definition of external driver influences (climate/disturbance) on life cycle processes\n" << endl;
	cout << "-fnameDist:" << endl << "[" << Constants::FILENAME_DISTURBANCES << "] filename of specific file containing the potential timing and location of disturbances\n" << endl;
	cout << "-fnameScheme:" << endl << "[" << Constants::FILENAME_DISTURBANCE_SCHEME << "] filename of specific file containing the applied timing and location of disturbances\n" << endl;
	cout << "--LDD:" << endl << "activates long distance dispersal allowing the species to cross distances outside dispersal radius in case no grassland cells are within a 90° range into a cardinal direction\n" << endl;
	cout << "--discrDisp:" << endl << "activates discrete dispersal limiting dispersal to 'full' individuals, i.e., a value of 1 / [habitat size] individuals per sqm\n" << endl;
	cout << "--soloPops:" << endl << "run a separate simulation for each distinct population defined in " << Constants::FILENAME_INITIAL_DENSITIES << " while using initial density value define in " << Constants::FILENAME_STAGES << " for all remaining populations\n" << endl;
	cout << "--movShfl:" << endl << "move the time window for reshuffling the simulation years along when advancing the current simulation year\n" << endl;
	cout << "--climInterpol:" << endl << "use bilinear interpolation of the up to four adjacent climate cell values to calculate the respective value in a grassland cell\n" << endl;
	cout << "--dynDist:" << endl << "dynamic relative timing (if applies) of disturbance schedule following the beginning of the vegetation period (temp. sum >= 200 °C)\n" << endl;
	cout << "--climOnly:" << endl << "execute simulation runs with climate conditions as only external driver\n" << endl;
	cout << "--distOnly:" << endl << "only apply additional disturbance schemes using potential disturbances defined in " << Constants::FILENAME_DISTURBANCES << "\n" << endl;
	cout << "--dens:" << endl << "create output file of daily density values (has large file size)\n" << endl;
	cout << "--finalDens:" << endl << "create an output file for each random seed containing the densities per life stage and grassland cell by the end of the simulation run\n" << endl;
	cout << "--yearly:" << endl << "output the yearly stats by the end of each simulation year\n" << endl;
	cout << "--meanStats:" << endl << "create an output file for each separate simulation (cf. --soloPops) containing the mean stats at the end of a simulation run for a reduced list of evaluation parameters\n" << endl;
	cout << "--uniformDist:" << endl << "apply disturbances uniformly in all grassland cells and create a scheme for each defined disturbance. Ignore file distScheme.csv\n" << endl;
	cout << "--prognosis:" << endl << "create one disturbance scheme for each unique disturbance in " << Constants::FILENAME_DISTURBANCES << " and each grassland cell flagged fixed=0 in " << Constants::FILENAME_DISTURBANCE_SCHEME << " (activates flag --meanStats)\n" << endl;
	cout << "--memSave:" << endl << "save memory by preloading climate data for the current simulation year only (instead of whole time frame)\n" << endl;
	cout << "--listClim:" << endl << "list available climate change scenarios and their index\n" << endl;
	cout << "--help:" << endl << "print this help page\n" << endl;
	cout << "Without command line arguments the simulation runs from 1/Jan/1970 to 30/Dec/2080 using the stages, influences, initial densities, locations and disturbances specified in the default files while creating 10 replicates\n\n" << endl;
	cout << "For implementation details refer to:" << endl;
	cout << "Leins J. A., Banitz T., Grimm V., Drechsler M. High-resolution PVA along large environmental gradients to model the combined effects of climate change and land use timing: lessons from the large marsh grasshopper" << endl;
	cout << "and" << endl;
	cout << "Leins J. A. and Drechsler M. Large scale PVA modelling of insects in cultivated grasslands: the role of dispersal in mitigating the effects of management schedules under climate change" << endl;
}

void printClimList() {
	cout << "--- Use parameter iClim with the index at the beginning of the following lines to refer to RCP scenario of the respective global climate model ---" << endl;
	cout << "CCCma-CanESM2 (global model):" << endl;
	cout << "0  -> RCP 8.5" << endl;
	cout << "CNRM-CERFACS-CNRM-CM5 (global model):" << endl;
	cout << "1  -> RCP 4.5" << endl;
	cout << "2  -> RCP 8.5" << endl;
	cout << "ICHEC-EC-EARTH (global model):" << endl;
	cout << "3  -> RCP 2.6" << endl;
	cout << "4  -> RCP 4.5" << endl;
	cout << "5  -> RCP 8.5" << endl;
	cout << "MIROC-MIROC5 (global model):" << endl;
	cout << "6  -> RCP 8.5" << endl;
	cout << "MOHC-HadGEM2-ES (global model):" << endl;
	cout << "7  -> RCP 4.5" << endl;
	cout << "8  -> RCP 8.5" << endl;
	cout << "MPI-M-MPI-ESM-LR (global model):" << endl;
	cout << "9  -> RCP 2.6" << endl;
	cout << "10 -> RCP 4.5" << endl;
	cout << "11 -> RCP 8.5" << endl;
}

// END: functions called by function handleArgs()

// BEGIN: functions called by function handleFileInput()

bool handleCoordinates(){
	climateCoords.clear();
	grasslandCoords.clear();
	grasslandCells.clear();
	cout << "Loading grassland/climate coordinates from file " << filenameGrasslandCoords << endl;
	CSVHandlerGrassland handler(filenameGrasslandCoords, interpolateClimate);
	if(handler.isValid() && !handler.isEmpty()) {
		climateCoords.insert(handler.getClimCoords().begin(), handler.getClimCoords().end());
		for(auto cell : handler.getGrasslandCells()) {
			Point grasslandCoord = cell->getCoord();
			grasslandCoords.emplace(grasslandCoord);
			grasslandCells.emplace(cell);
		}
		if(grasslandCoords.size() == 0) {
			cout << "STOPPING: EMPTY SET OF GRASSLAND COORDINATES" << endl;
			return false;
		}
		return true;
	}
	cout << "Coordinate creation ERROR: ";
	cout << handler.getMessage() << endl;
	return false;
}

bool updateDisturbanceSchemes(mt19937* randGen = NULL) {
	if(disturbanceAppType == 0) {
		return false;
	}

	unordered_set<Point> fixedCells;
	unordered_map<string, unsigned int> nFixedName;
	string maxNFixedName;
	unordered_map<Point, string> unfixedCells;
	unordered_map<string, unsigned int> nUnfixedName;
	string maxNUnfixedName;
	unordered_map<string, unsigned int> nDistName;
	string maxNDistName;
	for(auto coord : grasslandCoords) {
		string distName = handlerScheme->getDisturbanceNameAt(coord, randGen);
		if(nDistName.find(distName) == nDistName.end()) {
			nDistName[distName] = 1;
		} else {
			nDistName[distName]++;
		}
		if(nDistName[distName] > nDistName[maxNDistName]) { //update disturbance with maximum occurrence
			maxNDistName = distName;
		}
		if((!soloPops || currSoloCoord == coord || handlerScheme->isFixedAt(Point())) && handlerScheme->isFixedAt(coord)) {
			fixedCells.insert(coord);
			if(nFixedName.find(distName) == nFixedName.end()) {
				nFixedName[distName] = 1;
			} else {
				nFixedName[distName]++;
			}
			if(nFixedName[distName] > nFixedName[maxNFixedName]) { //update fixed disturbance with maximum occurrence
				maxNFixedName = distName;
			}
		}else {
			unfixedCells[coord] = distName;
			if(nUnfixedName.find(distName) == nUnfixedName.end()) {
				nUnfixedName[distName] = 1;
			} else {
				nUnfixedName[distName]++;
			}
			if(nUnfixedName[distName] > nUnfixedName[maxNUnfixedName]) { //update unfixed disturbance with maximum occurrence
				maxNUnfixedName = distName;
			}
		}
	}
	if(disturbanceAppType == 1) {
		disturbanceSchemes.clear();
		disturbanceSchemeNameByID.clear();

		string baseSchemeName;
		if(uniformDisturbances && fixedCells.size() < grasslandCoords.size()) {
			baseSchemeName = maxNUnfixedName;
		} else {
			string distNameWildcard = handlerScheme->getDisturbanceNameAt(Point());
			if(distNameWildcard != Constants::NAME_DISTURBANCE_NONE) {
				baseSchemeName = distNameWildcard;
			} else {
				baseSchemeName = maxNDistName;
			}
		}
		baseSchemeID = tools::strToHash(baseSchemeName);
		unsigned int baseDistID = baseSchemeID;
		map<Point, unsigned int> distByCoord({{Point(), baseDistID}});
		for(auto coord : fixedCells) {
			unsigned int fixedDistID = tools::strToHash(handlerScheme->getDisturbanceNameAt(coord));
			baseDisturbanceScheme[coord] = fixedDistID;
			distByCoord[coord] = fixedDistID;
		}
		for(auto distNameByCoord : unfixedCells) {
			unsigned int distID = tools::strToHash(distNameByCoord.second);
			baseDisturbanceScheme[distNameByCoord.first] = distID;
			if(distID != baseDistID) {
				distByCoord[distNameByCoord.first] = distID;
			}
		}
		disturbanceSchemes[baseSchemeID] = distByCoord;
		disturbanceSchemeNameByID[baseSchemeID] = baseSchemeName;
	}

	if(uniformDisturbances) {
		set<unsigned int> uniformDistIDs;
		if(grasslandCells.size() == fixedCells.size()) { // If all cells are fixed
			if(disturbanceAppType == 2) { // if clim only AND dist was applied (appType ==2) create ONE additional dist scheme
				uniformDistIDs.insert(tools::strToHash(maxNDistName)); // set dist with max occurrence as key
			}
		}else{
			uniformDistIDs.insert(disturbanceIDs.begin(), disturbanceIDs.end()); // set all disturbances to be used in unfixed cells
		}
		for(auto distID : uniformDistIDs) {
			if(distID == Constants::ID_DISTURBANCE_NONE || distID == baseSchemeID) {
				continue;
			}
			map<Point, unsigned int> distByCoord({{Point(), distID}});
			for(auto coord : fixedCells) {
				distByCoord[coord] = tools::strToHash(handlerScheme->getDisturbanceNameAt(coord));
			}
			disturbanceSchemes[distID] = distByCoord;
			disturbanceSchemeNameByID[distID] = disturbanceNameByID.at(distID);
		}
	}

	return true;
}

bool handleDisturbances() {
	baseDisturbanceScheme.clear();
	string baseDistName = "";
	string baseSchemeName = "base";
	unsigned int baseDistID = 1;
	baseSchemeID = tools::strToHash(baseSchemeName);
//	TODO M66 as base disturbance ?
	if(uniformDisturbances || disturbanceAppType == 0) {
		baseSchemeName = Constants::NAME_DISTURBANCE_NONE;
		baseDistName = Constants::NAME_DISTURBANCE_NONE;
		baseSchemeID =  Constants::ID_DISTURBANCE_NONE;
		baseDistID =  Constants::ID_DISTURBANCE_NONE;
	}
	disturbanceSchemes[baseSchemeID] = map<Point, unsigned int>({{Point(), baseDistID}});
	disturbanceSchemeNameByID[baseSchemeID] = baseSchemeName;
	for(auto coord : grasslandCoords) {
		baseDisturbanceScheme[coord] = Constants::ID_DISTURBANCE_NONE;
	}
	if(disturbanceAppType == 0 || disturbanceAppType == 2) {
		disturbanceIDs.emplace(Constants::ID_DISTURBANCE_NONE);
		disturbanceNameByID[Constants::ID_DISTURBANCE_NONE] = Constants::NAME_DISTURBANCE_NONE;
		if(disturbanceAppType == 0) {
			return false;
		}
	}
//	load disturbance timing from file, if defined and valid
	cout << "Loading disturbance timing from file " << filenameDisturbance << " ..." << endl;
	handlerDisturbances = new CSVHandlerDisturbance(filenameDisturbance);
	if(!handlerDisturbances->isValid()) {
		cout << "Disturbance creation ERROR: ";
		cout << handlerDisturbances->getMessage() << endl;
		return false;
	}
	if(handlerDisturbances->isEmpty()) {
		cout << "INFO: Empty disturbance timing. Skipping disturbances" << endl;
		return true;
	}
	unordered_set<string> loadedDisturbanceNames = handlerDisturbances->getDisturbanceNames();
	for(auto loadedDistName : loadedDisturbanceNames) {
//		int distID = tools::strToHash(loadedDistName);
		unsigned int distID = tools::strToHash(loadedDistName);
		disturbanceIDs.emplace(distID);
		disturbanceNameByID[distID] = loadedDistName;
	}
	cout << "Loading disturbance scheme from file " << filenameDisturbanceScheme << endl;
	handlerScheme = new CSVHandlerDisturbanceScheme(filenameDisturbanceScheme, loadedDisturbanceNames);
	if(!handlerScheme->isValid()) {
		cout << "Disturbance scheme creation ERROR: ";
		cout << handlerScheme->getMessage() << endl;
		return false;
	}
	updateDisturbanceSchemes();
	return true;
}

// END: functions called by function handleFileInput()

// BEGIN: functions called by function batchRun()

int initClimate() {
	climateStrategies.clear();
	loadedClimateData.clear();
	baseNetCDFClimateData.clear();
	unordered_set<ClimateVariable> appliedClimateParams = handlerInfluences->getAppliedClimParams();

	for(auto climParam : appliedClimateParams) {
		StrategyClimate* strategyClimate = loadClimateData(climParam);
		int validationCode = strategyClimate->getValidationCode();
		if(validationCode != 0) {
			return validationCode;
		}
		climateStrategies.push_back(strategyClimate);
	}

	environment->initClimate(climateStrategies);
	return 0;
}

/**
 * Initialize the batch run for a new climate scenario and time slice:
 * - create new output files
 * - set list of mean densities to zero
 * - clear list of used random seeds
 */
bool initBatchRun(int initialSimulationYear, int finalSimulationYear) {
	string gmName = climateScenario->getGlobalModelName();
	string rcpName = climateScenario->getReprConPathName();
	stringstream soloPopKey , filenameStats, filenameStatsYearly, filenameDensities, filenameOrderOfYears;
	if(soloPops) {
		Point soloPopCoord = environment->getSoloPop();
		soloPopKey << "_soloPop";
		if(soloPopCoord.getX() < 9) {
			soloPopKey << "0";
		}
		soloPopKey << (soloPopCoord.getX() + 1) << "_";
		if(soloPopCoord.getY() < 9) {
			soloPopKey << "0";
		}
		soloPopKey << (soloPopCoord.getY() + 1);
	}
	filenameStats << "output/" << folderNameRun.str() << "/" << runID << "_stats_"  << gmName << "_" << rcpName << soloPopKey.str() << "_" << initialSimulationYear << "_" << finalSimulationYear << ".csv";
	filenameStatsYearly << "output/" << folderNameRun.str() << "/" << runID << "_statsYearly_"  << gmName << "_" << rcpName << soloPopKey.str() << "_" << initialSimulationYear << "_" << finalSimulationYear << ".csv";
	filenameOrderOfYears << "output/"<< folderNameRun.str() << "/" << runID << "_orderOfYears_"  << gmName << "_" << rcpName << soloPopKey.str() << "_" << initialSimulationYear << "_" << finalSimulationYear << ".csv";
	fileOrderOfYears = new ofstream(filenameOrderOfYears.str());
	if(yearly) {
		fileStatsYearly = new ofstream(filenameStatsYearly.str());
		*fileStatsYearly << "distSchemeName,xCoord,yCoord,seed,disturbance,year";
	}
	if(!prognosis) {
		if(yearly) {
			*fileStatsYearly << ",lifetime,firstVisit,firstEst,finalDensity,nExtinction,nQuasiExtinction,meanDensity,meanMigration";
		} else {
			fileStats = new ofstream(filenameStats.str());
			*fileStats << "distSchemeName,xCoord,yCoord,seed,disturbance,year";
			*fileStats << ",lifetime,firstVisit,firstEst,finalDensity,nExtinction,nQuasiExtinction,meanDensity,meanMigration";
		}
	}
	*fileOrderOfYears << "seed,year,year_shuffled\n";
	if(meanStatsOutput) {
		stringstream filenameMeanStats;
		filenameMeanStats << "output/"<< folderNameRun.str() << "/" << runID << "_stats_mean"
				<< "_" << gmName << "_" << rcpName << soloPopKey.str() << "_" << initialSimulationYear << "_" << finalSimulationYear << ".csv";
		fileMeanStats = new ofstream(filenameMeanStats.str());
		*fileMeanStats << "distSchemeName,xCoord,yCoord";
		if(prognosis) {
			*fileMeanStats << ",climCellID,GLZ";
		}
		*fileMeanStats << ",disturbance,initialYear,finalYear";
		if(!prognosis) {
			*fileMeanStats << ",lifetime,firstVisit,firstEst,finalDensity,nExtinction,nQuasiExtinction,meanDensity,meanMigration";
		}
	}

	for(auto stage : handlerLifeCylce->getLifeStages()) {
		string stageName = stage->getName();
		bool migratory = stage->isMigratory() && dispersalType != IMMOBILE;
		if(prognosis) {
			*fileMeanStats<< ",meanPeak_" << stageName;
			if(yearly) {
				*fileStatsYearly<< ",meanPeak_" << stageName;
			}
			continue;

		}else{
			if(!yearly) {
				*fileStats << ",firstGain_" << stageName;
				*fileStats << ",lastAlive_" << stageName;
			}
			for(auto yearlyStat : stage->getYearlyStats()) {
				if(migratory || (yearlyStat.first != "immigration"
											&& yearlyStat.first != "emigration"
											&& yearlyStat.first.find("Residents") == std::string::npos
											&& yearlyStat.first.find("Ratio") == std::string::npos)
											)
				{
					if(!yearly) {
						*fileStats << ",yearly_" << yearlyStat.first << "_" << stageName;
					}
					if(meanStatsOutput) {
						*fileMeanStats << ",yearly_" << yearlyStat.first << "_" << stageName;
					}
				}
			}
		}
		if(yearly) {
			*fileStatsYearly << ",firstGain_" << stageName;
			*fileStatsYearly << ",lastAlive_" << stageName;
			for(auto yearlyStat : stage->getYearlyStats()) {
				if(migratory || (yearlyStat.first != "immigration"
											&& yearlyStat.first != "emigration"
											&& yearlyStat.first.find("Residents") == std::string::npos
											&& yearlyStat.first.find("Ratio") == std::string::npos)
											)
				{
					*fileStatsYearly << ",yearly_" << yearlyStat.first << "_" << stageName;
				}
			}
		}
	}
	if(yearly) {
		*fileStatsYearly << "\n";
	}
	if(meanStatsOutput) {
		*fileMeanStats << "\n";
	}
	if(!prognosis && !yearly){
		*fileStats << "\n";
	}

	usedSeeds.clear();
	return true;
}

/*
 * Create a density output file for the current coordinate, if density output is enabled
 */
void initDensOutput(int initialSimulationYear, int finalSimulationYear){
	if(noDens){
		return;
	}
	densityFiles.clear();
	stringstream soloPopKey;
	if(soloPops) {
		Point soloPopCoord = environment->getSoloPop();
		soloPopKey << "_soloPop";
		if(soloPopCoord.getX() < 9) {
			soloPopKey << "0";
		}
		soloPopKey << (soloPopCoord.getX() + 1) << "_";
		if(soloPopCoord.getY() < 9) {
			soloPopKey << "0";
		}
		soloPopKey << soloPopCoord.getY();
	}
	for(auto coord : grasslandCoords) {
		stringstream filenameDensities;
		string gmName = climateScenario->getGlobalModelName();
		string rcpName = climateScenario->getReprConPathName();
		filenameDensities << "output/"<< folderNameRun.str() << "/" << runID << "_densities_"  << gmName << "_" << rcpName << soloPopKey.str() << "_" << initialSimulationYear << "_" << finalSimulationYear << "_";
		if(coord.getX() < 9) {
			filenameDensities << "0";
		}
		filenameDensities << (coord.getX() + 1) << "_";
		if(coord.getY() < 9) {
			filenameDensities << "0";
		}
		filenameDensities << (coord.getY() + 1) << ".csv";
		ofstream *fileDensities = new ofstream(filenameDensities.str());
		*fileDensities << "seed,disturbance_scenario,timestep,iTime,temperature,RHUG,CW,MRSO,SMT";
		for(auto stage : handlerLifeCylce->getLifeStages()) {
			*fileDensities << "," << stage->getName() << "_density";
		}
		*fileDensities << ",pop_density,immigration,emigration";
		*fileDensities << "\n";
		densityFiles[coord] = fileDensities;
	}
}

void outputRun(unsigned int nTimesteps, Population* population) {
	int timestep = environment->getTimestep();
	ofstream *fileDensities;
	if(!noDens) {
		fileDensities = densityFiles.at(currentCoord);
		*fileDensities << environment->getSeedLocal() << "," << disturbanceNameByID.at(currDisturbanceID);
		*fileDensities << "," << timestep;
		*fileDensities << "," << environment->getIndexTime();
		*fileDensities << "," << environment->getValue(TS, currentCoord) << "," << environment->getValue(RHUG, currentCoord) << "," << environment->getValue(CW, currentCoord) << "," << environment->getValue(MRSO, currentCoord) << "," << environment->getValue(SMT, currentCoord);
	}
	vector<LifeStage*> stages = population->getStages();
	for(auto stage : stages) {
		if(noDens) {
			break;
		}
		*fileDensities << "," << stage->getDensity() / environment->getHabitatSize();
	}
	if(!noDens) {
		*fileDensities << "," << population->getDensity() / environment->getHabitatSize();
		*fileDensities << "," << population->getImmigration() / environment->getHabitatSize();
		*fileDensities << "," << population->getEmigration() / environment->getHabitatSize();
		*fileDensities << "\n";
	}
}

void outputFinalDens(unsigned int finalTimestep) {
	if(!createFinalDensFiles) {
		return;
	}
	unsigned int finalYear = Constants::BASE_YEAR;
	finalYear = finalTimestep > 0 ? finalYear + floor((finalTimestep - 1) / environment->getCycleLength()) : finalYear;
	stringstream soloPopKey;
	if(soloPops) {
		Point soloPopCoord = environment->getSoloPop();
		soloPopKey << "_soloPop";
		if(soloPopCoord.getX() < 9) {
			soloPopKey << "0";
		}
		soloPopKey << (soloPopCoord.getX() + 1) << "_";
		if(soloPopCoord.getY() < 9) {
			soloPopKey << "0";
		}
		soloPopKey << soloPopCoord.getY();
	}
	for (auto disturbanceScheme : disturbanceSchemes) { // loop over each disturbance scheme
		int disturbanceSchemeID = disturbanceScheme.first;
		unordered_map<Point, Population*> neighborhood = environment->getNeighborhood(disturbanceSchemeID);
		unordered_map<Point, Population*> baseNeighborhood = environment->getNeighborhood(baseSchemeID);
		stringstream filenameFinalDensities;
		string gmName = climateScenario->getGlobalModelName();
		string rcpName = climateScenario->getReprConPathName();
		filenameFinalDensities 	<< "output/"<< folderNameRun.str() << "/finalDens/" << runID << "_finalDens" << "_seed"  << seeds[seedIdx]
								<< "_" << disturbanceSchemeNameByID.at(disturbanceSchemeID) << "_" << gmName << "_" << rcpName << soloPopKey.str() << "_" << finalYear << ".csv";
		ofstream *fileFinalDensities = new ofstream(filenameFinalDensities.str());
		*fileFinalDensities << "xCoord,yCoord,stageName,density";
		*fileFinalDensities << "\n";
		for(auto coord : grasslandCoords) {
			Population* population = neighborhood.at(coord);
			if(!population) {
				population = baseNeighborhood.at(coord);
			}
			for(auto stage : population->getStages()) {
				*fileFinalDensities << coord.getX() + 1 << "," << coord.getY() + 1 << "," << stage->getName() << "," << stage->getDensitySqm(environment);
				*fileFinalDensities << "\n";
			}
		}
		fileFinalDensities->close();
	}

}

void outputMeanStatsSingle(int initialYear, int finalYear, Population* population, Point coord, string disturbanceSchemeName, string disturbanceName, ofstream *fileRef) {
	*fileRef << disturbanceSchemeName << "," << (coord.getX() + 1) << "," << (coord.getY() + 1);
	if(prognosis) {
		Cell* cell = new Cell(coord);
		for(auto itCell : grasslandCells) {
			if (itCell->getCoord() == coord) {
				cell = itCell;
				break;
			}
		}
		*fileRef << "," << cell->getClimCellID();
		*fileRef << "," << cell->getGLZ(); //GLZ
	}

	*fileRef << ","<< disturbanceName;
	*fileRef << ","<< initialYear;
	*fileRef << ","<< finalYear;
	if(!prognosis) {
		*fileRef << "," << population->getLifetime(true);
		*fileRef << "," << population->getFirstVisit(true);
		*fileRef << "," << population->getFirstEstablishment(true);
		*fileRef << "," << population->getDensity(true) / environment->getHabitatSize();
		*fileRef << "," << population->getNExtinction(true);
		*fileRef << "," << population->getNQuasiExtinction(true);
		*fileRef << "," << population->getMeanDensity(true) / environment->getHabitatSize();
		*fileRef << "," << population->getMeanMigration(true) / environment->getHabitatSize();
	}
	for(auto stage : population->getStages()) {
		bool migratory = stage->isMigratory() && dispersalType != IMMOBILE;
		if(prognosis) {
			*fileRef << "," << stage->getYearlyStat("peak", true) / environment->getHabitatSize();
			continue;
		}
		for(auto yearlyStat : stage->getYearlyStats(true)) {
			if(yearlyStat.first == "lifetime") {
				*fileRef << "," << yearlyStat.second;
				continue;
			}
			if(migratory || (yearlyStat.first != "immigration" && yearlyStat.first != "emigration" && yearlyStat.first.find("Residents") == std::string::npos && yearlyStat.first.find("Ratio") == std::string::npos)) {
				*fileRef << "," << yearlyStat.second / environment->getHabitatSize();
			}
		}
	}
	*fileRef << "\n";
}

void outputMeanStats(int initialYear, int finalYear) {
	if(!meanStatsOutput) {
		return;
	}
	for (auto disturbanceScheme : disturbanceSchemes) { // loop over each disturbance scheme
		unsigned int disturbanceSchemeID = disturbanceScheme.first;
		string disturbanceSchemeName = disturbanceSchemeNameByID[disturbanceSchemeID];
		map<Point, unsigned int> distByCoord = disturbanceScheme.second;
		unordered_map<Point, Population*> neighborhood = environment->getNeighborhood(disturbanceSchemeID);
		for(auto neighbor : neighborhood) {
			Population* population = neighbor.second;
			if(!(population->getLifetime(true) > 0)) {
				continue;
			}
			Point coord = neighbor.first;
			unsigned int distID;
			if(distByCoord.find(coord) != distByCoord.end() && distByCoord.find(Point()) != distByCoord.end()) {
				distID = distByCoord[coord];
			} else if(distByCoord.find(Point()) != distByCoord.end()) {
				distID = distByCoord[Point()];
			} else {
				distID = baseDisturbanceScheme[coord];
			}
			string disturbanceName = disturbanceNameByID.at(distID);
			outputMeanStatsSingle(initialYear, finalYear, population, coord, disturbanceSchemeName, disturbanceName, fileMeanStats);

		}
	}

}

Population* setupPopulation(Point coord, int neighborhoodID, bool printLifeCycle) {
	PopulationAtts* stats = new PopulationAtts(coord, carryingCapacity, dispersalType, longDistDisp, discreteDispersal);
	Population* population = new Population(environment, neighborhoodID, stats);

	vector<LifeStage*> lifeStages = handlerLifeCylce->getLifeStages();

	if(printLifeCycle) {
		cout << "Life cycle setup of a " << lifeStages.size() << " life stage population:" << endl;
	}
	unsigned int iStage = 0;
	unordered_map<string, double> initialDensities = handlerDensities->getDensitiesAt(coord);
	for(auto templStage : lifeStages) {
		Flow* flowMort = handlerLifeCylce->getMortFlow(iStage);
		if(printLifeCycle) {
			cout << "Stage " << iStage + 1 << " [" << templStage->getName()
					<< "] -> minDens=" << templStage->getMinDensity()
					<< ", maxAge=" << templStage->getMaxAge()
					<< ", multiCohort=" << templStage->hasMultiCohorts()
					<< ", capConstr=" << templStage->isAboveGround()
					<< endl;
			cout << "\tBase mortality rate = " << flowMort->getBaseRate() << endl;
		}

		auto itInitDens = initialDensities.find(templStage->getName());
		double initDens = -1.0;
		if(itInitDens != initialDensities.end()) {
			initDens = (*itInitDens).second;
//			cout << templStage->getName() << "[" << coord.getX() + 1 << "," << coord.getY() + 1 <<  "] initDens=" << initDens << endl;
		}
		population->addStage(templStage, initDens);
		population->addFlow(flowMort);

		iStage++;
	}
	if(printLifeCycle) {
		cout << endl;
	}
	for(iStage = 0; iStage < lifeStages.size(); iStage++) {
		Flow* flowTrans = handlerLifeCylce->getTransFlow(iStage);
		Flow* flowRepr = handlerLifeCylce->getReprFlow(iStage);

		if(flowTrans != NULL) {
			population->addFlow(flowTrans);
			if(printLifeCycle) {
				cout << "Transfer from stage[" << flowTrans->getFrom()->getName() << "] to [" << flowTrans->getTo()->getName() << "] with base rate = " << flowTrans->getBaseRate() << endl;
			}
		}

		if(flowRepr != NULL) {
			population->addFlow(flowRepr);
			if(printLifeCycle) {
				cout << "Reproduction from stage [" << flowRepr->getFrom()->getName() << "] to [" << flowRepr->getTo()->getName() << "] with base rate = " << flowRepr->getBaseRate() << endl;
			}
		}
	}
	if(dispersalType == BASE_IMMIGRATION || dispersalType == BOTH) {
		vector<Flow*> baseImmFlows = handlerLifeCylce->getBaseImmFlows();
		for(auto baseImmFlow : baseImmFlows) {
			baseImmFlow->getFrom()->reset(environment, population->getAtts());
//			baseImmFlow->getTo()->addInput(baseImmFlow);
			population->addFlow(baseImmFlow, false);
//			baseImmFlow->update(environment);
		}
	}

	if(handlerInfluences->isEmpty()) {
		return population;
	}
	vector<std::pair<string,Influence*>> devInfluencePairs = handlerInfluences->getDevInfluences();
	for(auto nameInfluencePair : devInfluencePairs) {
		if(!population->addDevInfluence(nameInfluencePair.first, nameInfluencePair.second)) {
			cout << "Dev Influence Associaton ERROR: no Life Stage named " << nameInfluencePair.first << " in Life Cycle" << endl;
			return NULL;
		}
	}
	unordered_map<FlowType,vector<std::pair<string,Influence*>>> flowInfluencesByType = handlerInfluences->getFlowInfluencesByType();
	for(auto& flowInfluenceByType : flowInfluencesByType) {
		if(flowInfluenceByType.first == MIGRATION) {
			continue;
		}
		vector<std::pair<string,Influence*>> flowInfluencePairs = flowInfluenceByType.second;
		for(auto nameInfluencePair : flowInfluencePairs) {
			if(!population->addFlowInfluence(flowInfluenceByType.first, nameInfluencePair.first, nameInfluencePair.second)) {
				cout << "Flow Influence Associaton ERROR: no Life Stage named " << nameInfluencePair.first << " in Life Cycle" << endl;
				return NULL;
			}
		}
	}
	return population;
}

void outputStats(ofstream *statsFile) {
	for (auto disturbanceScheme : disturbanceSchemes) { // loop over each disturbance scheme
		currDisturbanceSchemeID = disturbanceScheme.first;
		unordered_map<Point, Population*> neighborhood = environment->getNeighborhood(currDisturbanceSchemeID);
		unordered_map<Point, Population*> baseNeighborhood = environment->getNeighborhood(baseSchemeID);
		for(auto coord : grasslandCoords) {
			Population* population = neighborhood[coord];
			if(!population) {
				population = baseNeighborhood[coord];
			}
			int disturbanceID = baseDisturbanceScheme[coord];
			for(auto pairCoordDist : disturbanceScheme.second) {
				Point schemeCoord = pairCoordDist.first;
				if(schemeCoord == coord
						|| (currDisturbanceSchemeID != baseSchemeID && schemeCoord == Point(0,0))) {
					disturbanceID = pairCoordDist.second;
				}
			}

			if(population->getLifetime() > 0) {
				*statsFile << disturbanceSchemeNameByID.at(currDisturbanceSchemeID) << ",";
				*statsFile << (coord.getX() + 1) << "," << (coord.getY() + 1) << ","  << seeds[seedIdx] << "," << disturbanceNameByID.at(disturbanceID);
				*statsFile << "," << Constants::BASE_YEAR + environment->getCycle() - 1;
				if(prognosis) {
					for(auto stage : population->getStages()) {
						string stageName = stage->getName();
						*statsFile << "," << stage->getYearlyStat("peak") / environment->getHabitatSize();
					}
				}else{
					int firstVisit = population->getFirstVisit();
					int firstEst = population->getFirstEstablishment();
					*statsFile << ","<< (int) population->getLifetime();
					*statsFile << ",";
					if(firstVisit >= 0) {
						*statsFile << firstVisit;
					}
					*statsFile << ",";
					if(firstEst >= 0) {
						*statsFile << firstEst;
					}
					*statsFile << "," << population->getDensity() / environment->getHabitatSize();
					*statsFile << "," << (int) population->getNExtinction();
					*statsFile << "," << (int) population->getNQuasiExtinction();
					*statsFile << "," << population->getMeanDensity() / environment->getHabitatSize();
					*statsFile << "," << population->getMeanMigration() / environment->getHabitatSize();
					for(auto stage : population->getStages()) {
						bool migratory = stage->isMigratory() && dispersalType != IMMOBILE;
						*statsFile << "," << stage->getDayFirstGain();
						*statsFile << "," << stage->getDayLastAlive();
						for(auto yearlyStat : stage->getYearlyStats()) {
							if(yearlyStat.first == "lifetime" || (migratory && yearlyStat.first.find("Ratio") != std::string::npos)) {
								*statsFile << "," << yearlyStat.second;
								continue;
							}
							if(migratory || (yearlyStat.first != "immigration"
														&& yearlyStat.first != "emigration"
														&& yearlyStat.first.find("Residents") == std::string::npos
														&& yearlyStat.first.find("Ratio") == std::string::npos
														))
							{
								*statsFile << "," << yearlyStat.second / environment->getHabitatSize();
							}
						}
					}
				}
				*statsFile << "\n";
			}
//			else{
//				string emptyPopParams = ",,,,,,,,";
//				string emptyStageParams = ",,,,";
//				if(prognosis) {
//					emptyPopParams = "";
//					emptyStageParams = ",";
//				}
//				*statsFile << emptyPopParams;
//				for(auto stage : population->getStages()) {
//					*statsFile << emptyStageParams;
//				}
//			}
		} // end coord loop for stat output
	}  // end disturbance scheme loop for stat output
}

// END: functions called by function batchRun()
// ***** END   ------- LEVEL 2 ------- *****


// ***** BEGIN ------- LEVEL 1 ------- *****
// BEGIN: functions called by main()

bool handleArgs(int argc, char** argv) {
	//	initially setting values for selected climate scenarios and runtime to -1 (default)
	int iClim = -1;
	int nClim = -1;
	int iYear = -1;
	int nYear = -1;
	int iFrame = -1;
	int nFrame = -1;
	int dYear = -1;
	double cCap = 0;
	int seedID = -1;
	int nSeeds = -1;
	unsigned int numDispersalType = 2;

//	check if there are command line arguments
	if(argc > 1) {
//		process all command line arguments
		for (int i = 1; i < argc; ++i) {
			std::string arg = argv[i];
			int iBeg = i;
			if(i > 1) {
				argString.append(" ");
			}
			argString.append(arg);
			if (arg == "-iClim") { // index of initial climate scenario
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					iClim = atoi(argv[++i]); // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "-nClim") { //	number of climate scenarios
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					nClim = atoi(argv[++i]); // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "-iYear") { // initial simulation year
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					iYear = atoi(argv[++i]); // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "-nYear") { // number of simulation years
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					nYear = atoi(argv[++i]); // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "-dYear") { // delta to next initial simulation year
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					dYear = atoi(argv[++i]); // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "-iFrame") { // initial year of first simulation time frame
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					iFrame = atoi(argv[++i]); // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "-nFrame") { // number of years in a simulation time frame
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					nFrame = atoi(argv[++i]); // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "-nSeeds") { // number of seeds to use
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					nSeeds = atoi(argv[++i]); // Increment 'i' so we don't get the argument as the next argv[i].
					if(nSeeds > 0) {
						seeds.clear();
						int fromSeed = seedID > 0 ? seedID : 1;
						int toSeed = seedID > 0 ? nSeeds + seedID - 1 : nSeeds;
						for(int iSeed = fromSeed; iSeed <= toSeed; iSeed++) {
							seeds.push_back(iSeed);
						}
					}else {
						cout << "WARNING: ignoring argument -nSeeds because a value > 0 needs to be defined." << endl;
					}
				}
			}
			else if (arg == "-seed") { // number of single seed to use
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					seedID = atoi(argv[++i]); // Increment 'i' so we don't get the argument as the next argv[i].
					if(seedID > 0) {
						seeds.clear();
						int toSeed = nSeeds > 0 ? nSeeds + seedID - 1 : seedID;
						for(int iSeed = seedID; iSeed <= toSeed; iSeed++) {
							seeds.push_back(iSeed);
						}
					}else {
						cout << "WARNING: ignoring argument -seed because a value > 0 needs to be defined." << endl;
					}
				}
			}
			else if (arg == "-runID") { // run ID for to identify output files
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					int argRunID = atoi(argv[++i]); // Increment 'i' so we don't get the argument as the next argv[i].
					if(argRunID > 0) {
						runID = argRunID;
					}
				}
			}
			else if (arg == "-limShfl") { // upper/lower limit for picking shuffled years
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					limitShuffleYears = atoi(argv[++i]); // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "-fnameStages") { // filename for life stage setup
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					filenameStages = argv[++i]; // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "-fnameInfl") { // filename for influences on life stage
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					filenameInfluences = argv[++i]; // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "-fnameDist") { // filename for existing disturbance timing
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					filenameDisturbance = argv[++i]; // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "-fnameScheme") { // filename for applied disturbance scheme
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					filenameDisturbanceScheme = argv[++i]; // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "-fnameCells") { // filename for grassland cell coordinates
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					filenameGrasslandCoords = argv[++i]; // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "-fnameDens") { // filename for initial densities per coordinate and life stage
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					filenameInitialDensities = argv[++i]; // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "-dispersal") { // type of applied dispersal
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					numDispersalType = atoi(argv[++i]); // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "--LDD") { // use long distance dispersal
				longDistDisp = true;
			}
			else if (arg == "--discrDisp") { // use discrete dispersal of 'full' individuals
				discreteDispersal = true;
			}
			else if (arg == "--soloPops") { // run sim for each init pop separately
				soloPops = true;
			}
			else if (arg == "--dynDist") { // use dynamic relative disturbance schedule (if applies) following begin of vegetation period (temp. sum >= 200 °C)
				dynamicDisturbanceTiming = true;
			}
			else if (arg == "--climOnly") { // only run scenarios with disturbances
				if(disturbanceAppType == 1) {
					cout << "WARNING: skipping argument -climOnly because --distOnly was defined first";
					cout << endl;
					continue;
				}
				if(uniformDisturbances) {
					cout << "WARNING: skipping argument --uniformDist because --climOnly is defined as well";
					uniformDisturbances = false;
				}
				disturbanceAppType = 0;
			}
			else if (arg == "--distOnly") { // only run scenarios without disturbances
				if(disturbanceAppType == 0) {
					cout << "WARNING: skipping argument --distOnly because --climOnly was defined first";
					cout << endl;
					continue;
				}
				disturbanceAppType = 1;
			}
			else if (arg == "--uniformDist") { // apply disturbances uniformly in all grassland cells and create a scheme for each disturbance
				if(disturbanceAppType == 0) {
					cout << "WARNING: skipping argument --uniformDist because --climOnly is defined as well";
					cout << endl;
					continue;
				}
				uniformDisturbances = true;
			}
			else if (arg == "-cCap") { // carrying capacity
				if (i + 1 < argc) { // Make sure we aren't at the end of argv!
					cCap = atof(argv[++i]); // Increment 'i' so we don't get the argument as the next argv[i].
				}
			}
			else if (arg == "--dens") { // create output file for density per stage
				noDens = false;
			}
			else if (arg == "--movShfl") { // use moving time window to pick years from
				movingShuffleWindow = true;
			}
			else if (arg == "--help") { // print help page
				printHelp();
				return false;
			}
			else if (arg == "--listClim") { // print climate change scenario options
				printClimList();
				return false;
			}
			else if (arg == "--refOut") { // create file with the location of the output
				refOut = true;
			}
			else if (arg == "--climInterpol") { // interpolate climate of subplots (sub cells / habitats) in climate cells
				interpolateClimate = true;
			}
			else if (arg == "--memSave") { // save memory by yearly preloading climate data only
				memorySavingMode = true;
			}
			else if (arg == "--prognosis") { // run in prognosis mode applying all disturbance scenarios in unfixed cells of scheme
				prognosis = true;
				meanStatsOutput = true;
				if(disturbanceAppType == 0) {
					cout << "WARNING: skipping argument --uniformDist because --climOnly is defined as well";
					cout << endl;
					continue;
				}
				uniformDisturbances = true;
			}
			else if (arg == "--yearly") { // output yearly stats
				yearly = true;
			}
			else if (arg == "--finalDens") { // create final density output files
				createFinalDensFiles = true;
			}
			else if (arg == "--dailyPrint") { // print daily simulation info
				dailyPrint = true;
			}
			else if (arg == "--meanStats") { // output of mean stats (will be updated if prognosis is defined and remains active)
				meanStatsOutput = true;
			}
			else {
				cout << "ERROR: invalid command line parameter '" << arg << "' defined. Printing help page" << endl;;
				printHelp();
				return false;
			}
			if(i > iBeg) {
				argString.append(" ");
				argString.append(argv[i]);
			}
		}
	}

	cout << "--- Starting simulation setup ---" << endl << endl;
//	calculate climate scenario indexes from input parameters
	climScenIdxStart = iClim >= 0 ? (((unsigned int) iClim) < climateScenarios.size() ? iClim : 0) : 0;
	climScenIdxEnd = climScenIdxStart + nClim;
	climScenIdxEnd = nClim > 0 && climScenIdxEnd < climateScenarios.size() ? climScenIdxEnd : climScenIdxStart + 1;

	initialTimestep = iYear > Constants::BASE_YEAR ? (iYear - Constants::BASE_YEAR) * Constants::DAYS_PER_YEAR : 0;
	nTimestep = nYear > 0 ? nYear * Constants::DAYS_PER_YEAR : -1;
	initialSimulationYear = iYear > Constants::BASE_YEAR ? iYear : Constants::BASE_YEAR;
	finalSimulationYear = nYear > 0 ? initialSimulationYear + nYear : initialSimulationYear;
	initialYearTimeFrame = iFrame > Constants::BASE_YEAR ? iFrame : initialSimulationYear;
	deltaYearsTimeFrame = nFrame > 0 ? nFrame : finalSimulationYear - initialSimulationYear;
	if(initialYearTimeFrame > initialSimulationYear || (initialYearTimeFrame + deltaYearsTimeFrame) < finalSimulationYear) {
		cout << "ERROR: simuation period [" << initialSimulationYear << "-" << finalSimulationYear
				<< "] is outside time frame [" << initialYearTimeFrame << "-" << (initialYearTimeFrame + deltaYearsTimeFrame)
				<< "]. Please adjust..." << endl;
		return false;
	}
	deltaInit = dYear > 0 && nTimestep > 0 ? dYear * Constants::DAYS_PER_YEAR : -1;
	deltaYears = dYear > 0 ? dYear : 0;

//	BEGIN console output to inform about selected simulation type
	cout << "Running Climate Scenario " << climScenIdxStart;
	if(climScenIdxEnd > climScenIdxStart + 1) {
		cout << " to " << climScenIdxEnd - 1;
	}
	cout << " from year=" << initialSimulationYear;
	if(nTimestep > 0) {
		cout << " for " << nYear << " years.";
		if(deltaInit > 0) {
			cout << " Repeating initial conditions every " << dYear << " years.";
		}
	}else{
		cout << " to end.";
	}
	cout << endl << endl;

	cout << "Creating " << seeds.size() << " replicate(s) using random set of seed(s)={";
	for(unsigned int iSeed = 0; iSeed < seeds.size(); iSeed++) {
		cout << seeds[iSeed];
		if(iSeed < (seeds.size() - 1)) {
			cout << ",";
		}
	}
	cout  << "}" << endl << endl;

	limitShuffleYears = limitShuffleYears < 0 ? 0 : limitShuffleYears;
	if(limitShuffleYears > 0) {
		if(movingShuffleWindow) {
			cout << "Shuffling years using a moving time window of +/- " << limitShuffleYears << " years" << endl;
		} else {
			cout << "Extending limit for shuffling by " << limitShuffleYears << " years" << endl;
		}
	}

	if(filenameStages.empty()) {
		filenameStages = Constants::FILENAME_STAGES;
	}

	if(filenameInfluences.empty()) {
		filenameInfluences = Constants::FILENAME_INFLUENCES;
	}

	if(filenameGrasslandCoords.empty()) {
		filenameGrasslandCoords = Constants::FILENAME_GRASSLAND_COORDS;
	}

	if(filenameDisturbance.empty()) {
		filenameDisturbance = Constants::FILENAME_DISTURBANCES;
	}

	if(filenameDisturbanceScheme.empty()) {
		filenameDisturbanceScheme = Constants::FILENAME_DISTURBANCE_SCHEME;
	}

	if(filenameInitialDensities.empty()) {
		filenameInitialDensities = Constants::FILENAME_INITIAL_DENSITIES;
	}

	if(disturbanceAppType == 0) {
		cout << "Running without disturbance scenarios" << endl;
		cout << endl;
	}else if(disturbanceAppType == 1) {
		cout << "Running disturbance scenarios only" << endl;
	}
	else {
		cout << "Running simulations with and without disturbance scenarios" << endl;
	}
	cout << endl;

	carryingCapacity = cCap > 0 ? cCap : carryingCapacity;
	cout << "Using a carrying capacity of " << carryingCapacity << endl;

	switch (numDispersalType) {
		case 0:
			dispersalType = IMMOBILE;
			cout << "Running simulations without dispersal/migration" << endl;
			break;
		case 1:
			dispersalType = BASE_IMMIGRATION;
			cout << "Using constant base immigration" << endl;
			break;
		case 2:
			cout << "Using dispersal between neighboring cells" << endl;
			dispersalType = DISPERSAL;
			break;
		case 3:
			dispersalType = BOTH;
			cout << "Using both constant base immigration and dispersal between neighboring cells" << endl;
			break;
		default:
			cout << "Invalid dispersal type. Please choose value from 0 to 3" << endl;
			return false;
	}

//	END console output to inform about selected simulation type
	return true;
}

bool handleFileInput() {
	if(!handleCoordinates()) {
		return false;
	}
	// order of call to handler's isValid() method important
	if(!handleDisturbances() && ((handlerDisturbances && !handlerDisturbances->isValid()) || (handlerScheme && !handlerScheme->isValid()))) {
		return false;
	}
	cout << endl << "Loading life cycle from file " << filenameStages << endl;
	handlerLifeCylce = new CSVHandlerLifeCycle(filenameStages);
	if(!handlerLifeCylce->isValid()) {
		cout << "Life cycle creation ERROR: ";
		cout << handlerLifeCylce->getMessage() << endl;
		return false;
	}
	cout << "Life cycle successfully loaded" << endl << endl;
	cout << endl << "Loading external drivers from file " << filenameInfluences << endl;
	handlerInfluences = new CSVHandlerInfluences(filenameInfluences);
	if(!handlerInfluences->isValid()) {
		cout << "Influence creation ERROR: ";
		cout << handlerInfluences->getMessage() << endl;
		return false;
	}
	if(!handlerInfluences->isEmpty()) {
		cout << "Influences successfully loaded" << endl;
	}else {
		cout << "No Influences defined. Running without external drivers" << endl;

	}

	cout << endl << "Loading initial densities from file " << filenameInitialDensities << endl;
	handlerDensities = new CSVHandlerDensities(filenameInitialDensities, handlerLifeCylce->getStageNames());
	if(!handlerDensities->isValid()) {
		cout << "Initial density ERROR: ";
		cout << handlerDensities->getMessage() << endl;
		return false;
	}
	if(!handlerDensities->isEmpty()) {
		cout << "Initial densities successfully loaded" << endl;
	}else {
		cout << "No deviating initial densities defined. Using stage defaults in all cells" << endl;

	}

	return true;
}

bool setup() {
//	unordered_set<Cell*> grasslandCells;
//	for(auto coord : grasslandCoords) {
//		grasslandCells.insert(new Cell(coord, coord));
//	}
	environment = new Environment(climateStrategies, grasslandCells, currentStep, nTimestep);
	environment->setLifeCycleDefinition(handlerLifeCylce);
	environment->setInfluenceDefinitions(handlerInfluences);
	if(memorySavingMode) {
		timestepLimDelta = environment->getCycleLength();
	} else {
		timestepLimDelta = nTimestep < 1 ? 0 : nTimestep + 2 * environment->getCycleLength() * limitShuffleYears;
	}

	bool firstIteration = true;
	unsigned int iDistScheme = 1;
	for (auto disturbanceScheme : disturbanceSchemes) { // loop over each disturbance scheme
		int disturbanceSchemeID = disturbanceScheme.first;
		cout << "INFO: setting up disturbance scheme " << iDistScheme++ << "/" << disturbanceSchemes.size() << ": '" << disturbanceSchemeNameByID.at(disturbanceSchemeID) << "'" << endl;
		unordered_map<Point, Population*> neighborhood;
		unordered_set<Point>  activeNeighbors;
		for(auto coord : grasslandCoords) {
			Population* population = setupPopulation(coord, disturbanceSchemeID, firstIteration);
			if(!population) {
				return false;
			}
			neighborhood[coord] = population;
			firstIteration = false;
		}
		environment->addNeighborhood(disturbanceSchemeID, neighborhood);
	}
	return true;
}

int batchRun() {
//	get current timestamp from system clock
	runID = runID < 0 ? static_cast<long int>(time(NULL)) : runID;

//	BEGIN create output folders
	folderNameRun.str("");
	folderNameRun << "run_" << runID;
	stringstream fullpathRun;
	stringstream fullpathFinalDens;
	fullpathRun << Constants::FOLDERNAME_OUTPUT << "/" << folderNameRun.str();
	environment->setFullpathRun(fullpathRun.str());
	fullpathFinalDens << fullpathRun.str() << "/finalDens";
	int retvalMkdir = 0;
#ifdef WIN32
	mkdir(Constants::FOLDERNAME_OUTPUT);
	retvalMkdir = mkdir(fullpathRun.str().c_str());
	if(createFinalDensFiles) {
		mkdir(fullpathFinalDens.str().c_str());
	}
#else
	mkdir(Constants::FOLDERNAME_OUTPUT, 0755);
	retvalMkdir = mkdir(fullpathRun.str().c_str(), 0755);
	if(createFinalDensFiles) {
		mkdir(fullpathFinalDens.str().c_str(), 0755);
	}
#endif
//	END create output folder

	if(retvalMkdir < 0) {
		if(errno == EEXIST) {
			cout << "TERMINATING: specified runID " << runID << " was already used in a previous simulation. Please choose another one.";
		} else {
			cout << "TERMINATING: Could not create output folder.";
		}
		return retvalMkdir;
	}

//	BEGIN copy args and input files to output folder for future reproduction
	std::ofstream  argFile(fullpathRun.str().append("/").append("args.txt"), std::ios::binary);
	argFile << argString << endl;
	argFile.close();

	tools::copyFile(filenameStages, fullpathRun.str(), Constants::FILENAME_STAGES);
	tools::copyFile(filenameInfluences, fullpathRun.str(), Constants::FILENAME_INFLUENCES);
	tools::copyFile(filenameInitialDensities, fullpathRun.str(), Constants::FILENAME_INITIAL_DENSITIES);
	tools::copyFile(filenameGrasslandCoords, fullpathRun.str(), Constants::FILENAME_GRASSLAND_COORDS);
	tools::copyFile(filenameDisturbance, fullpathRun.str(), Constants::FILENAME_DISTURBANCES);
	tools::copyFile(filenameDisturbanceScheme, fullpathRun.str(), Constants::FILENAME_DISTURBANCE_SCHEME);
//	std::ifstream  src(filenameStages, std::ios::binary);
//	std::ofstream  dst(fullpathRun.str().append("/").append(filenameStages), std::ios::binary);
//	dst << src.rdbuf();
//	dst.close();
//	END copy input files to output folder

	filenameAppliedDisturbanceScheme << "output/"<< folderNameRun.str() << "/" << Constants::FILENAME_APPLIED_DISTURBANCE_SCHEME;
	fileAppliedDistrubanceScheme = new ofstream(filenameAppliedDisturbanceScheme.str());
	*fileAppliedDistrubanceScheme << "seed,xCoord,yCoord,disturbance\n";
	fileAppliedDistrubanceScheme->close();

//	BEGIN create file informing about output folder and simulation type
	if(refOut) {
		stringstream filenameRef;
		filenameRef << "output/ref_sim_folder_"<< runID << ".csv";
		fileRef = new ofstream(filenameRef.str());
		*fileRef << "version_ec_model,climate_model,RCP_scenario";
		*fileRef << "," << "spatial_extent";
		*fileRef << "," << "disturbance_scenario";
		*fileRef << ",disturbance_week" << ",initial_timestep";
		*fileRef << "," << "n_timesteps";
		*fileRef << "," << "n_seeds";
		*fileRef << "," << "lim_shuffle";
		*fileRef << "," << "subfolder_name";
		*fileRef << "\n";
	}
//	END create file informing about output folder and simulation type


	int nClimScens = climScenIdxEnd - climScenIdxStart;
	int iClimScens = 1;

	int cycleLength = environment->getCycleLength();
	int maxAlive = disturbanceSchemes.size() * grasslandCoords.size();
//	TODO when using schemes, sorting is probably not required anymore
//	vector<string> sortedDisturbanceNames;
//	sortedDisturbanceNames.insert(sortedDisturbanceNames.end(), disturbanceNames.begin(), disturbanceNames.end());
//	std::sort(sortedDisturbanceNames.begin(), sortedDisturbanceNames.end());
	for (unsigned int climScenIdx = climScenIdxStart; climScenIdx < climScenIdxEnd; ++climScenIdx) { // loop through requested climate model resp. RCP scenarios
//		set and init climate model resp. RCP scenario
		climateScenario = climateScenarios[climScenIdx];
		cout << "--- Starting simulations of scenario " << climateScenario->getReprConPathName() << " and global model " << climateScenario->getGlobalModelName() << " ---" << endl;
		cout << "Loading climate data timeseries:" << endl;
		timestepLimLower = initialTimestep - cycleLength * limitShuffleYears;
		timestepLimLower = timestepLimLower < 0 ? 0 : timestepLimLower;
		if(initClimate() != 0) {
			cout << "ERROR: FAILED TO INITIALIZE CLIMATE DATA. CHECK FILES." << endl;
			cout << "Climate data files must be located in folder folder ./climate/" << endl;
			cout << "These files can be downloaded from ..." << endl;
			return false;
		}

		nTimestep = nTimestep > 0 ? nTimestep : environment->getClimate()->getNTimesteps();
		finalSimulationYear = finalSimulationYear == initialSimulationYear ? Constants::BASE_YEAR + ceil(nTimestep / environment->getCycleLength()) : finalSimulationYear;
//		calc number of time slices from initial timestep to length of climate time series, depending number of time steps per sim run
		int nTimeSlice = deltaInit > 0 ? (environment->getClimate()->getNTimesteps() - initialTimestep - nTimestep) / deltaInit + 1: 1;
		int initialStepSimulation = initialTimestep;
		int initialYear = initialSimulationYear;
		int finalYear = finalSimulationYear;
		int lowLimTimeFrame = initialYearTimeFrame;
		for(int iTimeSlice = 0; iTimeSlice < nTimeSlice; ++iTimeSlice) { // one iteration per time slices
			if(iTimeSlice > 0) {
//				TODO increment iFrame as well if initYear >= iFrame + dFrame and shuffle years (?)
				initialStepSimulation += deltaInit;
				timestepLimLower += deltaInit;
				initialYear += deltaYears;
				finalYear += deltaYears;
				if(initialYear >= (lowLimTimeFrame + deltaYearsTimeFrame)) {
					lowLimTimeFrame += deltaYearsTimeFrame;
				}
				//	TODO: update time frame of climate
				updateClimate();
//				initClimate();
			}
			cout << "--- Running simulation for time frame " << initialYear << " to " << finalYear << " ---" << endl;

//			TODO loop over each init coord if they are to be processed separately
			unordered_set<Point> soloCoords = {Point()};
			if(soloPops) {
				soloCoords = handlerDensities->getCoords();
			}
			unsigned int nSoloCoord = 1;
			for(auto soloCoord : soloCoords) {
				currSoloCoord = soloCoord;
				environment->setSoloPop(soloCoord);
	//			Initialize the batch run for a new climate scenario and time slice
				if(!initBatchRun(initialYear, finalYear)) {
					return 4;
				}
				// create files for density output for each coordinate
				initDensOutput(initialYear, finalYear);
				for (seedIdx = 0; seedIdx < seeds.size(); ++seedIdx) {
					environment->setSeed(seeds[seedIdx], grasslandCoords);
					updateDisturbanceSchemes(environment->getRandGenGlobal());
					initEnvironment(initialStepSimulation, lowLimTimeFrame);
					unordered_set<Point> lastSchemeCoords;

					unsigned int timestep = environment->getTimestep();
					unsigned int toTimestep = nTimestep > 0 ? timestep + nTimestep : environment->getClimate()->getNTimesteps();
					toTimestep = toTimestep > environment->getClimate()->getNTimesteps() ? environment->getClimate()->getNTimesteps() : toTimestep;
					int simYear = 1;
					int nSimYears = (toTimestep - timestep - 1) / cycleLength + 1;
					milliseconds tBegin = duration_cast< milliseconds >(
						system_clock::now().time_since_epoch()
					);
					unordered_map<int, int> nAlivePrev;
					for (auto disturbanceScheme : disturbanceSchemes) { // loop over each disturbance scheme
	//				for (auto disturbanceName : sortedDisturbanceNames) {
						nAlivePrev[disturbanceScheme.first] = -1;
					}
					unsigned int nTimesteps = 0;
	//				loop from beginning to end of time frame
	//				TODO DEFINE the next time frame to load climate data from
					for (; timestep < toTimestep; ++timestep) {
						milliseconds dailyBegin = duration_cast< milliseconds >(
							system_clock::now().time_since_epoch()
						);
						int nAliveTotal = 0;
						int nAlivePrevTotal = 0;
	//					TODO load next time frame if not the full time slice is looped
						for (auto disturbanceScheme : disturbanceSchemes) { // loop over each disturbance scheme
							currDisturbanceSchemeID = disturbanceScheme.first;
							for(auto lastSchemeCoord : lastSchemeCoords) {
								unsigned int baseDisturbanceID = baseDisturbanceScheme[lastSchemeCoord];
								string distName = disturbanceNameByID[baseDisturbanceID];
								environment->setDisturbanceAt(lastSchemeCoord, baseDisturbanceID);
							}
							lastSchemeCoords.clear();
							if(nAlivePrev[currDisturbanceSchemeID] == 0 && dispersalType != BASE_IMMIGRATION && dispersalType != BOTH) { // all dead and no base immigration
//								benchmark.stop(10);
								continue;
							}
							nAlivePrev[currDisturbanceSchemeID] = 0;
							if(disturbanceSchemes.size() > 1) {
								for(auto pairCoordDist : disturbanceScheme.second) {
									Point currSchemeCoord = pairCoordDist.first;
									unsigned int newDisturbanceID = pairCoordDist.second;
									string distName = disturbanceNameByID[newDisturbanceID];
									if(!uniformDisturbances) {
										lastSchemeCoords.insert(currSchemeCoord);
									}
									environment->setDisturbanceAt(currSchemeCoord, newDisturbanceID);
								}
							}
							unordered_map<Point, Population*> activeNeighborhood = environment->getActiveNeighborhood(currDisturbanceSchemeID);
//							unordered_set<Point> newActiveNeighborhood;
							for(auto activeNeighbor : activeNeighborhood) {
								Population* population = activeNeighbor.second;
								if(dispersalType != DispersalType::IMMOBILE // dispersal and/or base immigration
										|| population->isAlive()) { // or population not extinct
									unordered_set<Point> activatedNeighbors = population->updateFlows(environment);
									environment->addActiveNeighbors(currDisturbanceSchemeID, activatedNeighbors);
									if(population->isAlive()) {
										nAlivePrev[currDisturbanceSchemeID]++;
										nAlivePrevTotal++;
									}
								}
							}
							unsigned int iCoord = 0;
	//						loop through all defined coordinates
							activeNeighborhood = environment->getActiveNeighborhood(currDisturbanceSchemeID); // get updated list of neighbors
							for(auto activeNeighbor : activeNeighborhood) {
								currentCoord = activeNeighbor.first;
								Population* population = activeNeighbor.second;
								currDisturbanceID = environment->getDisturbanceAt(currentCoord);
								iCoord++;

								if(dispersalType != DispersalType::IMMOBILE // dispersal and/or base immigration
										|| population->isAlive()) { // and population extinct
									population->updateStages(environment);
									population->updateStats(environment);
									outputRun(nTimesteps, population);
									if(population->isAlive()) {
										nAliveTotal++;
									}else{
										environment->removeActiveNeighbor(currDisturbanceSchemeID, currentCoord);
									}
								}
							} // end coord loop
						} // end disturbance loop
						if(dailyPrint) {
							milliseconds dailyEnd = duration_cast< milliseconds >(
								system_clock::now().time_since_epoch()
							);
							cout << "year=" << simYear << "/" << nSimYears;
							cout << " | day=" << timestep % cycleLength + 1 << "/" << cycleLength;
							cout << " | alive=" << nAliveTotal << "/" << maxAlive;
							cout << " | duration=";
							cout << dailyEnd - dailyBegin;
							cout << endl;
						}
						if((timestep + 1) == toTimestep || (timestep + 1) % cycleLength == 0) {
							if(yearly) {
								outputStats(fileStatsYearly);
							}
							milliseconds tEnd = duration_cast< milliseconds >(
								system_clock::now().time_since_epoch()
							);
							if(soloPops) {
								cout << "soloCoord=" << nSoloCoord << "/" << soloCoords.size() << " [" << soloCoord.getX() << "," << soloCoord.getY() << "] | ";
							}
							cout << "year=" << simYear << "/" << nSimYears;
							cout << " | scenario=" << iClimScens << "/" << nClimScens << " (" << climateScenario->getGlobalModelName() << "_" << climateScenario->getReprConPathName() << ")";
							cout << " | epoche=" << (iTimeSlice + 1) << "/" << nTimeSlice << " (" << initialYear << "-" << finalYear << ")";
							cout << " | seed=" << (seedIdx + 1) << "/" << seeds.size() << " (" << seeds[seedIdx] << ")";
							cout << " | #disturbanceSchemes=" << disturbanceSchemes.size();
							cout << " | #cells=" << grasslandCoords.size();
							cout << " | alive=" << nAliveTotal << "/" << maxAlive;
							cout << " | duration=";
							cout << tEnd - tBegin;
							cout << endl;

							tBegin = duration_cast< milliseconds >(
										system_clock::now().time_since_epoch()
									);
							simYear++;
						}
						if(nAlivePrevTotal == 0 && dispersalType != BASE_IMMIGRATION) {
							cout << "All population extinct in simulation time frame. Skipping" << endl;
							break;
						}
//						benchmark.mark(2);
						if(environment->update()) {
							updateClimate();
						}
//						benchmark.stop(2);
						nTimesteps++;
					} // end loop begin to end of time frame
					if(!prognosis && !yearly) {
						outputStats(fileStats);
					}
					if(refOut && seedIdx == 0) {
						for (auto disturbanceScheme : disturbanceSchemes) { // loop over each disturbance scheme
							currDisturbanceSchemeID = disturbanceScheme.first;
							*fileRef << "," << "," << climScenIdx;
							*fileRef << "," << grasslandCoords.size();
							*fileRef << "," << disturbanceSchemeNameByID.at(currDisturbanceSchemeID);
							*fileRef << "," << "," << initialStepSimulation;
							*fileRef << "," << nTimestep;
							*fileRef << "," << seeds.size();
							*fileRef << "," << limitShuffleYears;
							*fileRef << ",run_" << runID;
							*fileRef << "\n";
						}  // end disturbance scheme loop for ref output
					} //
					outputFinalDens(toTimestep);
				} // end seed loop
				outputMeanStats(initialYear, finalYear);
				if(!noDens) { // ???
					for(auto pDensityFile : densityFiles) {
						pDensityFile.second->close();
					}
				}
				if(yearly) {
					fileStatsYearly->close();
				}
				if(meanStatsOutput) {
					fileMeanStats->close();
				}else if(!yearly) {
					fileStats->close();
				}
				if(!soloPops) {
					break;
				}
				nSoloCoord++;
			} // end solo coord loop
		} // end time slice
		if(refOut) {
			fileRef->close();
		}
		iClimScens++;
	} // end climate scenarios
	return runID;
}

// END: functions called by main()
// ***** END   ------- LEVEL 1 ------- *****


/**
 * main function handling model execution
 */
int main(int argc, char** argv) {
	cout << "---------------------- (High resolution Large Environmental Gradient) ----------------------\n" << endl << endl;
	if(!handleArgs(argc, argv)) {
		return 1;
	}
	if(!handleFileInput()) {
		return 2;
	}

	if(!setup()) {
		return 3;
	}

//	Start batch run and end application afterwards
	return batchRun();
//	return batchRun();
}
