/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "StrategyClimate.h"

const char* StrategyClimateNetCDF::determineVarIndex() {
	return Constants::VAR_NAMES_NETCDF[climVar].c_str();
}

StrategyClimate::StrategyClimate(ClimateVariable climVar, int xDim, int yDim, size_t nTime) {
	this->climVar = climVar;
	this->nTime = nTime;
	this->xDim = xDim;
	this->yDim = yDim;
}

int StrategyClimate::getValidationCode() const {
	return validationCode;
}

ClimateVariable StrategyClimate::getClimateVariable() const {
	return climVar;
}

float StrategyClimate::getRelativeValue(unsigned int timestep, unsigned int iTime, Cell* cell) {
	return getValue(timestep, iTime, cell) / getMaxValue(cell);
}

size_t StrategyClimate::getTime() const {
	return nTime;
}

int StrategyClimate::actualIndexTime(int iTime) {
	return iTime % nTime;
}

StrategyClimateNetCDF::StrategyClimateNetCDF(string filename, mt19937 randGen, unordered_set<Point> climCoords, ClimateVariable climVar, unsigned int initTimestep, unsigned int nTimesteps, unsigned int nBefore, bool loadAll) : StrategyClimate(climVar) {
// ToDo: move file handling outside of climate and make it generic

	cout << "StrategyClimateNetCDF.Constructor(): " << filename << endl;
	this->filename = filename;
	this->randGen = randGen;
	this->loadAll = loadAll;
	this->nBefore = nBefore;
	resetTimeframe(initTimestep, true, nTimesteps);

	this->climCoords.insert(climCoords.begin(), climCoords.end());
	prepareData();
}

bool StrategyClimateNetCDF::skipValue() {
	if(calendarType[2]) {
		return false;
	}

	if(calendarType[1]) {
		int dayUnix = time_value - Constants::DAYS_1949_12_01_TO_1970_01_01_365_DAYS;
		int dayOfYear = (dayUnix % 365) + 1;
		//	skip December the 31st
		if(dayOfYear == 365) {
			return true;
		}
		return false;
	}
	int dayUnix = time_value - Constants::DAYS_1949_12_01_TO_1970_01_01_GREGORIAN;
	auto daysSince70 = sys_days{days{dayUnix}};

	auto currDate = year_month_day{daysSince70};
	auto currMonth = currDate.month();
	auto currDay = currDate.day();
//	skip December the 31st
	if(currMonth == date::December && currDay == day(31)) {
		return true;
	}
//	skip January the first in a leap year
	if(currDate.year().is_leap() && currMonth == date::January && currDay == day(1)) {
		return true;
	}
	return false;
}

bool StrategyClimateNetCDF::isEndOfYear() {
	if(calendarType[2]) {
		int dayUnix = time_value - Constants::DAYS_1949_12_01_TO_1970_01_01_360_DAYS;
		return ((dayUnix % 360) + 1) == 360;
	}

	else if(calendarType[1]) {
		int dayUnix = time_value - Constants::DAYS_1949_12_01_TO_1970_01_01_365_DAYS;
		return ((dayUnix % 365) + 1) == 364;
	}

	int dayUnix = time_value - Constants::DAYS_1949_12_01_TO_1970_01_01_GREGORIAN;
	auto daysSince70 = sys_days{days{dayUnix}};

	auto currDate = year_month_day{daysSince70};
	auto currMonth = currDate.month();
	auto currDay = currDate.day();
	if(currMonth == date::December && currDay == day(30)) {
		return true;
	}

	return false;
}

bool StrategyClimateNetCDF::duplicateValue() {
	if(!calendarType[2]) {
		return false;
	}
	int dayUnix = time_value - Constants::DAYS_1949_12_01_TO_1970_01_01_360_DAYS;
	int dayOfYear = dayUnix % 360;
	if(dayOfYear == 0) {
		uniform_int_distribution<> uniDistDay(0,359);
		idxsDupl[0] = 0;
		idxsDupl[1] = 0;
		idxsDupl[2] = 0;
		idxsDupl[3] = 0;
		//create 4 random timesteps
		for(int a = 0; a < 4; a++) {
			int day = 0;
			bool doubleHit = false;
			do {
				day = uniDistDay(randGen);
				doubleHit = false;
				for(int b = 0; b < a; b++) {
					if(idxsDupl[b] == day) {
						doubleHit = true;
						break;
					}
				}
			}while(doubleHit);
			idxsDupl[a] = day;
		}
	}
	if(dayOfYear == idxsDupl[0] || dayOfYear == idxsDupl[1] || dayOfYear == idxsDupl[2] || dayOfYear == idxsDupl[3]) {
		return true;
	}
	return false;
}

void StrategyClimateNetCDF::resetTimeframe(int initTimestep, bool reloadValues, int nTimesteps) {
	if(initTimestep < 0) {
		return;
	}
	this->initTimestep = initTimestep > nBefore ? initTimestep - nBefore : 0;
	if(nTimesteps >= 0) { // flagged as unchanged if below zero
		this->nTimesteps = nTimesteps > 0 ? nTimesteps + nBefore : 0; // value of 0 means to use length of data set
	}
	if(reloadValues) {
		loadValues(-1);
	}
}

float StrategyClimateNetCDF::getValue(unsigned int timestep, unsigned int iTime, Cell* cell) {
	loadValues(iTime);
	int iActual = actualIndexTime(iTime);
	float climateValue = 0.0;
	for(auto pairClimCoordInterpolRatio : cell->getInterpolRatioByClimCoord()) {
		climateValue += valuesByCoord.at(pairClimCoordInterpolRatio.first)[iActual] * pairClimCoordInterpolRatio.second;
	}
	return climateValue;
}

float StrategyClimateNetCDF::getRelativeValue(unsigned int timestep, unsigned int iTime, Cell* cell) {
	loadValues(iTime);
	int iActual = actualIndexTime(iTime);
	float relativeValue = 0.0;
	for(auto pairClimCoordInterpolRatio : cell->getInterpolRatioByClimCoord()) {
		float climateValue = valuesByCoord.at(pairClimCoordInterpolRatio.first)[iActual];
		float meanMaxValue = meanMaxValues.at(pairClimCoordInterpolRatio.first);
		relativeValue += (climateValue / meanMaxValue) * pairClimCoordInterpolRatio.second;
	}
	return relativeValue;
}

float StrategyClimateNetCDF::getMeanMaxValue(Cell* cell) const {
	float meanMaxValue = 0.0;
	for(auto pairClimCoordInterpolRatio : cell->getInterpolRatioByClimCoord()) {
		meanMaxValue += meanMaxValues.at(pairClimCoordInterpolRatio.first) * pairClimCoordInterpolRatio.second;
	}
	return meanMaxValue;
}

float StrategyClimateNetCDF::getMaxValue(Cell* cell) {
	return getMeanMaxValue(cell);
}

void StrategyClimateNetCDF::prepareData() {
	const char* varName = determineVarIndex();
	parameterID = 0;
	/* Open the file. NC_NOWRITE tells netCDF we want read-only access
	* to the file.*/
	validationCode = nc_open(filename.c_str(), NC_NOWRITE, &ncid);
	if (validationCode) {
		cout << "ERROR: nc_open(): "<< validationCode << endl;
		return;
//		ToDo: implement error handling during file handling
	}

//	inquire the ID of the time variable
	validationCode = nc_inq_varid(ncid, labelTime, &timeID);
	if (validationCode) {
		cout << "ERROR: nc_inq_varid(" << labelTime << "): "<< validationCode << endl;
		return;
//		ToDo: implement error handling during file handling
	}

//  inquire the number of dimensions the netcdf file has
	validationCode = nc_inq_ndims (ncid, &nDimensions);
	if (validationCode) {
		cout << "ERROR: nc_inq_ndims(): "<< validationCode << endl;
		return;
//		ToDo: implement error handling during file handling
	}

//  Defining dimid outside of loop header might be necessary because there was
//  some unexcpected behaviour with some netcdf functions if done otherwise
	int dimid;
	lengthDimTime = 0;
	for (dimid=0; dimid < nDimensions; dimid++) {
		char dimname[NC_MAX_NAME+1];
		size_t dimlength;
		validationCode = nc_inq_dim(ncid, dimid, dimname, &dimlength);
		if (validationCode) {
			cout << "ERROR: nc_inq_dim(" << dimname << "): "<< validationCode << endl;
			return;
	//		ToDo: implement error handling during file handling
		}

		if(strcmp(dimname, labelTime) == 0) {
			lengthDimTime = dimlength;
			break;
        }
	}
	validationCode = nc_inq_varid(ncid, varName, &parameterID);
	if (validationCode) {
		cout << "ERROR: nc_inq_varid(" << varName << "): "<< validationCode << endl;
		return;
//		ToDo: implement error handling during file handling
	}
	size_t t_len;

	validationCode = nc_inq_attlen(ncid, timeID, "calendar", &t_len);
	if (validationCode) {
		cout << "ERROR: nc_inq_attlen(timeID)" << validationCode << endl;
		return;
//		ToDo: implement error handling during file handling
	}
	t_len += 1;
	char* calendarName = (char *) malloc(t_len);
	validationCode = nc_get_att_text(ncid, timeID, "calendar", calendarName);
	if (validationCode) {
		cout << "ERROR: nc_get_att_text(calendar)" << validationCode << endl;
		return;
//		ToDo: implement error handling during file handling
	}
	int iCal = 0;
	for (auto cName : Constants::CALENDAR_NAMES) {
		int cLength = strlen(cName.c_str());
		calendarType[iCal++] = strncmp(calendarName, cName.c_str(), cLength) == 0;
	}

//	TODO recode to prevent duplicate use of coord loop / close file in prepare() and loadValues()
	for(auto coord : climCoords) {
		loadValuesAt(coord, true);
	}
	validationCode = nc_close(ncid);
	if (validationCode) {
		cout << "ERROR: nc_close(): "<< validationCode << endl;
		return;
	}
}

void StrategyClimateNetCDF::loadValues(int iTime) {
	if(iTime >= 0 && iTime >= initTimestep && iTime < (initTimestep + nTimesteps)){
		return;
	}
	resetTimeframe(iTime, false);
	int toTimestep = initTimestep + nTimesteps;
	toTimestep = nTime > 1 && toTimestep > nTime ? nTime : toTimestep;
//	cout << "Loading climate variable " << this->climVar << " from " << initTimestep << " to " << toTimestep << endl;
	validationCode = nc_open(filename.c_str(), NC_NOWRITE, &ncid);
	if (validationCode) {
		cout << "ERROR: nc_open(): "<< validationCode << endl;
		return;
	}
	valuesByCoord.clear();
	for(auto coord : climCoords) {
		loadValuesAt(coord, false);
	}
	validationCode = nc_close(ncid);
	if (validationCode) {
		cout << "ERROR: nc_close(): "<< validationCode << endl;
		return;
	}
}
void StrategyClimateNetCDF::loadValuesAt(Point coord, bool prepare) {
	bool firstPrepare = prepare && timestepToIndex.size() == 0;

	this->iLon = (size_t) coord.getX();
	this->iLat = (size_t) coord.getY();

	unordered_map<unsigned int,float> values;
// set maxima to an unrealistic small values to asure first loaded value is bigger
	float maxValue = -1000000.0;
	float maxValueYear = -1000000.0;
	float meanMaxValue = 0;
	int nYears = 0;
	int skipCount = 0;
	unsigned int iValue = prepare ? 0 : initTimestep;
	unsigned int iFinal = nTimesteps == 0 ? (prepare ? lengthDimTime : nTime) : initTimestep + nTimesteps;
	if(!firstPrepare && iFinal > nTime) {
		iFinal = nTime;
	}
	unsigned int fromTime = prepare ? 0 : timestepToIndex[initTimestep];
	fromTime = fromTime < 0 ? 0 : (fromTime > lengthDimTime ? lengthDimTime : fromTime);
	unsigned int toTime = prepare ? lengthDimTime : timestepToIndex[iFinal];
	if(toTime <= fromTime || toTime > lengthDimTime) {
		toTime = lengthDimTime;
	}

	dim3[2] = iLon;
	dim3[1] = iLat;
	dim3[0] = fromTime;

	if(firstPrepare) {
		nTime = 0;
	}

	// Read first value of time
	dim1[0] = dim3[0];
	validationCode = nc_get_var1_float(ncid, timeID, dim1, &time_value);
	if (validationCode) {
		//ToDo: implement error handling during file handling
		cout << "ERROR: nc_get_var1_float(time): "<< validationCode << endl;
		return;
	}

//	TODO: put handling of values into separate method to use per clim cell
	for (unsigned int iTime = fromTime; iTime < toTime; iTime++) {
		if(skipValue()) {
			skipCount++;
			time_value += 1.0;
			continue;
		}
		bool withinBounds = iValue >= initTimestep && iValue < iFinal;
		bool duplicate = duplicateValue();
		int nAdd = 0;
		if(prepare || withinBounds) {
			float var_value;
			dim3[0] = iTime;
			validationCode = nc_get_var1_float(ncid, parameterID, dim3, &var_value);
			if (validationCode) {
				//ToDo: implement error handling during file handling
				cout << "ERROR: nc_get_var1_float(value): "<< validationCode << endl;
				return;
			}
			if(climVar == TS || climVar == TASMAX || climVar == TASMIN) {
				var_value -= Constants::ZERO_KELVIN;
			}else if(climVar == PR || climVar == SMT) {
				var_value *= Constants::SECOND_TO_DAY;
			}

			if(duplicate) {
				nAdd++;
				if(nTimesteps == 0 && prepare) {
					iFinal++;
				}
				if(withinBounds) {
					float interpolValue = (values[iValue - 1] + var_value) / 2.0;
					values[iValue] = interpolValue;
				}
			}
			if(withinBounds) {
				values[iValue + nAdd] = var_value;
			}
			maxValue = var_value > maxValue ? var_value : maxValue;
			maxValueYear = var_value > maxValueYear ? var_value : maxValueYear;
			if(isEndOfYear() || iTime == lengthDimTime) {
				meanMaxValue = (maxValueYear + nYears * meanMaxValue) / (nYears + 1);
				nYears++;
				maxValueYear = -1000000.0;
			}
		}
		iValue += 1 + nAdd;
		if(firstPrepare) {
			timestepToIndex.push_back(iTime);
			for(int iAdd = 0; iAdd < nAdd; iAdd++) {
				timestepToIndex.push_back(iTime);
			}
			nTime += 1 + nAdd;
		}
		time_value += 1.0;
	}

	valuesByCoord[coord] = values;

	if(prepare) {
		maxValues[coord] = maxValue;
		meanMaxValues[coord] = meanMaxValue;
		nTimesteps = nTimesteps == 0 ? nTime : nTimesteps;
	}
}

StrategyClimateVTS::StrategyClimateVTS(StrategyClimate* tasmaxStrategy, StrategyClimate* tsStrategy, StrategyClimate* tasminStrategy, unsigned int deltaDays, float minTasmin) : StrategyClimate(VTS) {
	this->tasmaxStrategy = tasmaxStrategy;
	this->tsStrategy = tsStrategy;
	this->tasminStrategy = tasminStrategy;
	this->deltaDays = deltaDays;
	this->minTasmin = minTasmin;
}

float StrategyClimateVTS::getValue(unsigned int timestep, unsigned int iTime, Cell* cell) {
	unsigned int daysSinceDisturbance = timestep - cell->getLastDisturbanceTimestep();
	float valueTS = tsStrategy->getValue(timestep, iTime, cell);
	float valueVTS = valueTS;

	if(daysSinceDisturbance < deltaDays) {
		float valueTASMAX = tasmaxStrategy->getValue(timestep, iTime, cell);
		valueVTS = valueTS + (1.0 - daysSinceDisturbance/deltaDays) * (valueTASMAX - valueTS) / 2.0;
	} else {
		daysSinceDisturbance = daysSinceDisturbance < 2 * deltaDays ? daysSinceDisturbance : 2 * deltaDays;
		float valueTASMIN = tasminStrategy->getValue(timestep, iTime, cell);
		valueVTS = valueTS - (daysSinceDisturbance - deltaDays) / deltaDays  * (valueTS - valueTASMIN) / 2.0;
	}
	return valueVTS;
}

float StrategyClimateVTS::getMaxValue(Cell* cell) {
	float valueTS = tsStrategy->getMaxValue(cell);
	float valueTASMAX = tasmaxStrategy->getMaxValue(cell);
	return (valueTASMAX - valueTS) / 2.0;
}

StrategyClimateRSMC::StrategyClimateRSMC(StrategyClimate* mrsoStrategy, int depthTotal, int depthLayer) : StrategyClimate(RSMC) {
	this->mrsoStrategy = mrsoStrategy;
	validationCode = mrsoStrategy->getValidationCode();
	validationCode = mrsoStrategy->getValidationCode();
	this->depthTotal = depthTotal;
	this->depthLayer = depthLayer;
	nTime = mrsoStrategy->getTime();
}

float StrategyClimateRSMC::getValue(unsigned int timestep, unsigned int iTime, Cell* cell) {
	float relativeValue = mrsoStrategy->getRelativeValue(timestep, iTime, cell);

	if(relativeValue > 1.0) {
		return 1.0;
	}

	float depthTrapeze = depthTotal * sqrt(2.0 * (1 - relativeValue));

	if(depthTrapeze <= 0) {
		return 1.0;
	}
	if(depthLayer >= depthTrapeze) {
		return 1.0 - (depthTrapeze * depthTrapeze) / (2 * depthTrapeze * depthTotal);
	}
	return 1.0 - depthTrapeze / depthTotal + depthLayer / (2 * depthTotal);

}

float StrategyClimateRSMC::getMaxValue(Cell* cell) {
	return 1.0; // should always be 1.0 because it's an ratio compared to the max of MRSO series
}

StrategyClimateCMT::StrategyClimateCMT(StrategyClimate* prStrategy, StrategyClimate* smtStrategy, int nWeights) : StrategyClimate(CMT) {
	this->prStrategy = prStrategy;
	this->smtStrategy = smtStrategy;
	if(prStrategy->getValidationCode() != 0 || smtStrategy->getValidationCode() != 0) {
		validationCode = 1;
	}
	float sum = 0.0;
	for(int n = 1; n <= nWeights; n++) {
		float weight = 2.0 / (n + 1.0);
		weights.push_back(weight);
		sum += weight;
		summedWeights.push_back(sum);
	}
	nTime = prStrategy->getTime();
}

float StrategyClimateCMT::getValue(unsigned int timestep, unsigned int iTime, Cell* cell) {
	float prValue = prStrategy->getValue(timestep, iTime, cell);
	float smtValue = smtStrategy->getValue(timestep, iTime, cell);
	float diffValue = prValue - smtValue;
	float cmtValue = diffValue < 0.0 ? 0.0 : diffValue;

	// time step delta to calculate the moving average relevant for values at the beginning of the simulation
	int movDelta = iTime <= weights.size() ? iTime : weights.size();

	for(int iWeight = 0; iWeight < movDelta; iWeight++) {
		float ratio = weights[iWeight] / summedWeights[movDelta - 1];
		int iTimeDelta = iTime - 1 - iWeight;
		prValue = prStrategy->getValue(timestep, iTimeDelta, cell);
		smtValue = smtStrategy->getValue(timestep, iTimeDelta, cell);
		diffValue = (2.0 * (prValue - smtValue) * ratio) / 3.0;
		cmtValue += diffValue < 0.0 ? 0.0 : diffValue;
	}
	return cmtValue;
}

float StrategyClimateCMT::getMaxValue(Cell* cell) {
	//TODO: find a good way to calc or replace method by something to find max value ad hoc
	return 0.0;
}

StrategyClimateRHUG::StrategyClimateRHUG(StrategyClimate* rsmcStrategy,	float thdSated, bool limitAtThd) : StrategyClimate(RHUG) {
	this->rsmcStrategy = rsmcStrategy;
	validationCode = rsmcStrategy->getValidationCode();
	this->thdSated = thdSated;
	this->limitAtThd = limitAtThd;
	nTime = rsmcStrategy->getTime();
}

float StrategyClimateRHUG::getValue(unsigned int timestep, unsigned int iTime, Cell* cell) {
	float rsmcValue = rsmcStrategy->getValue(timestep, iTime, cell);
	float rhugValue = rsmcValue / thdSated;
	return limitAtThd && rhugValue > 1.0 ? 1.0 : rhugValue;
}

float StrategyClimateRHUG::getMaxValue(Cell* cell) {
	if(limitAtThd) {
		return 1.0;
	}
	float rsmcMax = rsmcStrategy->getMaxValue(cell);
	return rsmcMax / thdSated;
}

StrategyClimateCW::StrategyClimateCW(StrategyClimate* cmtStrategy, float thdCMT, StrategyClimate* rsmcStrategy, float thdRSMC) : StrategyClimate(CW) {
	this->cmtStrategy = cmtStrategy;
	this->rsmcStrategy = rsmcStrategy;
	if(cmtStrategy->getValidationCode() != 0 || rsmcStrategy->getValidationCode() != 0) {
		validationCode = 1;
	}
	this->thdCMT = thdCMT;
	this->thdRSMC = thdRSMC;
	nTime = cmtStrategy->getTime();
}

float StrategyClimateCW::getValue(unsigned int timestep, unsigned int iTime, Cell* cell) {
	float cmtValue = cmtStrategy->getValue(timestep, iTime, cell);
	float rsmcValue = rsmcStrategy->getValue(timestep, iTime, cell);
	return cmtValue < thdCMT ? (rsmcValue < thdRSMC ? 0.0 : thdCMT) : cmtValue;
}

float StrategyClimateCW::getMaxValue(Cell* cell) {
	return cmtStrategy->getMaxValue(cell);
}
