/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PopulationH
#define PopulationH

#include <vector>
#include "Environment.h"
#include "PopulationAtts.h"
#include "CSVHandler.h"

using namespace std;

class Population
{
private:
	Vitality vitality;
	int neighborhoodID;
	PopulationAtts* atts;
	vector<LifeStage*> stages;
	vector<Flow*> flows;
	vector<Flow*> emigrationFlows;
	vector<Flow*> immigrationFlows;
	unordered_map<FlowType,vector<Flow*>> flowsByType;
	unordered_map<string, double> meanStats;
	int nInit, nVisit, nEst;
	double density, meanDensity, sumDensity, meanMigration, sumMigration;
	unsigned int lifetime, nExtinction;
	int lastUpdate, firstVisit, firstEstablishment;
	bool extinct;

public:
	Population(Environment* environment, int neighborhoodID = 1, PopulationAtts* stats = new PopulationAtts(Point(17,17), 25, DISPERSAL));

	int getNeighborhoodID();

	double getDensity(bool useMean = false);

	double getMigration() const;

	double getEmigration() const;

	double getImmigration() const;

	void addStage(LifeStage* stage, double initDens = -1.0);

	void addFlow(Flow* templFlow, bool cloneFrom = true, bool cloneTo = true);

	bool addDevInfluence(string stageName ,Influence* devInfluence);

	bool addFlowInfluence(FlowType type, string stageName ,Influence* flowInfluence);

	vector<LifeStage*> getStages();

	LifeStage* getStage(string name);

	unordered_set<Point> updateFlows(Environment* environment);

	void updateStages(Environment* environment);

	void updateStats(Environment* environment);

	void init(Environment* environment, PopulationAtts* stats = NULL);

	void resetEmigration(Environment* environment);

	bool isAlive();

	PopulationAtts* getAtts() const;

	Point getCoord();

	int getILon();

	int getILat();

	double getCarryingCapacity() const;
	double getLifetime(bool useMean = false);
	double getMeanDensity(bool useMean = false);
	double getMeanMigration(bool useMean = false);
	double getNExtinction(bool useMean = false);
	double getNQuasiExtinction(bool useMean = false);
	Vitality getVitality() const;
	int getFirstEstablishment(bool useMean = false);
	int getFirstVisit(bool useMean = false);
};

#endif
