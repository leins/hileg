/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ConstraintsH
#define ConstraintsH

#include "Environment.h"

class Constraint
{

public:
	virtual ~Constraint(){};
	virtual bool isSatisfied(Environment* environment) = 0;

};

class ConstraintDefault : public Constraint
{

private:
	bool defaultValue;

public:
	ConstraintDefault(bool defaultValue=true);
	bool isSatisfied(Environment* environment);

};

#endif
