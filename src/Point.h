/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef POINT_H_
#define POINT_H_

#include <functional>

class Point {
private:
	int x, y;
public:
	Point(int x = 0, int y = 0);
	int getX() const;
	int getY() const;

	bool operator< (const Point &ob) const;
	bool operator== (const Point &ob) const;
	bool operator!= (const Point &ob) const;
};

// 	apapted from https://prateekvjoshi.com/2014/06/05/using-hash-function-in-c-for-user-defined-classes/
namespace std
{
    template <>
    struct hash<Point>
    {
//    	typedef Point argument_type;
//    	typedef std::size_t result_type;

        size_t
		operator()(const Point& p) const
        {
            // Compute individual hash values for two data members and combine them using XOR and bit shifting
            return ((hash<int>()(p.getX()) ^ (hash<int>()(p.getY()) << 1)) >> 1);
        }
    };
}

#endif /* POINT_H_ */
