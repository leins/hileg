/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Constants.h"

namespace Constants
{
    // actual global variables
	const char* FOLDERNAME_OUTPUT("output");
	extern const std::string FILENAME_STAGES("stages.csv");
	extern const std::string FILENAME_INFLUENCES("influences.csv");
	extern const std::string FILENAME_DISTURBANCES("disturbances.csv");
	extern const std::string FILENAME_DISTURBANCE_SCHEME("distScheme.csv");
	extern const std::string FILENAME_APPLIED_DISTURBANCE_SCHEME("appliedDistScheme.csv");
	extern const std::string FILENAME_GRASSLAND_COORDS("grasslandCells.csv");
	extern const std::string FILENAME_INITIAL_DENSITIES("initialDens.csv");
	extern const float THRESHOLD_RMUG(0.26);
	extern const float THRESHOLD_CW(0.98);
	extern const float TEMPERATURE_SUM_VEGETATION_START(200.0);
	extern const float ZERO_KELVIN(273.15);
	extern const unsigned int GRASSLAND_CELL_WIDTH(250);
	extern const unsigned int CLIMATE_CELL_WIDTH(12000);
	extern const float GRASSLAND_PER_CLIMATE_CELL_1D(((float) CLIMATE_CELL_WIDTH) / GRASSLAND_CELL_WIDTH);
	extern const float STRAIN_FACTOR_GRASSLAND_X(52.0 / GRASSLAND_PER_CLIMATE_CELL_1D);
	extern const float STRAIN_FACTOR_GRASSLAND_Y(-50.0 / GRASSLAND_PER_CLIMATE_CELL_1D);
	extern const int OFFSET_GRASSLAND_X(864);
	extern const int OFFSET_GRASSLAND_Y(-1818);
    extern const int SECOND_TO_DAY(60 * 60 * 24);
	extern const double EPSILON(0.00001);
    extern const int DAYS_1949_12_01_TO_1970_01_01_GREGORIAN(7336);
    extern const int DAYS_1949_12_01_TO_1970_01_01_365_DAYS(7331);
    extern const int DAYS_1949_12_01_TO_1970_01_01_360_DAYS(7230);
    extern const int BASE_YEAR(1970);
    extern const int DAYS_PER_YEAR(364);
    extern const std::string NAME_DISTURBANCE_NONE("none");
    extern const unsigned int ID_DISTURBANCE_NONE(1);
    extern const std::unordered_map<GlobalModel,std::string> GLOBAL_CLIMATE_MODEL_NAMES_BY_ENUM({
    	{CCCma, "CCCma-CanESM2"},
		{CNRM, "CNRM-CERFACS-CNRM-CM5"},
		{ICHEC, "ICHEC-EC-EARTH"},
		{MIROC, "MIROC-MIROC5"},
		{MOHC, "MOHC-HadGEM2-ES"},
		{MPI, "MPI-M-MPI-ESM-LR"}
    });
    extern const std::unordered_map<ReprConcPath,std::string> RCP_NAMES_BY_ENUM({
    	{RCP26, "rcp26"},
		{RCP45, "rcp45"},
		{RCP85, "rcp85"}
    });
    extern const std::string CLIMATE_MODEL_REGION("EUR-11");
    extern const std::unordered_map<GlobalModel,std::string> GLOBAL_CLIMATE_REALISATION_BY_MODEL({
    	{CCCma, "r1i1p1"},
		{CNRM, "r1i1p1"},
		{ICHEC, "r12i1p1"},
		{MIROC, "r1i1p1"},
		{MOHC, "r1i1p1"},
		{MPI, "r1i1p1"}
    });
    extern const std::string REGIONAL_CLIMATE_MODEL("CLMcom-CCLM4-8-17");
    extern const std::string CLIMATE_TIME_STEP("day");
    extern const std::string CLIMATE_TIME_PERIOD("1970-2080");
    extern const std::unordered_map<std::string,ClimateVariable> CLIMATE_VARIABLE_NAMES({
    		{"cmt",CMT},
			{"cw",CW},
			{"hurs",HURS},
			{"huss",HUSS},
			{"mrso",MRSO},
			{"par",PAR},
			{"pr",PR},
			{"rhug",RHUG},
			{"rsmc",RSMC},
			{"sfcwind",SFCWIND},
			{"smd",SMD},
			{"smt",SMT},
			{"sund",SUND},
			{"tas",TAS},
			{"tasmax",TASMAX},
			{"tasmin",TASMIN},
			{"ts",TS},
			{"vaps",VAPS},
			{"vts",VTS},
			{"wdirgeo_10m",WDIRGEO_10}
    });
    extern const std::unordered_map<ClimateVariable,std::string> CLIMATE_VARIABLE_NAMES_BY_ENUM({
    		{CMT, "cmt"},
			{CW, "cw"},
			{HURS, "hurs"},
			{HUSS, "huss"},
			{MRSO, "mrso"},
			{PAR, "par"},
			{PR, "pr"},
			{RHUG, "rhug"},
			{RSMC, "rsmc"},
			{SFCWIND, "sfcwind"},
			{SMD, "smd"},
			{SMT, "smt"},
			{SUND, "sund"},
			{TAS, "tas"},
			{TASMAX, "tasmax"},
			{TASMIN, "tasmin"},
			{TS, "ts"},
			{VAPS, "vaps"},
			{VTS, "vts"},
			{WDIRGEO_10, "wdirgeo_10m"}
    });
    extern const std::unordered_map<std::string,DensityLevel> DENSITY_LEVEL_NAMES({
    		{"cohort", COHORT},
			{"stage", LIFE_STAGE},
			{"below", BELOW_GROUND},
			{"above", ABOVE_GROUND},
			{"pop", POPULATION}
    });
    extern const std::unordered_map<DensityLevel,std::string> DENSITY_LEVEL_NAMES_BY_ENUM({
    		{COHORT, "cohort"},
			{LIFE_STAGE, "stage"},
			{BELOW_GROUND, "below"},
			{ABOVE_GROUND, "above"},
			{POPULATION, "pop"}
    });
    extern const std::vector<std::string> VAR_NAMES_NETCDF({"cmt", "cw", "hurs", "huss", "mrso", "par", "pr", "rhug", "rsmc", "sfcwind", "smd", "smt", "sund", "tas", "tasmax", "tasmin", "ts", "vaps", "wdirgeo_10"});
    extern const std::vector<std::string> CALENDAR_NAMES({"proleptic_gregorian", "365_day", "360_day"});
	extern const std::vector<std::string> VALID_INFLUENCE_PROCESS_NAMES({"dist", "stat", "cap", "lin", "exp", "sig", "thd", "bin"});
	extern const std::vector<std::string> VALID_INFLUENCE_PROCESS_NAMES_VARIABLE({"lin", "exp", "sig", "thd"});
	extern const std::vector<std::string> VALID_INFLUENCE_TYPES({"add", "mult"});
	extern const std::vector<std::string> VALID_INFLUENCE_TARGETS({"stage", "infl"});
	extern const std::unordered_map<std::string,FlowType> MAPPED_VALID_FLOW_TYPE_NAMES({ {"trans",FlowType::TRANSFER}, {"migr",FlowType::MIGRATION}, {"mort",FlowType::MORTALITY}, {"repr",FlowType::REPRODUCTION} });
//	valid coordinates for Northwest Germany and surrounding areas as of climate data available in publication Leins et al. 2020
	extern const std::unordered_set<Point> VALID_COORDINATES_NW_GERMANY({
		Point(0,0),Point(1,0),Point(2,0),Point(3,0),Point(4,0),Point(5,0),Point(6,0),Point(7,0),Point(8,0),Point(9,0),Point(10,0),Point(11,0),Point(12,0),Point(13,0),Point(14,0),Point(15,0),Point(16,0),Point(17,0),Point(18,0),Point(19,0),Point(20,0),Point(21,0),Point(22,0),Point(23,0),Point(24,0),Point(25,0),Point(26,0),Point(27,0),Point(28,0),Point(29,0),Point(30,0),Point(31,0),Point(32,0),Point(33,0),Point(34,0),Point(35,0),
		Point(0,1),Point(1,1),Point(2,1),Point(3,1),Point(4,1),Point(5,1),Point(6,1),Point(7,1),Point(8,1),Point(9,1),Point(10,1),Point(11,1),Point(12,1),Point(13,1),Point(14,1),Point(15,1),Point(16,1),Point(17,1),Point(18,1),Point(19,1),Point(20,1),Point(21,1),Point(22,1),Point(23,1),Point(24,1),Point(25,1),Point(26,1),Point(27,1),Point(28,1),Point(29,1),Point(30,1),Point(31,1),Point(32,1),Point(33,1),Point(34,1),Point(35,1),
		Point(0,2),Point(1,2),Point(2,2),Point(3,2),Point(4,2),Point(5,2),Point(6,2),Point(7,2),Point(8,2),Point(9,2),Point(10,2),Point(11,2),Point(12,2),Point(13,2),Point(14,2),Point(15,2),Point(16,2),Point(17,2),Point(18,2),Point(19,2),Point(20,2),Point(21,2),Point(22,2),Point(23,2),Point(24,2),Point(25,2),Point(26,2),Point(27,2),Point(28,2),Point(29,2),Point(30,2),Point(31,2),Point(32,2),Point(33,2),Point(34,2),Point(35,2),
		Point(0,3),Point(1,3),Point(2,3),Point(3,3),Point(4,3),Point(5,3),Point(6,3),Point(7,3),Point(8,3),Point(9,3),Point(10,3),Point(11,3),Point(12,3),Point(13,3),Point(14,3),Point(15,3),Point(16,3),Point(17,3),Point(18,3),Point(19,3),Point(20,3),Point(21,3),Point(22,3),Point(23,3),Point(24,3),Point(25,3),Point(26,3),Point(27,3),Point(28,3),Point(29,3),Point(30,3),Point(31,3),Point(32,3),Point(33,3),Point(34,3),Point(35,3),
		Point(0,4),Point(1,4),Point(2,4),Point(3,4),Point(4,4),Point(5,4),Point(6,4),Point(7,4),Point(8,4),Point(9,4),Point(10,4),Point(11,4),Point(12,4),Point(13,4),Point(14,4),Point(15,4),Point(16,4),Point(17,4),Point(18,4),Point(19,4),Point(20,4),Point(21,4),Point(22,4),Point(23,4),Point(24,4),Point(25,4),Point(26,4),Point(27,4),Point(28,4),Point(29,4),Point(30,4),Point(31,4),Point(32,4),Point(33,4),Point(34,4),Point(35,4),
		Point(0,5),Point(1,5),Point(2,5),Point(3,5),Point(4,5),Point(5,5),Point(6,5),Point(7,5),Point(8,5),Point(9,5),Point(10,5),Point(11,5),Point(12,5),Point(13,5),Point(14,5),Point(15,5),Point(16,5),Point(17,5),Point(18,5),Point(19,5),Point(20,5),Point(21,5),Point(22,5),Point(23,5),Point(24,5),Point(25,5),Point(26,5),Point(27,5),Point(28,5),Point(29,5),Point(30,5),Point(31,5),Point(32,5),Point(33,5),Point(34,5),Point(35,5),
		Point(0,6),Point(1,6),Point(2,6),Point(3,6),Point(4,6),Point(5,6),Point(6,6),Point(7,6),Point(8,6),Point(9,6),Point(10,6),Point(11,6),Point(12,6),Point(13,6),Point(14,6),Point(15,6),Point(16,6),Point(17,6),Point(18,6),Point(19,6),Point(20,6),Point(21,6),Point(22,6),Point(23,6),Point(24,6),Point(25,6),Point(26,6),Point(27,6),Point(28,6),Point(29,6),Point(30,6),Point(31,6),Point(32,6),Point(33,6),Point(34,6),Point(35,6),
		Point(0,7),Point(1,7),Point(2,7),Point(3,7),Point(4,7),Point(5,7),Point(6,7),Point(7,7),Point(8,7),Point(9,7),Point(10,7),Point(11,7),Point(12,7),Point(13,7),Point(14,7),Point(15,7),Point(16,7),Point(17,7),Point(18,7),Point(19,7),Point(20,7),Point(21,7),Point(22,7),Point(23,7),Point(24,7),Point(25,7),Point(26,7),Point(27,7),Point(28,7),Point(29,7),Point(30,7),Point(31,7),Point(32,7),Point(33,7),Point(34,7),Point(35,7),
		Point(0,8),Point(1,8),Point(2,8),Point(3,8),Point(4,8),Point(5,8),Point(6,8),Point(7,8),Point(8,8),Point(9,8),Point(10,8),Point(11,8),Point(12,8),Point(13,8),Point(14,8),Point(15,8),Point(16,8),Point(17,8),Point(18,8),Point(19,8),Point(20,8),Point(21,8),Point(22,8),Point(23,8),Point(24,8),Point(25,8),Point(26,8),Point(27,8),Point(28,8),Point(29,8),Point(30,8),Point(31,8),Point(32,8),Point(33,8),Point(34,8),Point(35,8),
		Point(0,9),Point(1,9),Point(2,9),Point(3,9),Point(4,9),Point(5,9),Point(6,9),Point(7,9),Point(8,9),Point(9,9),Point(10,9),Point(11,9),Point(12,9),Point(13,9),Point(14,9),Point(15,9),Point(16,9),Point(17,9),Point(18,9),Point(19,9),Point(20,9),Point(21,9),Point(22,9),Point(23,9),Point(24,9),Point(25,9),Point(26,9),Point(27,9),Point(28,9),Point(29,9),Point(30,9),Point(31,9),Point(32,9),Point(33,9),Point(34,9),Point(35,9),
		Point(0,10),Point(1,10),Point(2,10),Point(3,10),Point(4,10),Point(5,10),Point(6,10),Point(7,10),Point(8,10),Point(9,10),Point(10,10),Point(11,10),Point(12,10),Point(13,10),Point(14,10),Point(15,10),Point(16,10),Point(17,10),Point(18,10),Point(19,10),Point(20,10),Point(21,10),Point(22,10),Point(23,10),Point(24,10),Point(25,10),Point(26,10),Point(27,10),Point(28,10),Point(29,10),Point(30,10),Point(31,10),Point(32,10),Point(33,10),Point(34,10),Point(35,10),
		Point(0,11),Point(1,11),Point(2,11),Point(3,11),Point(4,11),Point(5,11),Point(6,11),Point(7,11),Point(8,11),Point(9,11),Point(10,11),Point(11,11),Point(12,11),Point(13,11),Point(14,11),Point(15,11),Point(16,11),Point(17,11),Point(18,11),Point(19,11),Point(20,11),Point(21,11),Point(22,11),Point(23,11),Point(24,11),Point(25,11),Point(26,11),Point(27,11),Point(28,11),Point(29,11),Point(30,11),Point(31,11),Point(32,11),Point(33,11),Point(34,11),Point(35,11),
		Point(0,12),Point(1,12),Point(2,12),Point(3,12),Point(4,12),Point(5,12),Point(6,12),Point(7,12),Point(8,12),Point(9,12),Point(10,12),Point(11,12),Point(12,12),Point(13,12),Point(14,12),Point(15,12),Point(16,12),Point(17,12),Point(18,12),Point(19,12),Point(20,12),Point(21,12),Point(22,12),Point(23,12),Point(24,12),Point(25,12),Point(26,12),Point(27,12),Point(28,12),Point(29,12),Point(30,12),Point(31,12),Point(32,12),Point(33,12),Point(34,12),Point(35,12),
		Point(0,13),Point(1,13),Point(2,13),Point(3,13),Point(4,13),Point(5,13),Point(6,13),Point(7,13),Point(8,13),Point(9,13),Point(10,13),Point(11,13),Point(12,13),Point(13,13),Point(14,13),Point(15,13),Point(16,13),Point(17,13),Point(18,13),Point(19,13),Point(20,13),Point(21,13),Point(22,13),Point(23,13),Point(24,13),Point(25,13),Point(26,13),Point(27,13),Point(28,13),Point(29,13),Point(30,13),Point(31,13),Point(32,13),Point(33,13),Point(34,13),Point(35,13),
		Point(0,14),Point(1,14),Point(2,14),Point(3,14),Point(4,14),Point(5,14),Point(6,14),Point(7,14),Point(8,14),Point(9,14),Point(10,14),Point(11,14),Point(12,14),Point(13,14),Point(14,14),Point(15,14),Point(16,14),Point(17,14),Point(18,14),Point(19,14),Point(20,14),Point(21,14),Point(22,14),Point(23,14),Point(24,14),Point(25,14),Point(26,14),Point(27,14),Point(28,14),Point(29,14),Point(30,14),Point(31,14),Point(32,14),Point(33,14),Point(34,14),Point(35,14),
		Point(0,15),Point(1,15),Point(2,15),Point(3,15),Point(4,15),Point(5,15),Point(6,15),Point(7,15),Point(8,15),Point(9,15),Point(10,15),Point(11,15),Point(12,15),Point(13,15),Point(14,15),Point(15,15),Point(16,15),Point(17,15),Point(18,15),Point(19,15),Point(20,15),Point(21,15),Point(22,15),Point(23,15),Point(24,15),Point(25,15),Point(26,15),Point(27,15),Point(28,15),Point(29,15),Point(30,15),Point(31,15),Point(32,15),Point(33,15),Point(34,15),Point(35,15),
		Point(1,16),Point(2,16),Point(3,16),Point(4,16),Point(5,16),Point(6,16),Point(7,16),Point(8,16),Point(9,16),Point(10,16),Point(11,16),Point(12,16),Point(13,16),Point(14,16),Point(15,16),Point(16,16),Point(17,16),Point(18,16),Point(19,16),Point(20,16),Point(21,16),Point(22,16),Point(23,16),Point(24,16),Point(25,16),Point(26,16),Point(27,16),Point(28,16),Point(29,16),Point(30,16),Point(31,16),Point(32,16),Point(33,16),Point(34,16),Point(35,16),
		Point(1,17),Point(2,17),Point(3,17),Point(4,17),Point(5,17),Point(6,17),Point(7,17),Point(8,17),Point(9,17),Point(10,17),Point(11,17),Point(12,17),Point(13,17),Point(14,17),Point(15,17),Point(16,17),Point(17,17),Point(18,17),Point(19,17),Point(20,17),Point(21,17),Point(22,17),Point(23,17),Point(24,17),Point(25,17),Point(26,17),Point(27,17),Point(28,17),Point(29,17),Point(30,17),Point(31,17),Point(32,17),Point(33,17),Point(34,17),Point(35,17),
		Point(2,18),Point(3,18),Point(4,18),Point(5,18),Point(6,18),Point(7,18),Point(8,18),Point(9,18),Point(10,18),Point(11,18),Point(12,18),Point(13,18),Point(14,18),Point(15,18),Point(16,18),Point(17,18),Point(18,18),Point(19,18),Point(20,18),Point(21,18),Point(22,18),Point(23,18),Point(24,18),Point(25,18),Point(26,18),Point(27,18),Point(28,18),Point(29,18),Point(30,18),Point(31,18),Point(32,18),Point(33,18),Point(34,18),Point(35,18),
		Point(1,19),Point(2,19),Point(3,19),Point(4,19),Point(5,19),Point(6,19),Point(7,19),Point(8,19),Point(9,19),Point(10,19),Point(11,19),Point(12,19),Point(13,19),Point(14,19),Point(15,19),Point(16,19),Point(17,19),Point(18,19),Point(19,19),Point(20,19),Point(21,19),Point(22,19),Point(23,19),Point(24,19),Point(25,19),Point(26,19),Point(27,19),Point(28,19),Point(29,19),Point(30,19),Point(31,19),Point(32,19),Point(33,19),Point(34,19),Point(35,19),
		Point(1,20),Point(2,20),Point(3,20),Point(4,20),Point(5,20),Point(6,20),Point(7,20),Point(8,20),Point(9,20),Point(10,20),Point(11,20),Point(12,20),Point(13,20),Point(14,20),Point(15,20),Point(16,20),Point(17,20),Point(18,20),Point(19,20),Point(20,20),Point(21,20),Point(22,20),Point(23,20),Point(24,20),Point(25,20),Point(26,20),Point(27,20),Point(28,20),Point(29,20),Point(30,20),Point(31,20),Point(32,20),Point(33,20),Point(34,20),Point(35,20),
		Point(1,21),Point(2,21),Point(3,21),Point(4,21),Point(5,21),Point(6,21),Point(7,21),Point(8,21),Point(9,21),Point(11,21),Point(12,21),Point(13,21),Point(14,21),Point(15,21),Point(16,21),Point(17,21),Point(18,21),Point(19,21),Point(20,21),Point(21,21),Point(22,21),Point(23,21),Point(24,21),Point(25,21),Point(26,21),Point(27,21),Point(28,21),Point(29,21),Point(30,21),Point(31,21),Point(32,21),Point(33,21),Point(34,21),Point(35,21),
		Point(2,22),Point(3,22),Point(4,22),Point(5,22),Point(6,22),Point(7,22),Point(8,22),Point(10,22),Point(11,22),Point(12,22),Point(13,22),Point(14,22),Point(15,22),Point(17,22),Point(18,22),Point(19,22),Point(20,22),Point(21,22),Point(22,22),Point(23,22),Point(24,22),Point(25,22),Point(26,22),Point(27,22),Point(28,22),Point(29,22),Point(30,22),Point(31,22),Point(32,22),Point(33,22),Point(34,22),Point(35,22),
		Point(10,23),Point(11,23),Point(12,23),Point(13,23),Point(14,23),Point(15,23),Point(18,23),Point(19,23),Point(20,23),Point(21,23),Point(22,23),Point(23,23),Point(24,23),Point(25,23),Point(26,23),Point(27,23),Point(28,23),Point(29,23),Point(30,23),Point(31,23),Point(32,23),Point(33,23),Point(34,23),Point(35,23),
		Point(11,24),Point(12,24),Point(13,24),Point(14,24),Point(15,24),Point(18,24),Point(19,24),Point(20,24),Point(21,24),Point(22,24),Point(23,24),Point(24,24),Point(25,24),Point(26,24),Point(27,24),Point(28,24),Point(29,24),Point(30,24),Point(31,24),Point(32,24),Point(33,24),Point(34,24),Point(35,24),
		Point(19,25),Point(21,25),Point(22,25),Point(23,25),Point(24,25),Point(25,25),Point(26,25),Point(27,25),Point(28,25),Point(29,25),Point(30,25),Point(35,25),
		Point(21,26),Point(22,26),Point(23,26),Point(24,26),Point(25,26),Point(26,26),Point(27,26),Point(28,26),Point(29,26),Point(30,26),Point(31,26),
		Point(21,27),Point(22,27),Point(23,27),Point(24,27),Point(25,27),Point(26,27),Point(27,27),Point(28,27),Point(29,27),Point(30,27),Point(31,27),Point(32,27),
		Point(21,28),Point(22,28),Point(23,28),Point(24,28),Point(25,28),Point(26,28),Point(27,28),Point(28,28),Point(29,28),Point(31,28),Point(32,28),
		Point(20,29),Point(21,29),Point(22,29),Point(23,29),Point(24,29),Point(25,29),Point(26,29),Point(27,29),Point(28,29),Point(33,29),
		Point(21,30),Point(22,30),Point(23,30),Point(24,30),Point(25,30),Point(26,30),
		Point(21,31),Point(22,31),Point(23,31),Point(24,31),Point(25,31),Point(26,31),Point(34,31),Point(35,31),
		Point(21,32),Point(22,32),Point(23,32),Point(24,32),Point(25,32),Point(26,32),Point(33,32),Point(34,32),Point(35,32),
		Point(19,33),Point(21,33),Point(22,33),Point(23,33),Point(24,33),Point(25,33),Point(31,33),Point(33,33),Point(34,33),
		Point(20,34),Point(21,34),Point(22,34),Point(23,34),Point(24,34),Point(25,34),Point(26,34),Point(27,34),
		Point(21,35),Point(22,35),Point(23,35),Point(24,35),Point(25,35),Point(26,35),Point(29,35),Point(30,35),Point(31,35)
	});


}


