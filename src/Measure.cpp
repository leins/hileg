/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Disturbance.h"


Measure::Measure(unsigned int ID, bool relativeTiming, bool followVegetation, int durationDays, UsageType typeOfUse,
		int typeOfStrips, MowingPatternType mowingPatternType,
		int widthOfStrips, double stockingRate, bool fertilizerUsed,
		bool mandatory, double payment, bool insideOutMown, int cuttingHeight)
{
	this->ID = ID;
	this->duration = durationDays;
	this->typeOfUse = typeOfUse;
	this->typeOfStrips = typeOfStrips;
	this->mowingPattern = new MowingPattern(mowingPatternType);
	this->stockingRate = stockingRate;
	this->fertilizerUsed = fertilizerUsed;
	this->mandatory = mandatory;
	this->payment = payment;
	this->weekOfLastUse = -1;
	this->timestepOfLastUse = -1;
	this->insideOutMown = insideOutMown;
	this->cuttingHeight = cuttingHeight;

	this->relativeTiming = relativeTiming;
	this->followVegetation = followVegetation;

	this->mownRatio = 0.0;
	if(typeOfUse == MOWING) {
		//	calculate the amount or rather the rather of the mown grass within this grid cell
//		TODO if this ever comes to use, replace Constants::GRID_CELL_WIDTH with getHabitatWidth() from Environment
		this->mownRatio = 1.0 - ((double) (mowingPattern->getStripWidth() * mowingPattern->getNStrips())) / ((double) Constants::GRASSLAND_CELL_WIDTH);
	}
}

Measure::Measure(unsigned int ID, vector<int> daysOfUse, bool relativeTiming, bool followVegetation, int durationDays, UsageType typeOfUse,
		int typeOfStrips, MowingPatternType mowingPatternType,
		int widthOfStrips, double stockingRate, bool fertilizerUsed,
		bool mandatory, double payment, bool insideOutMown, int cuttingHeight)
			: Measure(ID, relativeTiming, followVegetation, durationDays, typeOfUse, typeOfStrips, mowingPatternType,	widthOfStrips, stockingRate, fertilizerUsed, mandatory, payment, insideOutMown, cuttingHeight)
{
	timing.insert(daysOfUse.begin(), daysOfUse.end());
}

Measure::Measure(unsigned int ID, bool followVegetation, int weekOfFirstUse, int deltaSecondUse, int deltaThirdUse, int deltaFourthUse, int durationWeeks, UsageType typeOfUse, int typeOfStrips, MowingPatternType mowingPatternType,
		int widthOfStrips, double stockingRate, bool fertilizerUsed, bool mandatory, double payment, bool insideOutMown, int cuttingHeight)
			: Measure(ID, true, followVegetation, durationWeeks * 7, typeOfUse, typeOfStrips, mowingPatternType,	widthOfStrips, stockingRate, fertilizerUsed, mandatory, payment, insideOutMown, cuttingHeight)
{
	startingWeeks.insert(weekOfFirstUse);
	int dayOfFirstUse = (weekOfFirstUse - 1) * 7;
	timing.insert(dayOfFirstUse);
	int lastWeekOfUse = weekOfFirstUse;
	int lastDayOfUse = dayOfFirstUse;

	if(deltaSecondUse > 0) {
		lastWeekOfUse += deltaSecondUse;
		lastDayOfUse += deltaSecondUse * 7;
		startingWeeks.insert(lastWeekOfUse);
		timing.insert(lastDayOfUse);
	}

	if(deltaThirdUse > 0) {
		lastWeekOfUse += deltaSecondUse;
		lastDayOfUse += deltaSecondUse * 7;
		startingWeeks.insert(lastWeekOfUse);
		timing.insert(lastDayOfUse);
	}

	if(deltaFourthUse > 0) {
		lastWeekOfUse += deltaSecondUse;
		lastDayOfUse += deltaSecondUse * 7;
		startingWeeks.insert(lastWeekOfUse);
		timing.insert(lastDayOfUse);
	}
}

bool Measure::hasSchedule() {
	return timing.size() > 0;
}

int Measure::getDuration() {
	return duration;
}

bool Measure::isMandatory() {
	return mandatory;
}

MowingPattern* Measure::getMowingPattern() {
	return mowingPattern;
}

double Measure::getPayment() {
	return payment;
}

double Measure::getStockingRate() {
	return stockingRate;
}

int Measure::getTypeOfStrips() {
	return typeOfStrips;
}

UsageType Measure::getTypeOfUse() {
	return typeOfUse;
}

bool Measure::isFertilizerUsed() {
	return fertilizerUsed;
}

int Measure::getLastUse() {
	return timestepOfLastUse;
}

int Measure::getNextUse() {
	if(timing.size() == 0) {
		return -1;
	}
	return *std::next(timing.begin(), iNextUse);
}

int Measure::getWeekOfLastUse() {
	return weekOfLastUse;
}

void Measure::resetLastUse(unsigned int timestep, int week) {
	this->weekOfLastUse = week;
	this->timestepOfLastUse = timestep;
	iNextUse = (iNextUse + 1) < timing.size() ? iNextUse + 1 : 0;
}

bool Measure::hasRelativeTiming() {
	return relativeTiming;
}

bool Measure::doesFollowVegetation() {
	return followVegetation;
}

int Measure::getCuttingHeight() {
	return cuttingHeight;
}

bool Measure::isInsideOutMown() {
	return insideOutMown;
}

double Measure::getMownRatio() {
	return mownRatio;
}

unsigned int Measure::getID() {
	return ID;
}
