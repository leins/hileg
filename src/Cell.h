/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SRC_CELL_H_
#define SRC_CELL_H_

#include "Disturbance.h"
#include "Point.h"
#include <functional>
#include <unordered_map>

class Cell {
private:
	Point coord, climCoord;
	unordered_map<unsigned int, Measure*> measureByID;
	unsigned int activeMeasureID, GLZ, climCellID;
	double coverRatio;
	unsigned int lastUpdate = 0;
	int vegetationDay = -1;
	float temperatureSum = 0.0;
	unordered_map<Point,float> interpolRatioByClimCoord;
public:
	Cell(const Point& coord = Point(), const Point& climCoord = Point(), double coverRatio = 1.0, unsigned int GLZ = 0, unsigned int climCellID = 0, Measure* measure = NULL);
	Point getClimCoord();
	Point getCoord();
	double getCoverRatio();
	void addMeasure(Measure *measure = NULL);
	void clearMeasures();
	Measure* getMeasure(unsigned int measureID = 0);
	void setActiveMeasure(unsigned int measureID);
	void setInterpolRatio(Point climCoordInterpol, float ratio);
	int getLastDisturbanceTimestep(unsigned int measureID = 0);
	unsigned int getLastUpdateTimestep();
	int getDayOfVegetation();
	float getTemperatureSum();
	void reset(unsigned int timestep, int vegetationDay = -1, float temperatureSum = 0.0);

	bool operator< (const Cell &ob) const;
	bool operator== (const Cell &ob) const;
	bool operator!= (const Cell &ob) const;
	const unordered_map<Point, float>& getInterpolRatioByClimCoord() const;
	unsigned int getGLZ() const;
	unsigned int getClimCellID() const;
};

#endif /* SRC_CELL_H_ */
