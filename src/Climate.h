/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ClimateH
#define ClimateH

#include <vector>
#include "Constants.h"
#include "Cell.h"
#include "StrategyClimate.h"
#include <string.h>
#include <unordered_map>

using namespace std;

class Climate
{
private:
	int startValue;
	int rangeValues;
    size_t nTimesteps; //number of timesteps resp. time dimension

    unordered_map<ClimateVariable, StrategyClimate*> climateStrategiesByVar;
    unordered_set<ClimateVariable> climateVars;

    Point currCoord;
    unordered_map<ClimateVariable, float> currValsByVar;
    int lastTimestep = -1;

public:
	Climate(vector<StrategyClimate*> climateStrategies, int startValue = 0, int rangeValues = 364);

	void setClimateStrategy(StrategyClimate* climateStrategy);

	void setClimateStrategies(vector<StrategyClimate*> climateStrategies);

	float getValue(ClimateVariable climVar, int timestep, int iTime, Cell* cell, bool useRange = false);

	size_t getNTimesteps();
};

#endif
