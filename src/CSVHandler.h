/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CSVHANDLER_H_
#define CSVHANDLER_H_

#include <unordered_set>
#include <unordered_map>
#include <vector>
#include "CSVReader.h"
#include "Point.h"
#include "LifeCycle.h"
#include "Influences.h"
#include "ConstraintCycle.h"
#include "Disturbance.h"
#include "MeasureHandler.h"
#include "tools.h"

using namespace std;

class CSVHandler {
protected:
	vector<string> header;
	vector<vector<string>> data;
	bool empty = true;
	int nRows = 0;
	bool valid = true;
	string message = "INVALID";

public:
	CSVHandler(string filename, bool hasHeader = true, char delm = ',');
	vector<vector<string>> getData();
	vector<string> getHeader();
	bool isEmpty() const;
	bool isValid() const;
	string getMessage() const;
};

class CSVHandlerDefault : public CSVHandler {
private:
	unordered_map<string, vector<string>> valuesByHeader;
protected:
	bool missingHeaders = false;
	vector<string> validHeaderNames;
	vector<string> headerNames;
public:
	CSVHandlerDefault(string filename, vector<string> headerNames);
	vector<string> getValues(string headerName);
};

class CSVHandlerLifeCycle : public CSVHandlerDefault {
private:
	vector<LifeStage*> lifeStages;
	unordered_set<string> uniqueStageNames;
	unordered_map<int, Flow*> transFlows;
	unordered_map<int, Flow*> reprFlows;
	unordered_map<int, double> ratesMigr;
	unordered_map<int, int> radsMigr;
	unordered_map<int, double> migrDecays;
	unordered_map<int, double> migrSights;
	unordered_map<int, double> migrPrefs;
	vector<Flow*> mortFlows;
	vector<Flow*> baseImmFlows;

	unsigned int iHeader;

	bool handleStages();
	bool handleFlows();
public:
	CSVHandlerLifeCycle(string filename);
	vector<LifeStage*> getLifeStages();
	unordered_set<string> getStageNames();
	Flow* getTransFlow(int iStage);
	Flow* getReprFlow(int iStage);
	Flow* getMortFlow(int iStage);
	double getMigrRate(unsigned int iStage);
	int getMigrRad(unsigned int iStage);
	double getMigrDecay(unsigned int iStage);
	double getMigrSight(unsigned int iStage);
	double getMigrPref(unsigned int iStage);
	vector<Flow*> getBaseImmFlows();

};

class CSVHandlerCoordinates : public CSVHandlerDefault {
private:
	unordered_set<Point*> coordinates;
public:
	CSVHandlerCoordinates(string filename, vector<string> headerNames = {"ilon", "ilat"});
	unordered_set<Point*> getCoordinates();

};

class CSVHandlerGrassland : public CSVHandlerDefault {
private:
	unordered_set<Cell*> grasslandCells;
	unordered_set<Point> climCoords;
public:
	CSVHandlerGrassland(string filename, bool climInterpolate = false, vector<string> headerNames = {"xCoord","yCoord","coverRatio","iLonClim","iLatClim"});
	unordered_set<Cell*> getGrasslandCells();
	unordered_set<Point> getClimCoords();

};

class CSVHandlerInfluences : public CSVHandlerDefault {
private:
	unordered_map<FlowType,vector<std::pair<string,Influence*>>> flowInfluencesByType;
	vector<std::pair<string,Influence*>> flowInfluences;
	vector<std::pair<string,Influence*>> devInfluences;
	unordered_set<ClimateVariable> appliedClimParams;

	bool handleInfluences();
public:
	CSVHandlerInfluences(string filename);
	unordered_map<FlowType,vector<std::pair<string,Influence*>>> getFlowInfluencesByType();
	vector<std::pair<string,Influence*>> getDevInfluences();
	unordered_set<ClimateVariable> getAppliedClimParams();
};

class CSVHandlerDisturbance : public CSVHandlerDefault {
private:
	unordered_set<string> disturbanceNames;
	unordered_map<string, unordered_map<Point, unordered_map<int, vector<int>>>> coordTimingByDisturbance;
	unordered_map<string, unordered_map<Point, unordered_map<int, vector<int>>>> climCoordTimingByDisturbance;
	void handleTiming();
public:
	CSVHandlerDisturbance(string filename);
	unordered_set<string> getDisturbanceNames();
	unordered_map<int, vector<int>> getTimingByYear(Point* coord, Point* climCoord, string landUseScenarioID);

};

class CSVHandlerDisturbanceScheme : public CSVHandlerDefault {
private:
	unordered_set<string> disturbanceNames;
	unordered_map<Point, pair<string, bool>> schemeByCoord;
	unordered_map<string, double> probsByDistName;
public:
	CSVHandlerDisturbanceScheme(string filename, unordered_set<string> validDisturbanceNames);
	string getDisturbanceNameAt(Point coord, mt19937* randGen = NULL);
	bool isFixedAt(Point coord);

};

class CSVHandlerDensities : public CSVHandlerDefault {
private:
	unordered_map<Point, unordered_map<string, double>> densitiesByCoord;
	unordered_set<Point> coords;
public:
	CSVHandlerDensities(string filename, unordered_set<string> validStageNames);
	unordered_map<string, double> getDensitiesAt(Point coord);
	unordered_set<Point> getCoords();

};

#endif /* CSVHANDLER_H_ */
