/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "LifeCycle.h"

long Cohort::NEXT_ID = 1;

Cohort::Cohort(LifeStage* parentStage, int creationTime, double density) {
	this->id = Cohort::NEXT_ID++;
	this->parentStage = parentStage;
	this->creationTime = creationTime;
	this->density = density;
	this->flowAmount = 0;
    this->devState = 0;
    this->developedRatio = 0.0;
    this->maturation = 0.0;
}

long Cohort::getId() {
	return id;
}

int Cohort::getAge(Environment* environment) {
	return environment->getTimestep() - creationTime;
}

//	overaged is defined as being above maximum age
bool Cohort::isOveraged(Environment* environment) {
	return getAge(environment) > parentStage->getMaxAge();
}

int Cohort::getCycle(Environment* environment) {
	return environment->getCycle(creationTime);
}

double Cohort::getDensity() {
	return density;
}

//	empty is defined as being below minimum density
bool Cohort::isEmpty() {
	return density < parentStage->getMinDensity();
}

bool Cohort::isDeveloped(double delta_max) {
	return (devState + delta_max) >= 1;
}

void Cohort::addDevState(double devState) {
	this->devStates.push_back(devState);
}

double Cohort::getDevelopedRatio() {
	return developedRatio;
}

void Cohort::setDevelopedRatio(double developedRatio) {
	this->developedRatio = developedRatio;
}

double Cohort::getMaturation() {
	return maturation;
}

void Cohort::setMaturation(double maturation) {
	this->maturation = maturation;
}

LifeStage* Cohort::getParentStage() {
	return parentStage;
}

void Cohort::updateDevState(Environment* environment, vector<Influence*> devInfluences) {
	devState = 1.0;
	if(devInfluences.size() == 0) {
		return;
	}
	devState = 0.0;
	unsigned int iDevState = 0;
//	TODO: distinguish between multiplicative and additive factors
	for(std::vector<Influence*>::iterator it_influences = devInfluences.begin(); it_influences != devInfluences.end(); ++it_influences) {
		if(devStates.size() <= iDevState) {
			devStates.push_back(0.0);
		}
		if(devStates[iDevState] < 1.0 || !(*it_influences)->isDevRemain()) {
			devStates[iDevState] += (*it_influences)->factor(environment, this);
		}
		// keep development state between 0 and 1
		devStates[iDevState] = devStates[iDevState] < 0 ? 0 : (devStates[iDevState] > 1 ? 1 : devStates[iDevState]);
		devState += devStates[iDevState];
		iDevState++;
	}
	devState /= iDevState;
}

void Cohort::update(Environment* environment, vector<Influence*> devInfluences) {
	bool isGainingCohort = (parentStage->getGainingCohortID() == id);
	double gain = 0;
	double loss = 0;

	updateDevState(environment, devInfluences);

	// get the total gain of the parent stage in case this cohort is the gaining one
	if(isGainingCohort) {
		gain = parentStage->getGain();
	}

	// loop through all parent's stage output flows to accumulate cohort's loss
	for(auto flow : parentStage->getOutput()) {
		// reproductive flows do not not cause loss in reproducing cohort
		if(flow->getType() == FlowType::REPRODUCTION) {
			continue;
		}
		// get the flow's amount corresponding to the cohort's ID
		loss += flow->getAmount(id);
	}

	density += gain - loss;
}
