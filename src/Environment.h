/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EnvironmentH
#define EnvironmentH

#include "Climate.h"
#include "Point.h"
#include "Cell.h"
#include <random>
#include <vector>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <fstream>
#include "Disturbance.h"

// forward declaration to prevent circular inclusion
class Population;
class CSVHandlerLifeCycle;
class CSVHandlerInfluences;

using namespace std;

class Environment
{
private:
	Climate* climate;
	unordered_map<Point, Cell*> cellsByCoord;
	unordered_map<Point, Cell*> activeCellsByCoord;
	unordered_set<Point> grasslandCoords;
	unsigned int minX = std::numeric_limits<int>::max();
	unsigned int minY = std::numeric_limits<int>::max();
	unsigned int minLon = std::numeric_limits<int>::max();
	unsigned int minLat = std::numeric_limits<int>::max();
	unsigned int maxX = 0;
	unsigned int maxY = 0;
	unsigned int maxLon = 0;
	unsigned int maxLat = 0;
	bool uniformDisturbances = false;
	unsigned int uniformDisturbanceID;
	unordered_set<Point> fixedDisturbanceCoords;
	unsigned int habitatWidth, climateCellWidth; // in m
	int timestep, indexTime;
	int initialTimestep, deltaTimesteps;
	int cycleLength;
	long int runID;
	string fullpathRun;
	mt19937 randGenGlobal;
	int seedGlobal;
	unordered_map<Point, mt19937> localRandGens;
	unordered_map<Point, int> localSeeds;

	map<int, int> orderOfYears;
	unordered_map<int, unordered_map<Point, Population*>> neighborhoods;
	unordered_map<int, unordered_map<Point, Population*>> activeNeighborhoods;
	Point soloPop;
	CSVHandlerLifeCycle* lifeCycleDefinition;
	CSVHandlerInfluences* influenceDefinitions;

public:
	Environment(vector<StrategyClimate*> climateStrategies, unordered_set<Cell*> grasslandCells, int timestep = 0, int deltaTimesteps = 1, int habitatWidth = -1, int climateCellWidth = -1, int cycleLength = -1, int seed = 123);

	bool update();

	bool updateIndexTime(bool force = false);

	void initClimate(vector<StrategyClimate*> climateStrategies);

	void initGrasslandCells(unordered_set<Cell*> grasslandCells);

	unordered_set<Point> getGrasslandCoords();

	int getTimestep();

	void setTimestep(int timestep);

	int getInitialTimestep();

	int getDeltaTimesteps();

	void setInitialTimestep(int initialTimestep);

	int getCycle(int reference=-1);

	int getInitialCycle();

	int getFinalCycle();

	int getCycleLength();

	int getWeek(int reference = -1);

	int getWeekOfCylce();

	int getDayOfCycle();

	int getDayOfVegetation(Point coord);

	Climate* getClimate();

	Measure* getMeasure(Point coord);

	bool hasDisturbanceScheduled(Point coord);

	float getValue(ClimateVariable climVar, Point coord);

	bool addDisturbance(Point coord, unsigned int disturbanceID, unordered_map<int, vector<int>> disturbancesTimingByYear = unordered_map<int, vector<int>>(), bool dynamicDisturbanceTiming = false);

	void addActiveNeighbor(int key, Point activeNeighbor);

	void removeActiveNeighbor(int key, Point activeNeighbor);

	void clearActiveNeighbors(int key);

	void addActiveNeighbors(int key, unordered_set<Point> activeNeighbors);

	unordered_map<Point, Population*> getActiveNeighborhood(int key);

	void addNeighborhood(int key, unordered_map<Point, Population*> neighborhood);

	unordered_map<Point, Population*> getNeighborhood(int key);

	void setDisturbanceAt(Point coord, unsigned int disturbanceID);

	unsigned int getDisturbanceAt(Point coord);

	void clearDisturbances();

	mt19937* getRandGenLocal(Point coord);

	mt19937* getRandGenGlobal();

	void setSeed(int seed, unordered_set<Point> coords = {});

	unsigned int getHabitatWidth() const;

	unsigned int getHabitatSize() const;

	unsigned int getClimateCellWidth() const;

	unsigned int getClimateCellSize() const;

	void setOrderOfYears(const map<int, int>& orderOfYears);

	int getIndexTime() const {
		return indexTime;
	}

	long int getRunID() const {
		return runID;
	}

	string getFullpathRun() const {
		return fullpathRun;
	}

	void setFullpathRun(string fullpathRun) {
		this->fullpathRun = fullpathRun;
	}

	int getSeedLocal(Point coord = Point()) const;
	CSVHandlerLifeCycle* getLifeCycleDefinition() const;
	CSVHandlerInfluences* getInfluenceDefinitions() const;
	void setLifeCycleDefinition(CSVHandlerLifeCycle *&lifeCycleDefinition);
	void setInfluenceDefinitions(CSVHandlerInfluences *&influenceDefinitions);
	unsigned int getMaxLat() const;
	unsigned int getMaxLon() const;
	unsigned int getMaxX() const;
	unsigned int getMaxY() const;
	unsigned int getMinLat() const;
	unsigned int getMinLon() const;
	unsigned int getMinX() const;
	unsigned int getMinY() const;
	Point getSoloPop() const;
	void setSoloPop(Point soloPop);
	bool isSoloPop();
};

#endif
