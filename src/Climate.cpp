/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Climate.h"

Climate::Climate(vector<StrategyClimate*> climateStrategies, int startValue, int rangeValues) {
	this->nTimesteps = 1;

	setClimateStrategies(climateStrategies);

	this->startValue = startValue;
	this->rangeValues = rangeValues;
}

float Climate::getValue(ClimateVariable climVar, int timestep, int iTime, Cell* cell, bool useRange) {
	if(currCoord != cell->getCoord() || timestep != lastTimestep) {
		currCoord = cell->getCoord();
		lastTimestep = timestep;
		if(useRange) {
			iTime = startValue + (iTime % rangeValues);
		}
		iTime %= nTimesteps;
		for(auto currClimVar : climateVars) {
			float value = 0.0;
			auto sPair = climateStrategiesByVar.find(currClimVar);
			if(sPair == climateStrategiesByVar.end()) {
				value = nanf(to_string(currClimVar).c_str());
			}
			StrategyClimate* climateStrategy = sPair->second;
			value = climateStrategy->getValue(timestep, iTime, cell);
			currValsByVar[currClimVar] = value;
		}
	}
	return currValsByVar[climVar];
}

void Climate::setClimateStrategy(StrategyClimate* climateStrategy) {
	ClimateVariable climVar = climateStrategy->getClimateVariable();
	climateVars.insert(climVar);
	climateStrategiesByVar[climVar] = climateStrategy;
	if(climateStrategy->getTime() > nTimesteps) {
		nTimesteps = climateStrategy->getTime();
	}
}

void Climate::setClimateStrategies(vector<StrategyClimate*> climateStrategies) {
	for(auto climateStrategy : climateStrategies) {
		setClimateStrategy(climateStrategy);
	}
}

size_t Climate::getNTimesteps() {
	return nTimesteps;
}
