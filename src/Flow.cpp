/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "LifeCycle.h"

long Flow::NEXT_ID = 1;

Flow::Flow(LifeStage* from, LifeStage* to, double baseRate, bool discrete)
	: Flow(from, to == NULL ? FlowType::MORTALITY : FlowType::TRANSFER, to, baseRate, discrete)
{
}

Flow::Flow(LifeStage* from, FlowType type, LifeStage* to, double baseRate, bool discrete)
{
	this->id = Flow::NEXT_ID++;
	this->type = type;
	// if the flow is defined as transfer but there's no to-LifeStage it becomes a mortality flow instead
	if(to == NULL && type == FlowType::TRANSFER) {
		this->type = FlowType::MORTALITY;
	}
	this->from = from;
	this->to = to;
	setBaseRate(baseRate);
	this->discrete = discrete;

	amount = 0;
	rate = 0;

	stringstream tempName;
	tempName << from->getName() << "_";
	if(to) {
		tempName << to->getName();
	}else{
		switch (type) {
			case TRANSFER:
				tempName << "Trans";
				break;
			case MORTALITY:
				tempName << "Mort";
				break;
			case MIGRATION:
				tempName << "Migr";
				break;
			case REPRODUCTION:
				tempName << "Repr";
				break;
			default:
				break;
		}
	}
}

void Flow::reset() {
	amount = 0.0;
	rate = 0.0;
	amountByCohort.clear();
	rateByCohort.clear();
}

long Flow::getId() const {
	return id;
}

double Flow::getBaseRate() {
	return baseRate;
}

double Flow::getRate(long cohortID) {
	// if the cohortID equals -1, return the total rate
	if(cohortID == -1) {
		return rate;
	}
	// find the rate mapped by the cohortID
	auto cohortRate = rateByCohort.find(cohortID);
	// if the cohortID exists in the map, return the mapped rate
	if (cohortRate != rateByCohort.end()) {
		return cohortRate->second;
    }
	// otherwise return ZERO
	return 0;
}

double Flow::getAmount(long cohortID) {
	// if the cohortID equals -1, return the total amount
	if(cohortID == -1) {
		return amount;
	}
	// find the amount mapped by the cohortID
	auto cohortAmount = amountByCohort.find(cohortID);
	// if the cohortID exists in the map, return the mapped amount
	if (cohortAmount != amountByCohort.end()) {
		return cohortAmount->second;
    }
	// otherwise return ZERO
	return 0;
}

void Flow::setBaseRate(double baseRate) {
	baseRate = baseRate < 0 ? 0 : baseRate;
	this->baseRate = baseRate;
}

LifeStage* Flow::getFrom() {
	return from;
}

LifeStage* Flow::getTo() {
	return to;
}

vector<Influence*> Flow::getInfluences() {
	return allInfluences;
}

void Flow::addAll(vector<Influence*> influences) {
	for(auto influence : influences) {
		addInfluence(influence);
	}
}

void Flow::addInfluence(Influence* influence) {
	if(!influence) {
		return;
	}
	allInfluences.push_back(influence);
	if(influence->isMultiplicative()) {
		multiplicativeInfluences.push_back(influence);
	} else {
		additiveInfluences.push_back(influence);
	}
}

double Flow::calculateRate(Environment* environment, Cohort* cohort) {
	// if this is the mortality flow and the cohort has exceeded the maximum, set mortality rate to 1.0
	if(type == MORTALITY && cohort->isOveraged(environment)) {
		return 1.0;
	}
	double calcRate = baseRate;
	for(auto influence : multiplicativeInfluences) {
		calcRate *= influence->factor(environment, cohort);
	}
	for(auto influence : additiveInfluences) {
		calcRate += influence->factor(environment, cohort) * (1 - calcRate);
	}
	return calcRate;
}

bool Flow::update(Environment* environment) {
	reset();
	//get list of Cohorts of from-LifeStage
	vector<Cohort*> cohorts = from->getCohorts();
	//if life stage currently does not have cohorts, leave update method
	if(cohorts.size() == 0) {
		return false;
	}

//	workaround to skip base immigration outside of designated time window
	if(additiveInfluences.size() == 0) {
		for(auto infl : multiplicativeInfluences) {
//			TODO check whether multiplicative Influences do return values other than zero if isDue() == false
//			for now InfluenceTimeWindow is the only Influence to return isDue() == false while having a value of zero
			if(!infl->isDue(environment)) {
				return false;
			}
		}
	}

	// loop through all existing cohorts of FROM stage to calculate their corresponding flow amount
	for(auto cohort : cohorts) {
		// skip the cohort, if the it is not developed yet AND Flow type is TRANSFER or REPRODUCTION
		if(!cohort->isDeveloped()
			&& (type == FlowType::TRANSFER || type == FlowType::REPRODUCTION))
		{
			continue;
		}
		long cohortID = cohort->getId();
		// calculate the flow rate
		double cohortRate = calculateRate(environment, cohort);
		// take into account the rates of the other flows (except REPRODUCTIVE)
		double summedOtherRates = 0;
		// loop through other flows of FROM stage
		for(auto flow : from->getOutput()) {
			if(from->isDummy()) {
				break;
			}
			// ignore the rate if...
			if(flow->getId() == this->id // ... flows are identical
					|| flow->getType() == FlowType::REPRODUCTION // ... other flow is reproductive
//					|| (flow->getType() == FlowType::MIGRATION && type == FlowType::MIGRATION) // ... both are migration flows
			){
				continue;
			}
			// sum up the rates for this cohort ID
			summedOtherRates += flow->getRate(cohortID);
		}
		// calculate the actual rate by multiplying with the difference to the other flow rates
		double diffRate = cohortRate * summedOtherRates;
		double newRate = cohortRate - diffRate;
		cohortRate = newRate;
		// calculate the amount by multiplying flow rate and cohort density
		double cohortAmount = cohortRate * cohort->getDensity();
		// map the rate with the cohort id
		rateByCohort[cohortID] = cohortRate;
		// map the amount with the cohort id
		amountByCohort[cohortID] = cohortAmount;
		// add amount to the total amount
		amount += cohortAmount;
	} // end loop
	if(discrete && amount > 0.0) {
		double discrAmount = floor(amount);
		double ratioDiscr = discrAmount / amount;
		amount = 0.0;
		for(auto cohort : cohorts) {
			long cohortID = cohort->getId();
			double cohortRate = rateByCohort[cohortID] * ratioDiscr;
			double cohortAmount = cohortRate * cohort->getDensity();
			rateByCohort[cohortID] = cohortRate;
			amountByCohort[cohortID] = cohortAmount;
			amount += cohortAmount;
		}
	}
	if(type != MORTALITY && amount < to->getMinDensity()) {
		amount = 0.0;
		for(auto cohort : cohorts) {
			long cohortID = cohort->getId();
			rateByCohort[cohortID] = 0.0;
			amountByCohort[cohortID] = 0.0;
		}
	}
	// set a total rate of this flow if FROM's density is > ZERO
	rate = from->getDensity() > 0 ? amount / from->getDensity() : 0;
	if(to && amount > 0.0) {
		to->setGaining(true);
		return true;
	}
	return false;
}

FlowType Flow::getType(){
	return type;
}

bool Flow::operator< (const Flow &ob) const
{
	return id < ob.id;
}

bool Flow::operator== (const Flow &ob) const {
	return id == ob.id;
}

bool Flow::operator!= (const Flow &ob) const {
	return id != ob.id;
}
