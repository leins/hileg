/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_

//#include "LandUse.h"
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <string>
#include "Point.h"

enum GlobalModel {
	CCCma, CNRM, ICHEC, MIROC, MOHC, MPI
};

enum ReprConcPath{
	RCP26, RCP45, RCP85
};

enum Vitality{
	DEAD, REVIVED, ALIVE, DIED
};

enum ClimateVariable {
	CMT, // change in moisture tendency
	CW, // contact water ground
	HURS,
	HUSS,
	MRSO,
	PAR,
	PR,
	RHUG, // relative humidity ground
	RSMC, // relative soil moisture content
	SFCWIND,
	SMD,
	SMT,
	SUND,
	TAS,
	TASMAX,
	TASMIN,
	TS,
	VAPS,
	VTS, // vegetation dependent temperature soil
	WDIRGEO_10
};

enum DensityLevel {
	COHORT,
	LIFE_STAGE,
	BELOW_GROUND,
	ABOVE_GROUND,
	POPULATION
};

enum FlowType {
	TRANSFER,
	MIGRATION,
	MORTALITY,
	REPRODUCTION
};

enum DispersalType {
	IMMOBILE,
	BASE_IMMIGRATION,
	DISPERSAL,
	BOTH
};

enum Direction {
	NO,
	NE,
	EA,
	SE,
	SO,
	SW,
	WE,
	NW
};

namespace Constants
{
    // forward declarations only
	extern const char* FOLDERNAME_OUTPUT;
	extern const std::string FILENAME_STAGES;
	extern const std::string FILENAME_INFLUENCES;
	extern const std::string FILENAME_DISTURBANCES;
	extern const std::string FILENAME_DISTURBANCE_SCHEME;
	extern const std::string FILENAME_APPLIED_DISTURBANCE_SCHEME;
	extern const std::string FILENAME_GRASSLAND_COORDS;
	extern const std::string FILENAME_INITIAL_DENSITIES;
	extern const float THRESHOLD_RMUG;
	extern const float THRESHOLD_CW;
	extern const float TEMPERATURE_SUM_VEGETATION_START;
    extern const float ZERO_KELVIN;
    extern const unsigned int GRASSLAND_CELL_WIDTH;
    extern const unsigned int CLIMATE_CELL_WIDTH;
    extern const float GRASSLAND_PER_CLIMATE_CELL_1D;
    extern const float STRAIN_FACTOR_GRASSLAND_X;
    extern const float STRAIN_FACTOR_GRASSLAND_Y;
    extern const int OFFSET_GRASSLAND_X;
    extern const int OFFSET_GRASSLAND_Y;
    extern const int SECOND_TO_DAY;
    extern const double EPSILON;
    extern const int DAYS_1949_12_01_TO_1970_01_01_GREGORIAN;
    extern const int DAYS_1949_12_01_TO_1970_01_01_365_DAYS;
    extern const int DAYS_1949_12_01_TO_1970_01_01_360_DAYS;
    extern const int BASE_YEAR;
    extern const int DAYS_PER_YEAR;
    extern const std::string NAME_DISTURBANCE_NONE;
    extern const unsigned int ID_DISTURBANCE_NONE;
    extern const std::unordered_map<GlobalModel,std::string> GLOBAL_CLIMATE_MODEL_NAMES_BY_ENUM;
    extern const std::unordered_map<ReprConcPath,std::string> RCP_NAMES_BY_ENUM;
    extern const std::string CLIMATE_MODEL_REGION;
    extern const std::unordered_map<GlobalModel,std::string> GLOBAL_CLIMATE_REALISATION_BY_MODEL;
    extern const std::string REGIONAL_CLIMATE_MODEL;
    extern const std::string CLIMATE_TIME_STEP;
    extern const std::string CLIMATE_TIME_PERIOD;
    extern const std::unordered_map<std::string,ClimateVariable> CLIMATE_VARIABLE_NAMES;
    extern const std::unordered_map<ClimateVariable, std::string> CLIMATE_VARIABLE_NAMES_BY_ENUM;
    extern const std::unordered_map<std::string,DensityLevel> DENSITY_LEVEL_NAMES;
    extern const std::unordered_map<DensityLevel, std::string> DENSITY_LEVEL_NAMES_BY_ENUM;
    extern const std::vector<std::string> VAR_NAMES_NETCDF;
    extern const std::vector<std::string> CALENDAR_NAMES;
	extern const std::vector<std::string> VALID_INFLUENCE_PROCESS_NAMES;
	extern const std::vector<std::string> VALID_INFLUENCE_PROCESS_NAMES_VARIABLE;
	extern const std::vector<std::string> VALID_INFLUENCE_TYPES; //Influence
	extern const std::vector<std::string> VALID_INFLUENCE_TARGETS;
	extern const std::unordered_map<std::string,FlowType> MAPPED_VALID_FLOW_TYPE_NAMES;
	extern const std::unordered_set<Point> VALID_COORDINATES_NW_GERMANY;
}

#endif /* CONSTANTS_H_ */
