/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Cell.h"

Cell::Cell(const Point& coord, const Point& climCoord, double coverRatio, unsigned int GLZ, unsigned int climCellID, Measure* measure) {
	this->coord = coord;
	this->climCoord = climCoord;
	this->coverRatio = coverRatio;
	this->GLZ = GLZ;
	this->climCellID = climCellID;

	interpolRatioByClimCoord[climCoord] = 1.0;

	if(!measure) {
		vector<int> timing;
	//	empty timing vector => no disturbance
		measure =  new Measure(Constants::ID_DISTURBANCE_NONE, timing);
	}
	addMeasure(measure);
}

Point Cell::getClimCoord() {
	return climCoord;
}

Point Cell::getCoord() {
	return coord;
}

Measure* Cell::getMeasure(unsigned int measureID) {
	if(measureID == 0 || measureByID.find(measureID) == measureByID.end()) {
		return measureByID[activeMeasureID];
	}
	return measureByID[measureID];
}

void Cell::addMeasure(Measure *measure) {
	if(measure) {
		activeMeasureID = measure->getID();
		measureByID[activeMeasureID] = measure;
	}
}

void Cell::clearMeasures() {
	for(auto pairIDMeasure : measureByID) {
		delete pairIDMeasure.second;
	}
	measureByID.clear();
}

void Cell::setActiveMeasure(unsigned int measureID) {
	if(measureByID.find(measureID) != measureByID.end()) {
		this->activeMeasureID = measureID;
	}
}

void Cell::setInterpolRatio(Point climCoordInterpol, float ratio) {
	interpolRatioByClimCoord[climCoordInterpol] = ratio;
}


double Cell::getCoverRatio() {
	return coverRatio;
}

int Cell::getLastDisturbanceTimestep(unsigned int measureID) {
	return getMeasure(measureID)->getLastUse();
}

bool Cell::operator< (const Cell &ob) const
{
	return coord < ob.coord;
}

bool Cell::operator== (const Cell &ob) const {
	return coord == ob.coord;
}

bool Cell::operator!= (const Cell &ob) const {
	return coord != ob.coord;
}

unsigned int Cell::getGLZ() const {
	return GLZ;
}

unsigned int Cell::getClimCellID() const {
	return climCellID;
}

const unordered_map<Point, float>& Cell::getInterpolRatioByClimCoord() const {
	return interpolRatioByClimCoord;
}

int Cell::getDayOfVegetation() {
	return vegetationDay;
}

unsigned int Cell::getLastUpdateTimestep() {
	return lastUpdate;
}

float Cell::getTemperatureSum() {
	return temperatureSum;
}

void Cell::reset(unsigned int timestep, int vegetationDay, float temperatureSum) {
	this->lastUpdate = timestep;
	this->vegetationDay = vegetationDay < -1 ? -1 : vegetationDay;
	this->temperatureSum = temperatureSum < 0.0 ? 0.0 : temperatureSum;
}

