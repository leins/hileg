/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef StrategyClimateH
#define StrategyClimateH

#include <netcdf.h>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include "Constants.h"
#include "Cell.h"
#include <string.h>
#include <iostream>
#include "../lib/date.h"
#include <random>

using namespace std::chrono;
using namespace date;
using namespace std;

class StrategyClimate
{
protected:
	int xDim, yDim;
	int validationCode = 0;
	size_t nTime;
	int actualIndexTime(int iTime);
	ClimateVariable climVar;
	unordered_map<Point, float> maxValues;
public:
	virtual ~StrategyClimate() {};
	StrategyClimate(ClimateVariable climVar, int xDim = 1, int yDim = 1, size_t nTime = 1);
	virtual float getValue(unsigned int timestep, unsigned int iTime, Cell* cell) = 0;
	virtual float getMaxValue(Cell* cell) = 0;
	virtual float getRelativeValue(unsigned int timestep, unsigned int iTime, Cell* cell);
	size_t getTime() const;
	ClimateVariable getClimateVariable() const;
	int getValidationCode() const;
};

class StrategyClimateNetCDF : public StrategyClimate
{
private:
	string filename;
	mt19937 randGen;
	bool calendarType[3] = {0, 0, 0};
	int ncid, timeID, parameterID;
	bool loadAll;
	const char* labelTime = "time";
	const char* labelRlat = "rlat";
	const char* labelRlon = "rlon";
	int nDimensions = 1; //number of dimensions of the netcdf file
    unsigned int initTimestep = 0; //initial timestep
    unsigned int nTimesteps = 0; //number of timesteps resp. time dimension: 0=till end
    unsigned int nBefore = 0; //number of timesteps to additionally load before initTimestep
	unordered_set<Point> climCoords; //list of applied climate data coordinates
	size_t iLon = 0; // current index for longitude
	size_t iLat = 0; // current index for latidude
    size_t nLat = 1; //number of grid cells in latitude direction
    size_t nLon = 1; //number of grid cells in longitude direction
	size_t dim3[3] = {0, iLon, iLat};
	size_t dim1[1] = {1};

	size_t lengthDimTime;

	float time_value;

	int idxsDupl[4] = {0,0,0,0};

	unordered_map<Point, unordered_map<unsigned int,float>> valuesByCoord;
	unordered_map<Point, float> meanMaxValues;
	vector<int> timestepToIndex; //maps the model time step to the climate data time index

    bool skipValue();
    bool isEndOfYear();
    bool duplicateValue();
    void prepareData();
    void loadValues(int iTime);
    void loadValuesAt(Point coord, bool prepare = false);
	const char* determineVarIndex();

public:
    StrategyClimateNetCDF(string filename, mt19937 randGen, unordered_set<Point> climCoords, ClimateVariable climVar = TS, unsigned int initTimestep = 0, unsigned int nTimesteps = 0, unsigned int nBefore = 0, bool loadAll = false); // default constructor
	float getValue(unsigned int timestep, unsigned int iTime, Cell* cell);
	float getRelativeValue(unsigned int timestep, unsigned int iTime, Cell* cell) override;
	void resetTimeframe(int initTimestep, bool reloadValues = false, int nTimesteps = -1);
	float getMaxValue(Cell* cell);
	float getMeanMaxValue(Cell* cell) const;
};

class StrategyClimateVTS : public StrategyClimate
{
private:
	StrategyClimate* tasmaxStrategy;
	StrategyClimate* tsStrategy;
	StrategyClimate* tasminStrategy;

	unsigned int deltaDays; // days to go from tasmax to ts and afterwards from ts to tasmin
	float minTasmin; // tasmin threshold below which vts switches to ts

public:
	StrategyClimateVTS(StrategyClimate* tasmaxStrategy, StrategyClimate* tsStrategy, StrategyClimate* tasminStrategy, unsigned int deltaDays = 14, float minTasmin = 5.0);
	float getValue(unsigned int timestep, unsigned int iTime, Cell* cell);
	float getMaxValue(Cell* cell);
};

class StrategyClimateRSMC : public StrategyClimate
{
private:
	StrategyClimate* mrsoStrategy;
	int depthTotal, depthLayer; // in cm

public:
	StrategyClimateRSMC(StrategyClimate* mrsoStrategy, int depthTotal = 390, int depthLayer = 2);
	float getValue(unsigned int timestep, unsigned int iTime, Cell* cell);
	float getMaxValue(Cell* cell);
};

/**
 * calc the CMT time series by adding the sum of PR and SMT to an exponential moving average
 * of nWeights where weight(n) = 2 / (n + 1)
 */
class StrategyClimateCMT : public StrategyClimate
{
private:
	StrategyClimate* prStrategy;
	StrategyClimate* smtStrategy;
	vector<float> weights;
	vector<float> summedWeights;

public:
	StrategyClimateCMT(StrategyClimate* prStrategy, StrategyClimate* smtStrategy, int nWeights);
	float getValue(unsigned int timestep, unsigned int iTime, Cell* cell);
	float getMaxValue(Cell* cell);
};

class StrategyClimateRHUG : public StrategyClimate
{
private:
	StrategyClimate* rsmcStrategy;
	float thdSated;
	bool limitAtThd;

public:
	StrategyClimateRHUG(StrategyClimate* rsmcStrategy, float thdSated, bool limitAtThd = true);
	float getValue(unsigned int timestep, unsigned int iTime, Cell* cell);
	float getMaxValue(Cell* cell);
};

class StrategyClimateCW : public StrategyClimate
{
private:
	StrategyClimate* cmtStrategy;
	StrategyClimate* rsmcStrategy;
	float thdCMT;
	float thdRSMC;

public:
	StrategyClimateCW(StrategyClimate* cmtStrategy, float thdCMT, StrategyClimate* rsmcStrategy, float thdRSMC);
	float getValue(unsigned int timestep, unsigned int iTime, Cell* cell);
	float getMaxValue(Cell* cell);
};

#endif
