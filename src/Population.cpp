﻿/**
 * Copyright (C) 2020 Johannes A. Leins, UFZ
 *
 * This file is part of model HiLEG - High-resolution Large Environmental Gradient.
 *
 * HiLEG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HiLEG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HiLEG. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Population.h"
#include "ConstraintCycle.h"

Population::Population(Environment* environment, int neighborhoodID, PopulationAtts* atts) { // default constructor
	this->neighborhoodID = neighborhoodID;
	this->density = 0.0;
	this->vitality = DEAD;
	this->atts = atts;
	nInit = -1; // signalling that init will be called from constructor
	init(environment, atts);
}

int Population::getNeighborhoodID() {
	return neighborhoodID;
}

double Population::getDensity(bool useMean) {
	if(useMean) {
		return meanStats["finalDensity"] / nInit;
	}
	return density;
}

bool Population::isAlive() {
	return density > 0.0;
}

double Population::getMigration() const {
	return getImmigration() - getEmigration();
}

double Population::getEmigration() const {
	double emSum = 0;
	double emComp = 0; // compensation value
	for(auto stage : stages) {
//		using Kahan summation algorithm for compensation of numerical error inside stages' getEmigration
		pair<double, double> em = stage->getEmigration(emComp);
		emSum = em.first;
		emComp = em.second;
	}
	return emSum;
}

double Population::getImmigration() const {
	double imSum = 0;
	double imComp = 0; // compensation value
	for(auto stage : stages) {
//		using Kahan summation algorithm for compensation of numerical error inside stages' getImmigration
		pair<double, double> im = stage->getImmigration(imComp);
		imSum = im.first;
		imComp = im.second;
	}
	return imSum;
}

void Population::addStage(LifeStage* templStage, double initDens){
	LifeStage* stage = new LifeStage((*templStage));
	if(!(initDens < 0)) {
		stage->setInitDensity(initDens);
	}
	if(stages.size() > 0) {
		LifeStage* prevStage = stages.back();
		stage->setPrevStage(prevStage);
		stages.front()->setPrevStage(stage);
	}
	stages.push_back(stage);
}

void Population::addFlow(Flow* templFlow, bool cloneFrom, bool cloneTo) {
	Flow* flow;
	FlowType type = templFlow->getType();
	if(!cloneFrom && ! cloneTo) {
		flow = templFlow;
	} else {
		LifeStage* templFrom = templFlow->getFrom();
		LifeStage* templTo = templFlow->getTo();
		vector<Influence*> Influences = templFlow->getInfluences();
		double baseRate = templFlow->getBaseRate();

		LifeStage* from = templFrom;
		LifeStage* to = templTo;
		for(auto stage : stages) {
			if(cloneFrom && stage->getName() == templFrom->getName()) {
				from = stage;
			}
			if(cloneTo && templTo && stage->getName() == templTo->getName()) {
				to = stage;
			}
		}
		flow = new Flow(from, type, to, baseRate);
		flow->addAll(Influences);
	}
//	TODO At the moment !cloneFrom AND cloneTo on MIGRATION is equal to base immigration flows. Might change, however
	if(type == FlowType::MIGRATION && !cloneFrom && cloneTo) {
		immigrationFlows.push_back(flow);
	}else{
		flowsByType[type].push_back(flow);
	}

	flows.push_back(flow);
	flow->getFrom()->addOutput(flow);
	if(flow->getTo()) {
		flow->getTo()->addInput(flow);
	}
}


vector<LifeStage*> Population::getStages() {
	return stages;
}


unordered_set<Point> Population::updateFlows(Environment* environment) {
	for(auto flow : immigrationFlows) {
		flow->update(environment);
	}
	unordered_set<Point> activatedNeighbors;
	if(vitality == DEAD) {
		return activatedNeighbors;
	}
	for(auto flow : flowsByType[REPRODUCTION]) {
		flow->update(environment);
	}

	vector<Flow*> flowsMigr = flowsByType[MIGRATION];
	shuffle(flowsMigr.begin(), flowsMigr.end(), *(environment->getRandGenLocal(atts->getCoord())));
	for(auto flow : flowsMigr) {
		if(flow->update(environment)) {
			activatedNeighbors.insert(flow->getTo()->getAtts()->getCoord());
		}
	}

	for(auto flow : flowsByType[MORTALITY]) {
		flow->update(environment);
	}

	for(auto flow : flowsByType[TRANSFER]) {
		flow->update(environment);
	}

	return activatedNeighbors;
}

void Population::updateStages(Environment* environment) {
	vitality = density > 0.0 ? ALIVE : DEAD;

	if(vitality == DEAD && !atts->isGaining()) {
		return;
	}

	double densityAboveGround = 0.0;
	double densityBelowGround = 0.0;
	meanStats["finalDensity"] -= density;
//	flag defining if population was ALIVE or DEAD before update
	density = 0.0;
	for(auto stage : stages) {
		stage->update(environment);
		double stageDensity = stage->getDensity();
		density += stageDensity;
		if(stage->isAboveGround()) {
			densityAboveGround += stageDensity;
		} else {
			densityBelowGround += stageDensity;
		}
	}


	if(vitality == ALIVE && !(density > 0.0)) {
		vitality = DIED; //	flag that population died, if it was alive before
	} else if (vitality == DEAD && density > 0.0) {
		vitality = REVIVED; //	flag that population was revived, if it was dead before
	}

	meanStats["finalDensity"] += density;

	atts->setDensityAboveGround(densityAboveGround);
	atts->setDensityBelowGround(densityBelowGround);

	resetEmigration(environment);
	atts->setGaining(false); // reset flag for next iteration
}

void Population::updateStats(Environment* environment) {
	int timestep = environment->getTimestep();
	if(lastUpdate == timestep) {
		return;
	}
	lastUpdate = timestep;
	sumDensity += density;
	meanStats["meanDensity"] -= meanDensity;
	meanDensity = sumDensity / environment->getDeltaTimesteps();
	meanStats["meanDensity"] += meanDensity;
	sumMigration += getMigration();
	meanStats["meanMigration"] -= meanMigration;
	meanMigration = sumMigration / environment->getDeltaTimesteps();
	meanStats["meanMigration"] += meanMigration;
	if(!isAlive()) {
		if(!extinct) { // was not already extinct?
			extinct = true;
			for(auto stage : stages) { // set all stags quasi extinction to false, so a low future gain can be counted as quasi extinct again
				stage->setQuasiExtinct(false);
			}
			nExtinction++;
			meanStats["nExtinction"]++;
		}
		return;
	}
	if(firstVisit < 0) {
		firstVisit = timestep;
		meanStats["firstVisit"] += firstVisit;
		nVisit++;
	}
	if(firstEstablishment < 0) {
		bool established = true;
		for(auto stage : stages) {
			if(!stage->isEstablished()) {
				established = false;
				break;
			}
		}
		if(established) {
			firstEstablishment = timestep;
			meanStats["firstEst"] += firstEstablishment;
			nEst++;
		}
	}
	extinct = false;
	lifetime++;
	meanStats["lifetime"]++;
}

PopulationAtts* Population::getAtts() const {
	return atts;
}

Point Population::getCoord() {
	return atts->getCoord();
}

int Population::getILon() {
	return atts->getCoord().getX();
}

int Population::getILat() {
	return atts->getCoord().getY();
}

bool Population::addDevInfluence(string stageName ,Influence* devInfluence) {
	for(auto stage : stages) {
		if(stageName == stage->getName()) {
			stage->addDevInfluence(devInfluence);
			return true;
		}
	}
	return false;
}

bool Population::addFlowInfluence(FlowType type, string stageName ,Influence* flowInfluence) {
	vector<Flow*> flows = flowsByType[type];
	for(auto flow : flows) {
		if(stageName == flow->getFrom()->getName()) {
			flow->addInfluence(flowInfluence);
			return true;
		}
	}
	return false;
}

LifeStage* Population::getStage(string name) {
	for(auto stage : stages) {
		if(stage->getName() == name) {
			return stage;
		}
	}
	return NULL;
}

double Population::getLifetime(bool useMean) {
	if(useMean) {
		return meanStats["lifetime"] / nInit;
	}
	return lifetime;
}

double Population::getMeanDensity(bool useMean) {
	if(useMean) {
		return meanStats["meanDensity"] / nInit;
	}
	return meanDensity;
}

double Population::getMeanMigration(bool useMean) {
	if(useMean) {
		return meanStats["meanMigration"] / nInit;
	}
	return meanMigration;
}

double Population::getNExtinction(bool useMean) {
	if(useMean) {
		return meanStats["nExtinction"] / nInit;
	}
	return nExtinction;
}

int Population::getFirstEstablishment(bool useMean) {
	if(useMean) {
		if(nEst > 0) {
			return meanStats["firstEst"] / nEst;
		}
		return -1;
	}
	return firstEstablishment;
}

int Population::getFirstVisit(bool useMean) {
	if(useMean) {
		if(nVisit > 0) {
			return meanStats["firstVisit"] / nVisit;
		}
		return -1;
	}
	return firstVisit;
}

Vitality Population::getVitality() const {
	return vitality;
}

double Population::getNQuasiExtinction(bool useMean) {
	double nQuasiExtinction = 0;
	for(auto stage : stages) {
		nQuasiExtinction += stage->getNQuasiExtinct(useMean);
	}
	return nQuasiExtinction;
}

double Population::getCarryingCapacity() const {
	return atts->getCarryingCapacity();
}

void Population::init(Environment* environment, PopulationAtts* atts) {
//	if there's a change of atts (atts != NULL)
	if(atts) {
		this->atts = atts;
	}

	vitality = density > 0.0 ? ALIVE : DEAD;

	density = 0.0;
	lastUpdate = -1;
	firstVisit = -1;
	firstEstablishment = -1;
	meanDensity = 0.0;
	sumDensity = 0.0;
	lifetime = 0;
	nExtinction = 0;

	double densityAboveGround = 0.0;
	double densityBelowGround = 0.0;
//	reset life stages
	for(auto stage : stages) {
		stage->reset(environment, this->atts); //important NOT to use method argument "atts"
		double stageDensity = stage->getDensity();
		density += stageDensity;
		if(stage->isAboveGround()) {
			densityAboveGround += stageDensity;
		} else {
			densityBelowGround += stageDensity;
		}
	}


	if(vitality == ALIVE && !(density > 0.0)) {
		vitality = DIED; //	flag that population died, if it was alive before
	} else if (vitality == DEAD && density > 0.0) {
		vitality = REVIVED; //	flag that population was revived, if it was dead before
	}
	resetEmigration(environment);

	if(nInit < 0) { // was called by constructor
		nInit = 0;
		nEst = 0;
		nVisit = 0;
		meanStats["finalDensity"] = 0.0;
		meanStats["meanDensity"] = 0.0;
		meanStats["meanMigration"] = 0.0;
		meanStats["lifetime"] = 0.0;
		meanStats["firstVisit"] = 0.0;
		meanStats["firstEst"] = 0.0;
		meanStats["nExtinction"] = 0.0;
	} else {
		meanStats["finalDensity"] += getDensity();
		nInit++;
	}

//	reset flows
	for(auto flow : flows) {
	   flow->reset();
	}

	if(!isAlive()) {
		extinct = true;
	}
}

void Population::resetEmigration(Environment *environment) {
	if(emigrationFlows.size() > 0 || // IF emigration flows were created before
			(atts->getDispersalType() != DISPERSAL && atts->getDispersalType() != BOTH) // OR no dispersal is active
			|| vitality == DEAD || vitality == ALIVE // OR population already was DEAD/ALIVE nothing needs to be done
	)
	{
		return;
	}
//	Otherwise, migration to this population was activated for the first time and the emigration edges need to be constructed

//	get neighborhood (full grid) of this population
	unordered_map<Point, Population*> neighborhood = environment->getNeighborhood(neighborhoodID);
//	get migration parameters
	CSVHandlerLifeCycle* lifeCycleDefinition = environment->getLifeCycleDefinition();
	CSVHandlerInfluences* influenceDefinitions = environment->getInfluenceDefinitions();
	vector<std::pair<string,Influence*>> migrationInfluencePairs = influenceDefinitions->getFlowInfluencesByType()[FlowType::MIGRATION];
	vector<LifeStage*> lifeStages = lifeCycleDefinition->getLifeStages();
//	get the list of valid grassland coordinates
	unordered_set<Point> grasslandCoords = environment->getGrasslandCoords();

	unsigned int iStage = 0;
	for(auto stage : lifeStages) {
		string stageName = stage->getName();
		vector<Influence*> migrInfluences;
		for(auto nameInfluencePair : migrationInfluencePairs) {
			if(stageName == nameInfluencePair.first) {
				migrInfluences.push_back(nameInfluencePair.second);
			}
		}
		double baseMigrRate = lifeCycleDefinition->getMigrRate(iStage);
		double migrRad = lifeCycleDefinition->getMigrRad(iStage) / ((double) environment->getHabitatWidth());
		if((baseMigrRate > 0.0 && migrRad > 0)) {
			unordered_map<Direction, bool> hasNeighInDir;
			hasNeighInDir[NO] = false;
			hasNeighInDir[NE] = false;
			hasNeighInDir[EA] = false;
			hasNeighInDir[SE] = false;
			hasNeighInDir[SO] = false;
			hasNeighInDir[SW] = false;
			hasNeighInDir[WE] = false;
			hasNeighInDir[NW] = false;
			double rMigrDecay = lifeCycleDefinition->getMigrDecay(iStage); // decay rate used for distance dependant survival probality
			double rMigrSight = lifeCycleDefinition->getMigrSight(iStage); // rate/exponent used informed migration. Values << 1 increase probability to find grassland
			double rMigrPref = lifeCycleDefinition->getMigrPref(iStage); // exponent used for preference of nearby cells. Values 0 means that cells are preferred equally independent of their distance
			influenceDefinitions->getFlowInfluencesByType();
			// map to count number of cells at a certain distance
			unordered_map<int, pair<int, int>> nDist; // pair.first = #grasslandCells, pair.second = total number of cells in that distance
			// map to store the proportion of grassland by distance
			unordered_map<int, pair<double, double>> propGrass; // pair.first = proportion at distance, pair.second = proportion below distance
			unordered_set<Point> potentialNeighbors;
			Point popCoord = atts->getCoord();
			for (int distX = -migrRad; distX < migrRad; ++distX) {
				for (int distY = -migrRad; distY < migrRad; ++distY) {
					if(distX == 0 && distY == 0) {
						continue;
					}
					potentialNeighbors.emplace(Point(popCoord.getX() + distX, popCoord.getY() + distY));
					int distKey = distX * distX + distY * distY; //key for same distance cells is the sum of squares of distance in each direction
					if(nDist.find(distKey) == nDist.end()) {
						nDist[distKey] = pair<int,int>(0,0); // initialize the total number of cells at this distance
						propGrass[distKey] = pair<double,double>(0.0,0.0); // initialize the map for grassland proportion
					}
					nDist[distKey].second++; // increase total number of cells at the distance
				}
			}
			double sumPMigr = 0.0;
			LifeStage* localStage = getStage(stageName);
			double sumMigrDistance = 0;
			unordered_map<Point, double> neighMigrCoords;
			for(auto distPair : nDist) {
				nDist[distPair.first].first = 0; // reset the number of grassland cells at each distance
				propGrass[distPair.first] = pair<double,double>(0.0,0.0); // reset the map for grassland proportion
			}
			for(auto coord : potentialNeighbors) { // loop through pontential neighbors to find the actual neighbors
				if(grasslandCoords.find(coord) == grasslandCoords.end()) {
					continue; // skip if potential neighbor is not a grassland coord
				}
				int distX = popCoord.getX() - coord.getX();
				int distY = popCoord.getY() - coord.getY();
				int distKey = distX * distX + distY * distY; //key for same distance cells is the sum of squares of distance in each direction
				double distance = sqrt(((double) distKey));
				if(popCoord == coord || distance > migrRad) { //potential neighbor is actually source itself OR not within migration radius
					continue; // skip to next coord
				}
				sumMigrDistance += 1.0 / pow(distance, rMigrPref);
				neighMigrCoords[coord] = distance;
				nDist[distKey].first++;
				hasNeighInDir[NO] = hasNeighInDir[NO]
									|| (abs(distX) <= abs(distY)
									&& coord.getY() < popCoord.getY()); // y decreases from south to north
				hasNeighInDir[NE] = hasNeighInDir[NE]
									|| (coord.getX() >= popCoord.getX()
									&& coord.getY() <= popCoord.getY());
				hasNeighInDir[EA] = hasNeighInDir[EA]
									|| (coord.getX() > popCoord.getX()
									&& abs(distY) <= abs(distX));
				hasNeighInDir[SE] = hasNeighInDir[SE]
									|| (coord.getX() >= popCoord.getX()
									&& coord.getY() >= popCoord.getY());
				hasNeighInDir[SO] = hasNeighInDir[SO]
									|| (abs(distX) <= abs(distY)
									&& coord.getY() > popCoord.getY());
				hasNeighInDir[SW] = hasNeighInDir[SW]
									|| (coord.getX() <= popCoord.getX()
									&& coord.getY() >= popCoord.getY());
				hasNeighInDir[WE] = hasNeighInDir[WE]
									|| (coord.getX() < popCoord.getX()
									&& abs(distY) <= abs(distX));
				hasNeighInDir[NW] = hasNeighInDir[NW]
									|| (coord.getX() <= popCoord.getX()
									&& coord.getY() <= popCoord.getY());
			}
//			TODO put scanning for nearest neighbors in separat method
			for(auto hasNeigh : hasNeighInDir) {
				if(!atts->isLongDistDisp()) { // only search LDD neighbors if activated
					break;
				}
				if(!hasNeigh.second) {
					unordered_map<int, pair<int, int>> nDistTemp;
					unordered_map<int, pair<double, double>> propGrassTemp;
					bool cardinalDirection = hasNeigh.first == NO || hasNeigh.first == EA || hasNeigh.first == SO || hasNeigh.first == WE;
					unsigned int maxX = environment->getMaxX();
					unsigned int maxY = environment->getMaxY();
					unsigned int minX = environment->getMinX();
					unsigned int minY = environment->getMinY();
					unsigned int maxDistNOSO = 0;
					unsigned int maxDistEAWE = 0;
					// factors to apply to calc x-y-coords of potential long distance neighbors
					// [0] -> factor for stepFwd
					// [1] -> factor for (radLow - stepDwn)
					int factLeftX[] = {0,0};
					int factLeftY[] = {0,0};
					int factRightX[] = {0,0};
					int factRightY[] = {0,0};
					switch (hasNeigh.first) {
						case NO:
							factLeftX[0] = -1; // x decreases stepping forward westwards
							factLeftY[1] = -1; // y starts northwards, i.e. lower
							factRightX[0] = 1; // // x increases stepping forward eastwards
							factRightY[1] = -1; // y starts northwards, i.e. lower
							maxDistNOSO = popCoord.getY() - minY;
							maxDistEAWE = max(popCoord.getX() - minX, maxY - popCoord.getX());
							break;
						case NE:
							factLeftX[0] = 1; // x increases stepping forward eastwards
							factLeftY[1] = -1; // y starts northwards, i.e. lower
							factRightX[1] = 1; // x starts eastwards, i.e. higher
							factRightY[0] = -1; // y decreases stepping forward northwards
							maxDistNOSO = popCoord.getY() - minY;
							maxDistEAWE = maxX - popCoord.getX();
							break;
						case EA:
							factLeftX[1] = 1; // x starts eastwards, i.e. higher
							factLeftY[0] = -1; // y decreases stepping forward northwards
							factRightX[1] = 1; // // x starts eastwards, i.e. higher
							factRightY[0] = 1; // y increases stepping forward southwards
							maxDistNOSO = popCoord.getY() - minY;
							maxDistEAWE = maxX - popCoord.getX();
							break;
						case SE:
							factLeftX[1] = 1; // x starts eastwards, i.e. higher
							factLeftY[0] = 1; // y increases stepping forward southwards
							factRightX[0] = 1; // x increases stepping forward eastwards
							factRightY[1] = 1; // y starts southwards, i.e. higher
							maxDistNOSO = popCoord.getY() - minY;
							maxDistEAWE = maxX - popCoord.getX();
							break;
						case SO:
							factLeftX[0] = 1; // x increases stepping forward eastwards
							factLeftY[1] = 1; // y starts southwards, i.e. higher
							factRightX[0] = -1; // // x decreases stepping forward westwards
							factRightY[1] = 1; // y starts southwards, i.e. higher
							maxDistNOSO = popCoord.getY() - minY;
							maxDistEAWE = maxX - popCoord.getX();
							break;
						case SW:
							factLeftX[0] = -1; // x decreases stepping forward westwards
							factLeftY[1] = 1; // y starts southwards, i.e. higher
							factRightX[1] = -1; // x starts westwards, i.e. lower
							factRightY[0] = 1; // y increases stepping forward southwards
							maxDistNOSO = popCoord.getY() - minY;
							maxDistEAWE = maxX - popCoord.getX();
							break;
						case WE:
							factLeftX[1] = -1; // x starts westwards, i.e. lower
							factLeftY[0] = 1; // y increases stepping forward southwards
							factRightX[1] = -1; // // x starts westwards, i.e. lower
							factRightY[0] = -1; // y decreases stepping forward northwards
							maxDistNOSO = popCoord.getY() - minY;
							maxDistEAWE = maxX - popCoord.getX();
							break;
						case NW:
							factLeftX[1] = -1; // x starts westwards, i.e. lower
							factLeftY[0] = -1; // y decreases stepping forward northwards
							factRightX[0] = -1; // x decreases stepping forward westwards
							factRightY[1] = -1; // y starts northwards, i.e. lower
							maxDistNOSO = popCoord.getY() - minY;
							maxDistEAWE = maxX - popCoord.getX();
							break;
						default:
							break;
					}
					unsigned int maxKey = maxDistNOSO * maxDistNOSO + maxDistEAWE * maxDistEAWE;
					unsigned int maxRad = ceil(sqrt(maxKey));
					for(unsigned int radLow = migrRad; radLow < maxRad;  radLow++) {
						unsigned int nearestKey = maxKey + 1; // set largest greater than max
						unordered_set<Point> nearestCoords;
						int radHigh = radLow + 1;
						int distKeyLow = radLow * radLow;
						int distKeyHigh = radHigh * radHigh;
						int stepDwn = 0;
						for(int stepFwd = cardinalDirection ? 0 : 1; stepFwd < radHigh; stepFwd++) { // when handling ordinal directions start with stepFwd=1
							unsigned int distKey = stepFwd * stepFwd + (radLow - stepDwn) * (radLow - stepDwn);
							if(distKey > distKeyHigh) {
								stepFwd--;
								stepDwn++;
								distKey = stepFwd * stepFwd + (radLow - stepDwn) * (radLow - stepDwn);
							}
							if(cardinalDirection && stepFwd == (radLow - stepDwn)) { // when handling cardinal direction stop before diagonal
								break;
							}
							unsigned int leftX = popCoord.getX() + factLeftX[0] * stepFwd + factLeftX[1] * (radLow - stepDwn);
							unsigned int leftY = popCoord.getY() + factLeftY[0] * stepFwd + factLeftY[1] * (radLow - stepDwn);
							unsigned int rightX = popCoord.getX() + factRightX[0] * stepFwd + factRightX[1] * (radLow - stepDwn);
							unsigned int rightY = popCoord.getY() + factRightY[0] * stepFwd + factRightY[1] * (radLow - stepDwn);
							Point left(leftX, leftY);
							Point right(rightX, rightY);
							if(distKey > migrRad && distKey >= distKeyLow && distKey <= nearestKey) {
								bool leftWithinBounds = leftX <= maxX && leftX >= minX && leftY <= maxY && leftY >= minY;
								bool rightWithinBounds = rightX <= maxX && rightX >= minX && rightY <= maxY && rightY >= minY;
								if(!(leftWithinBounds || rightWithinBounds)) {
									continue;
								}
								if(nDistTemp.find(distKey) == nDistTemp.end()) {
									nDistTemp[distKey] = pair<int,int>(0,0); // initialize the total number of cells at this distance
								}
								if(leftWithinBounds) {
									nDistTemp[distKey].second++;
									if(grasslandCoords.find(left) != grasslandCoords.end()) // coord must be in list of grassland coords
									{
										if(distKey < nearestKey) { // if the current key is lower than the previous, clear list of nearest
											nearestCoords.clear();
										}
										nearestCoords.emplace(left);
										nearestKey = distKey;
										nDistTemp[distKey].first++;
									}
								}
								if(rightWithinBounds) {
									nDistTemp[distKey].second++;
									if(grasslandCoords.find(right) != grasslandCoords.end())  // coord must be in list of grassland coords
									{
										if(distKey < nearestKey) { // if the current key is lower than the previous, clear list of nearest
											nearestCoords.clear();
										}
										nearestCoords.emplace(right);
										nearestKey = distKey;
										nDistTemp[distKey].first++;
									}
								}
							}
							if(!cardinalDirection && stepFwd == (radLow - stepDwn)) { // when handling ordinal direction stop after diagonal
								break;
							}
						}
						if(nearestCoords.size() > 0) {
							for(auto coord : nearestCoords) {
								int distX = popCoord.getX() - coord.getX();
								int distY = popCoord.getY() - coord.getY();
								int distKey = distX * distX + distY * distY; //key for same distance cells is the sum of squares of distance in each direction
								double distance = sqrt(((double) distKey));
								sumMigrDistance += 1.0 / pow(distance, rMigrPref);
								//TODO check if next lines are valid
								neighMigrCoords[coord] = distance;
							}
							for(auto distPair : nDistTemp) {
								int distKey = distPair.first;
								if(nDist.find(distKey) == nDist.end()) {
									nDist[distKey] = pair<int,int>(0,0); // initialize the total number of cells at this distance
									propGrass[distKey] = pair<double,double>(0.0,0.0); // initialize the map for grassland proportion
								}
								nDist[distKey].first += nDistTemp[distKey].first;
								nDist[distKey].second += nDistTemp[distKey].second;
							}
							break; // end search in this direction
						}
					}
				}
			}
			for(auto propPair : propGrass) {
				int nLowerGrass = 0;
				int nLowerTotal = 0;
				for(auto distPair : nDist) {
					if(distPair.first == propPair.first) {
						double propAtDist = ((double) distPair.second.first) / distPair.second.second;
						propGrass[propPair.first].first = propAtDist;
						continue;
					}
					if(distPair.first < propPair.first) {
						nLowerGrass += distPair.second.first;
						nLowerTotal += distPair.second.second;
					}
				}
				double propBelowDist = 0.0;
				if(nLowerTotal > 0) {
					propBelowDist = ((double) nLowerGrass) / nLowerTotal;
				}
				propGrass[propPair.first].second = propBelowDist;
			}
			for (auto neighbor : neighMigrCoords) {
				if(!(baseMigrRate > 0)) {
					break;
				}
				Point neighCoord = neighbor.first;
				int distX = popCoord.getX() - neighCoord.getX();
				int distY = popCoord.getY() - neighCoord.getY();
				int distKey = distX * distX + distY * distY; //key for same distance cells is the sum of squares of distance in each direction
				double distance = neighbor.second;
				double prefNeigh = (1.0 / pow(distance, rMigrPref)) / sumMigrDistance; // the preference of migrating to this neighbor
//				TODO maybe change to use coordinate distance without actual distance between cells --> if so, change rDecay as well
				double pFind = pow(propGrass[distKey].first, (1-rMigrSight)); // probabilty to find a grassland cell at this distance
				double pSurv = exp(-rMigrDecay * (1 - propGrass[distKey].second) * distance); // distance-dependant survival probability
				double pMigr = prefNeigh * pFind * pSurv;
				double migrRate = pMigr * baseMigrRate; // the neighbors actual migration rate based on its share
				sumPMigr += pMigr;
				Population* neighPop = neighborhood[neighCoord];
				LifeStage* neighStage = neighPop->getStage(stageName);
				Flow* flowMigration = new Flow(localStage, FlowType::MIGRATION, neighStage, 1.0, atts->isDiscreteDispersal());
				InfluenceBinomialFactor* inflBinMigr = new InfluenceBinomialFactor(migrRate, true);
				for(auto influence : migrInfluences) {
					inflBinMigr->setModInfl(influence);
				}
				flowMigration->addInfluence(inflBinMigr);
				addFlow(flowMigration, false, false);
				emigrationFlows.push_back(flowMigration);
			}
			double pMortMigr = 1.0 - sumPMigr;
			if(baseMigrRate > 0 && pMortMigr > 0.0) {
				double mortMigrRate = pMortMigr * baseMigrRate;
				Flow* flowMortMigration = new Flow(localStage, NULL, 1.0, atts->isDiscreteDispersal());
				InfluenceBinomialFactor* inflBinMort = new InfluenceBinomialFactor(mortMigrRate, true);
				flowMortMigration->addInfluence(inflBinMort);
				for(auto influence : migrInfluences) {
					flowMortMigration->addInfluence(influence);
				}
				addFlow(flowMortMigration, false, false);
				emigrationFlows.push_back(flowMortMigration);
			}
		}
		iStage++;
	}
}

//---------------------------------------------------------------------------
