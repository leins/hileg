FROM debian:stable

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install --yes --no-install-recommends \
        wget ca-certificates p7zip-full \
        build-essential cmake pkg-config ninja-build \
        mingw-w64 \
        libnetcdf-dev

ENV NETCDF_PREFIX=/opt/netcdf

RUN export NETCDF_VERSION=4.7.4 && \
    mkdir -p ${NETCDF_PREFIX} && \
    cd ${NETCDF_PREFIX} && \
    wget http://www.unidata.ucar.edu/downloads/netcdf/ftp/netCDF${NETCDF_VERSION}-NC4-64.exe && \
    7z x netCDF${NETCDF_VERSION}-NC4-64.exe && \
    rm netCDF${NETCDF_VERSION}-NC4-64.exe
