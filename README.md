# HiLEG - High-resolution Large Environmental Gradient

HiLEG is a spatially explicit stage- and cohort-based simulation model that allows to use daily of time steps, external drivers, such as climate and land use, and dispersal between neighboring habitats.
With its mechanistic implementation, its simulation results can be used to examine the interrelations between population dynamics and external drivers.

The HiLEG model is designed to be used for different (dispersing) terrestrial animal species by specifying a corresponding set of parameters and external drivers along large environmental gradients. If the spatial resolution of the available external drivers data is too coarse, bilinear interpolation can be applied to achieve a finer resolution.

It was developed as part of the [Ecoclimb project][ECOCLIMB].

## Build

The executable model is build using CMake. For details refer to files [CMakeLists.txt](CMakeLists.txt), [.gitlab-ci.yml](.gitlab-ci.yml) and [builder.Dockerfile](builder.Dockerfile).

The most recent binaries pre-built by GitLab's CI/CD processor are located here:
- [Linux build][BUILD_LINUX] (zip)
- [Windows build][BUILD_WIN] (zip)

## Run

To run HiLEG using external climate conditions (input file [influences.csv](influences.csv)), it requires specific NetCDF input files located in subfolder ./climate.
Request the climate data for download at the UFZ data investigation portal: [Full data][CLIMATE_FULL] (~28 GB) or the [data subset][CLIMATE_PART] (~1.5 GB) as used for publications [Leins et al. (2021)][LEINSETAL2021], [Leins et al. (2022)][LEINSETAL2022] and the [PhD thesis of J. A. Leins](documents/Leins_JA_PhD_thesis.pdf).

### Parameterization

Two mandatory and four optional (have to exist but can be empty) CSV input files must be supplied in the root folder to set up the simulation run.

Refer to the header of these files in the repository for usage instructions.

| file                                      | mandatory | description                                                                             |
|-------------------------------------------|-----------|-----------------------------------------------------------------------------------------|
| [stages.csv](stages.csv)                  | YES       | defines the life cycle of the target species using the definition of life stages        |
| [grasslandCells.csv](grasslandCells.csv)  | YES       | locations (grassland cells) of simulated populations and their associated climate cells |
| [initialDens.csv](initialDens.csv )       | NO        | initial density per stage and location used instead of value defined in stages.csv      |
| [influences.csv](influences.csv)          | NO        | influence of external drivers (climate/disturbance) on life cycle processes             |
| [disturbances.csv](disturbances.csv)      | NO        | potential timing and location of disturbances                                           |
| [distScheme.csv](distScheme.csv)          | NO        | applied timing and location of disturbances                                             |

Additionally, the following command line arguments can be used to parameterize the simulation run.

Without command line arguments the simulation runs from 1/Jan/1970 to 30/Dec/2080 using the stages, influences, initial densities, locations and disturbances specified in the default files while creating 10 replicates.

| parameter      | default            | description                                                                                                                                                                    |
|----------------|--------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| -cCap          | 25                 | carrying capacity per square meter. Floating point values > 0 are valid                                                                                                        |
| -iClim         | 0                  | index of initial climate change scenario to use. Values between 0 and 11 are valid                                                                                             |
| -nClim         | 1                  | number of consecutive climate change scenario to use. Valid if > 0 AND (iClim + nClim - 1) < 12                                                                                |
| -iYear         | 1970               | initial year of simulation run. Values from 1970 to 2080 are valid                                                                                                             | 
| -nYear         | 111                | duration of a simulation run in years.                                                                                                                                         |
| -dYear         | 0                  | delta years to additional simulation runs. Repeated n times as long as (iYear + n * dYear + nYear) <= 2081                                                                     |
| -iFrame        | iYear              | initial year of first time frame used to randomly draw years from. Defaults to initial year of simulation run                                                                  | 
| -nFrame        | nYear              | length in years of first time frame used to randomly draw years from. Defaults to duration of of simulation run                                                                |
| -nSeeds        | 10                 | number of replicates (random seeds) to create per climate change scenario and (timely) additional simulation runs                                                              |
| -seed          |                    | run simulation without replicates using the given explicit integer number for the random seed                                                                                  |
| -limShfl       | 0                  | number of years before (iTime) and after (iTime + nTime) simulation period to include in reshuffling the order of years                                                        |
| -runID         | timestamp          | use the given (unique) integer number to name output files associated with this simulation run. Uses current timestamp as default                                              |
| -dispersal     | 2                  | applied dispersal type: 0 = none, 1 = base immigration, 2 = migration between neighboring cells, 3 = both (1 and 2)                                                            |
| -fnameStages   | stages.csv         | filename of specific file defining the life cycle of the target species using the definition of life stages                                                                    |
| -fnameCells    | grasslandCells.csv | filename of specific file containing locations (grassland cells) of simulated populations and their associated climate cells                                                   |
| -fnameDens     | initialDens.csv    | filename of specific file containing initial density per stage and location                                                                                                    |
| -fnameInfl     | influences.csv     | filename of specific file containing the definition of external driver influences (climate/disturbance) on life cycle processes                                                |
| -fnameDist     | disturbances.csv   | filename of specific file containing the potential timing and location of disturbances                                                                                         |
| -fnameScheme   | distScheme.csv     | filename of specific file containing the applied timing and location of disturbances                                                                                           |
| --climInterpol |                    | use bilinear interpolation of the up to four adjacent climate cell values to calculate the respective value in a grassland cell                                                |
| --climOnly     |                    | execute simulation runs with climate conditions as only external driver                                                                                                        |
| --dens         |                    | create output file of daily density values (has large file size)                                                                                                               |
| --discrDisp    |                    | activates discrete dispersal limiting dispersal to 'full' individuals, i.e., a value of 1 / [habitat size] individuals per sqm                                                 |
| --distOnly     |                    | only apply additional disturbance schemes using potential disturbances defined in disturbances.csv                                                                             |
| --dynDist      |                    | dynamic relative timing (if applies) of disturbance schedule following the beginning of the vegetation period (temp. sum >= 200 °C)                                            |
| --finalDens    |                    | create an output file for each random seed containing the densities per life stage and grassland cell by the end of the simulation run                                         |
| --help         |                    | prints help page                                                                                                                                                               |
| --LDD          |                    | activates long distance dispersal allowing the species to cross distances outside dispersal radius in case no grassland cells are within a 90° range into a cardinal direction |
| --listClim     |                    | list available climate change scenarios and their index                                                                                                                        |
| --meanStats    |                    | create an output file for each separate simulation (cf. --soloPops) containing the mean stats at the end of a simulation run for a reduced list of evaluation parameters       |
| --memSave      |                    | save memory by preloading climate data for the current simulation year only (instead of whole time frame)                                                                      |
| --movShfl      |                    | move the time window for reshuffling the simulation years along when advancing the current simulation year                                                                     |
| --prognosis    |                    | create one disturbance scheme for each unique disturbance in disturbances.csv and each grassland cell flagged fixed=0 in distScheme.csv  (activates flag --meanStats)          |
| --soloPops     |                    | run a separate simulation for each distinct population defined in initialDens.csv while using initial density value define in stages.csv for all remaining populations         |                                                                |
| --uniformDist  |                    | apply disturbances uniformly in all grassland cells and create a scheme for each defined disturbance. Ignore file distScheme.csv                                               |
| --yearly       |                    | output the yearly stats by the end of each simulation year                                                                                                                     |

### Output

A simulation run creates five types of output files in subfolder \_./output/run\_\<runID\>/\_ depending command line arguments (see tables below).
Furthermore, the command line arguments used for the simulation run are saved in the file **args.txt** and the CSV input files are copied to the output folder using their default naming (cf. above table).
The randomly applied disturbance scheme (cf. parameter prob in distScheme.csv) and the temperature dependent disturbance timing (cf. parameter --dynDist) are stored in files **appliedDistScheme.csv** and **disturbanceTiming.csv**.

| filename                                                                                                                | creation (argument)             | description                                                                                                                                                                                            |
|-------------------------------------------------------------------------------------------------------------------------|---------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| \<runID\>\_stats\_\<climateModel\>\_\<RCPscenario\>\_\<iYear\>\_\<iYear+nYear\>.csv                                     | always                          | population statistics per grassland cell and replicate at the end of a single simulation time slice (one file per time slice, disturbance scheme, climate model and RCP scenario)                      |
| \<runID\>\_orderOfYears\_\<climateModel\>\_\<RCPscenario\>\_\<iYear\>\_\<iYear+nYear\>.csv                              | always                          | the order of reshuffled years per random seed                                                                                                                                                          |
| \<runID\>\_statsYearly\_\<climateModel\>\_\<RCPscenario\>\_\<iYear\>\_\<iYear+nYear\>.csv                               | --yearly                        | yearly population statistics per grassland cell and replicate at the end of each simulation year (one file per time slice, disturbance scheme, climate model and RCP scenario)                         |
| \<runID\>\_stats\_mean\_\<distSchemeName\>\_\<climateModel\>\_\<RCPscenario\>\_\<iYear\>\_\<iYear+nYear\>.csv           | --prognosis or --meanStats      | mean population statistics per grassland cell of all replicates at the end of a single simulation time slice (one file per time slice, disturbance scheme, climate model and RCP scenario)             |
| finalDens/\<runID\>\_finalDens\_seed\<#seed\>\_\<distSchemeName\>\_\<climateModel\>\_\<RCPscenario\>\_\<finalYear\>.csv | --finalDens                     | final density per grassland cell and life stage at the end of a simulation time slice (one file per seed, time slice, disturbance scheme, climate model and RCP scenario)                              |

The files containing the _final density_ are of the same format as the file **initialDens.csv** and thus can be used as simulation input for _initial density_

Depending on file in question, different of the following columns are contained:

| column name                          | description                                                                                               |
|--------------------------------------|-----------------------------------------------------------------------------------------------------------|
| distSchemeName                       | mowing schedule name (acronym), where M66 is equal to mowing schedule M20+00+44 (see publication)         |
| xCoord                               | X Coordinate of grassland cell / population                                                               |
| yCoord                               | Y Coordinate of grassland cell / population                                                               |
| seed                                 | Number of the seed used in the random generator for this replicate (misses in stats_mean)                 |
| disturbance                          | Name of the disturbance scenario applied at the coordinate                                                |
| year                                 | simulation year                                                                                           |
| RCPscenario                          | climate change scenario                                                                                   |
| lifetime                             | Number of the days the population's density was >= minimum density                                        |
| firstVisit                           | day of first visit to a habitat                                                                           |
| firstEst                             | day of first population establishment                                                                     |
| finalDensity                         | Population density at the of a simulation year the current simulation run time slice                      |
| nExtinction                          | Number of times the population density dropped below the minimum density                                  |
| nQuasiExtinction                     | Number of times the population density dropped below the threshold defining quasi extinction (stages.csv) |
| meanDensity                          | Daily mean population density [ind. per m^2] with respect to the full runtime                             |
| meanMigration                        | net migration [ind. per m^2] during simulation year                                                       |
| [yearly\_]lifetime\_\<stageName\>    | (Yearly) number of the days a life stage's density was >= minimum density (active)                        |
| [yearly\_]meanDensity\_\<stageName\> | Daily mean life stage density with respect to its lifetime (per year)                                     |
| [yearly\_]peak\_\<stageName\>        | (yearly) maximum life stage density during the whole last period it was active                            |
| [yearly\_]grossPeak\_\<stageName\>   | (yearly) maximum life stage density during the whole last period it was active considering dispersal      |
| [yearly\_]mortality\_\<stageName\>   | total Life Stage mortality [ind. per m^2] (during a simulation year)                                      |
| [yearly\_]gain\_\<stageName\>        | total Life Stage gain [ind. per m^2] (during a simulation year)                                           |
| [yearly\_]immigration\_\<stageName\> | total immigration [ind. per m^2] to imago Life Stage during simulation year (during a simulation year)    |
| [yearly\_]emigration\_\<stageName\>  | total emigration [ind. per m^2] from imago Life Stage during simulation year (during a simulation year)   |

## Publications

With the files and parameters presented for this version of HiLEG model, the dispersal success of the large marsh grasshopper (LMG) was analyzed as described in Chapter 5 of the [PhD thesis by J. A. Leins](documents/Leins_JA_PhD_thesis.pdf).

The output for this analysis was generated using [HiLEG release v1.5][HILEGV15] with the command line arguments below.
Line 1 is the command for running simulations of protective mowing (schedule T_prot, see Table 5.1 of thesis) in all grassland cells. Parameters -iClim, -fnameCells and -fnameCells must be adapted to select climate change scenario (3=RCP2.6, 4=RCP4.5, 5=RCP8.5) and region containing initial population (**aggr** or **frag**)
Line 2 is the command for running simulations of conventional mowing (schedule T_conv, see Table 5.1 of thesis) in most grassland cells and randomly applying protective mowing in other cells. Parameters -iClim, -fnameCells and -fnameCells must be adapted as described for line 1 and parameter -fnameScheme to select region and define probability of protective grassland mowing (values 001, 002, ..., 020).

```sh
hileg[.exe] --dynDist -iClim <3-5> -nClim 1 -iYear 2020 -nYear 60 -seed 101 -nSeeds 100 -dispersal 2 --memSave --climInterpol --yearly --distOnly -limShfl 10 --movShfl --discrDisp -fnameCells grasslandCells_<region>.csv -fnameDens initialDens_<region>.csv -fnameDist disturbances_M00_M99.csv -fnameScheme distScheme_M00.csv
hileg[.exe] --dynDist -iClim <3-5> -nClim 1 -iYear 2020 -nYear 60 -seed 101 -nSeeds 100 -dispersal 2 --memSave --climInterpol --yearly --distOnly -limShfl 10 --movShfl --discrDisp -fnameCells grasslandCells_<region>.csv -fnameDens initialDens_<region>.csv -fnameDist disturbances_M00_M99.csv -fnameScheme distScheme_M00_M99_<pProt>_<region>.csv
```

For the actual output files generated per region (distinguished by spatial grassland configuration) using the above arguments and the calculated evaluation data, please refer to the following archives: [aggregated grasslands][OUTPUT15_AGGR], [fragmented grasslands][OUTPUT15_FRAG] and [evaluation data][EVAL15].

Other publications using HiLEG are:
- The original study regarding the combined effect of climate change and land use on the LMG ([Leins et al., 2021][LEINSETAL2021]) using [model version v1.0][HILEGV10]
- A follow up study assessing the additional effect of an dispersal process ([Leins et al., 2022][LEINSETAL2022]) using [model version v1.4][HILEGV14]
- A study of cost-effective conservation ([Gerling et al., (2022)][GERLINGETALL2022]) using [model version v1.1.2][HILEGV112]
- A study regarding the robustness cost-effective conservation measures ([Drechsler et al. (2021)][DRECHSLERETAL2021]) using [model version v1.1.2][HILEGV112]
- A study comparing purchase of grassland to offset measures in conservation planning (Gerling et al., in preparation) using [model version v1.2.4][HILEGV124]

## Documents and folders

| file / folder                                          		| description                                                                                                         |
|---------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------|
| [ODD.pdf](documents/ODD.pdf)                           		| Overview, design concepts and details (delta ODD model description)                                                 |
| [HiLEG_er.pdf](documents/HiLEG_er.pdf)                 		| Visualization of HiLEG model entities, relations and processes                                                      |
| [HiLEG_er_caption.txt](documents/HiLEG_er_caption.txt) 		| Explanatory caption for file [HiLEG_er.pdf](documents/HiLEG_er.pdf)                                                 |
| [Leins_JA_PhD_thesis.pdf](documents/Leins_JA_PhD_thesis.pdf)	| PhD thesis of J.A. Leins containing HiLEG model description and three studies of the large marsh grasshopper using HiLEG                                                |
| `climate`                                              	  	| Not in repository. Required for simulation runs with external climate conditions. Request the data for download at the UFZ data investigation portal: [Full data][CLIMATE_FULL] (~28 GB) or the [data subset][CLIMATE_PART] (~1.5 GB) as used for publication [Leins et al. 2020][LEINSETAL2021]. |
| `documents`                                            		| Model documentation files                                                                                           |
| `lib`                                                  		| Additional libraries required to build model                                                                        |
| `output`                                               		| Not in repository. Will be created during simulation run as location for output files                               |
| `src`                                                  		| C++ source files (headers, classes)                                                                                 |


[ECOCLIMB]: https://www.b-tu.de/en/ecoclimb
[LEINSETAL2021]: https://doi.org/10.1016/j.ecolmodel.2020.109355
[LEINSETAL2022]: https://doi.org/10.1101/2021.11.21.469124
[DRECHSLERETAL2021]: https://doi.org/10.1016/j.jenvman.2021.113201
[GERLINGETALL2022]: https://doi.org/10.1093/qopen/qoac004
[BUILD_LINUX]: https://git.ufz.de/leins/hileg/-/jobs/artifacts/main/download?job=build_linux
[BUILD_WIN]: https://git.ufz.de/leins/hileg/-/jobs/artifacts/main/download?job=build_windows
[CLIMATE_FULL]: https://www.ufz.de/record/dmp/archive/8665/en/
[CLIMATE_PART]: https://www.ufz.de/record/dmp/archive/8852/en/
[HILEGV10]: https://git.ufz.de/leins/hileg/-/tree/v1.0
[HILEGV14]: https://git.ufz.de/leins/hileg/-/tree/v1.4
[HILEGV112]: https://git.ufz.de/leins/hileg/-/tree/v1.1.2-cost
[HILEGV124]: https://git.ufz.de/leins/hileg/-/tree/v1.2.4-offset
[OUTPUT15_AGGR]: https://www.ufz.de/record/dmp/archive/12742/en/
[OUTPUT15_FRAG]: https://www.ufz.de/record/dmp/archive/12741/en/
[EVAL15]: https://www.ufz.de/record/dmp/archive/12743/en/
